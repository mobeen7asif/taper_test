<?php

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1170; /* pixels */
}

if ( ! function_exists( 'appcastle_theme_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 */
	function appcastle_theme_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		load_theme_textdomain( 'appcastle', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );


		// Register nav menu.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'appcastle' ),
		) );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) );

		/*
		 * Adding Thumbnail support
		 */
		add_theme_support( "post-thumbnails" );

		add_image_size( 'appcastle-small', 100, 100, TRUE );
		add_image_size( 'appcastle-medium', 200, 200, TRUE );
		add_image_size( 'appcastle-full', 875, 500, TRUE );


		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'gallery', 'audio', 'video', 'chat'
		) );


		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'appcastle_custom_background_args', array(
			'default-color' => 'f2f3f5',
			'default-image' => '',
		) ) );
	}
	add_action( 'after_setup_theme', 'appcastle_theme_setup' );
} // appcastle_theme_setup



function appcastle_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	* supported by Raleway, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$raleway = esc_html_x( 'on', 'Raleway font: on or off', 'appcastle' );

	if ( 'off' !== $raleway ) {
		$font_families = array();

		if ( 'off' !== $raleway ) {
			$font_families[] = 'Raleway:400,300,500,700,800,100,600';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}


//////////////////////////////////////////////////////////////////
// Enqueue scripts and styles.
//////////////////////////////////////////////////////////////////
if (!function_exists('appcastle_style_scripts_and_fonts')) {
	function appcastle_style_scripts_and_fonts(){

		// CSS Files
		wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), NULL);
		wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), NULL);
		wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), NULL);
		wp_enqueue_style('magnific-popup-css', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), NULL);
		wp_enqueue_style('animation-css', get_template_directory_uri() . '/assets/css/animation.css', array(), NULL);
		wp_enqueue_style('vegas-css', get_template_directory_uri() . '/assets/css/vegas.min.css', array(), NULL);
		wp_enqueue_style('appcastle-style', get_stylesheet_uri() );
		wp_enqueue_style('appcastle-responsive-css', get_template_directory_uri() . '/assets/css/responsive.css', array(), NULL);


		// Google Fonts
		wp_enqueue_style( 'appcastle-fonts', appcastle_fonts_url(), array(), null );


		// JS Files
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'jquery-easing-min-js', get_template_directory_uri() . '/assets/js/jquery.easing.min.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'fitvids-js', get_template_directory_uri() . '/assets/js/jquery.fitvids.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'magnific-popup-js', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'parallax-js', get_template_directory_uri() . '/assets/js/jquery.parallax.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'vegas-js', get_template_directory_uri() . '/assets/js/vegas.min.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'youtube-background-js', get_template_directory_uri() . '/assets/js/jquery.youtubebackground.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'swfobject-js', get_template_directory_uri() . '/assets/js/swfobject.js', array('jquery'), NULL, TRUE );
		wp_enqueue_script( 'appcastle-main-js', get_template_directory_uri() . '/assets/js//main.js', array('jquery'), NULL, TRUE );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action('wp_enqueue_scripts', 'appcastle_style_scripts_and_fonts');
}


// Admin Script
if (!function_exists('appcastle_admin_scripts')) {
	function appcastle_admin_scripts() {
		if(is_admin()){
			wp_enqueue_script( 'post-meta-js', get_template_directory_uri() . '/assets/js/admin/post-meta.js', array('jquery'), NULL, TRUE );
		}
	}
	add_action('admin_enqueue_scripts','appcastle_admin_scripts');
}



//////////////////////////////////////////////////////////////////
// Widget register.
//////////////////////////////////////////////////////////////////
if (!function_exists('appcastle_widgets_init')) {
	function appcastle_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'appcastle' ),
			'id'            => 'sidebar',
			'description'   => esc_html__('Widgets in this area will be shown on all posts and pages.', 'appcastle'),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );

	}
	add_action( 'widgets_init', 'appcastle_widgets_init' );
}




//////////////////////////////////////////////////////////////////
// Comment
//////////////////////////////////////////////////////////////////

if ( ! function_exists( 'appcastle_comment' ) ):

	function appcastle_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		switch ( $comment->comment_type ) :
			case 'pingback' :
			case 'trackback' :
				// Display trackbacks differently than normal comments.
				?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<p><?php esc_html_e( 'Pingback:', 'appcastle' ) ?><?php comment_author_link(); ?><?php edit_comment_link( esc_html__( '(Edit)', 'appcastle' ), '<span class="edit-link">', '</span>' ); ?></p>
				<?php
				break;
			default :

				global $post;
				?>
			<li <?php comment_class( 'media' ); ?> id="li-comment-<?php comment_ID(); ?>">

				<div id="comment-<?php comment_ID(); ?>" class="comment-body">
					<div class="media-left media-object">
						<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
					</div>
					<div class="media-body">
						<?php
						printf( '<h2 class="comment-author">%1$s</h2>',
							get_comment_author_link() );
						?>
						<h3 class="date"><?php echo get_comment_date() ?></h3>
						<?php edit_comment_link( esc_html__( 'Edit', 'appcastle' ), '<span class="edit-link">', '</span>' ); ?>

						<?php if ( '0' == $comment->comment_approved ) : ?>
							<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'appcastle' ); ?></p>
						<?php endif; ?>
						<div class="comment-content">
							<?php comment_text(); ?>
						</div>

						<?php comment_reply_link( array_merge( $args, array(
							'reply_text' => esc_html__( 'Replay', 'appcastle' ),
							'after'      => '',
							'depth'      => $depth,
							'max_depth'  => $args['max_depth']
						) ) ); ?>
					</div>
				</div>
				<?php
				break;
		endswitch;
	}

endif;




//////////////////////////////////////////////////////////////////
// Include the TGM Plugin Activation class
//////////////////////////////////////////////////////////////////

require_once( get_template_directory() . '/admin/tgm/activation.php' );

if(!function_exists('appcastle_register_required_plugins')):

	add_action( 'tgmpa_register', 'appcastle_register_required_plugins' );

	function appcastle_register_required_plugins() {

		$plugins = array(

			// WPBakery Visual Composer
			array(
				'name'               => esc_html__('WPBakery Visual Composer', 'appcastle'),
				'slug'               => 'js_composer',
				'source'             => get_template_directory() . '/admin/plugins/js_composer.zip',
				'required'           => true,
				'version'            => '',
				'force_activation'   => false,
				'force_deactivation' => false,
				'external_url'       => '',
				'is_callable'        => '',
			),

			// AppCastle Core
			array(
				'name'               => esc_html__('AppCastle Core', 'appcastle'),
				'slug'               => 'shapedtheme-core',
				'source'             => get_template_directory() . '/admin/plugins/shapedtheme-core.zip',
				'required'           => true,
				'version'            => '',
				'force_activation'   => false,
				'force_deactivation' => false,
				'external_url'       => '',
				'is_callable'        => '',
			),
			// Contact Form 7
			array(
				'name'     => esc_html__('Contact Form 7', 'appcastle'),
				'slug'     => 'contact-form-7',
				'required' => true,
			),
			// Meta Box
			array(
				'name'     => esc_html__('Meta Box', 'appcastle'),
				'slug'     => 'meta-box',
				'required' => true,
			),
			// Redux Framework
			array(
				'name'     => esc_html__('Redux Framework', 'appcastle'),
				'slug'     => 'redux-framework',
				'required' => true,
			),

		);


		$config = array(
			'id'           => 'tgmpa',
			'default_path' => '',
			'menu'         => 'tgmpa-install-plugins',
			'parent_slug'  => 'themes.php',
			'capability'   => 'edit_theme_options',
			'has_notices'  => true,
			'dismissable'  => true,
			'dismiss_msg'  => '',
			'is_automatic' => false,
			'message'      => '',
		);

		tgmpa( $plugins, $config );
	}

endif;


//////////////////////////////////////////////////////////////////
// Woocommerce support
//////////////////////////////////////////////////////////////////
if(!function_exists('appcastle_woocommerce_support')){
	function appcastle_woocommerce_support() {
		add_theme_support( 'woocommerce' );
	}
	add_action( 'after_setup_theme', 'appcastle_woocommerce_support' );
}



//////////////////////////////////////////////////////////////////
// Search only posts
//////////////////////////////////////////////////////////////////

function appcastle_search_only_posts($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','appcastle_search_only_posts');



// Redux integration
if ( ! isset( $redux_demo ) ) {
	require_once( get_template_directory() . '/inc/theme-options/admin-config.php' );
}


// Remove Demo Mode
function appcastle_remove_demo_mode_link() { // Be sure to rename this function to something more unique
	if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
		remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks' ), null, 2 );
	}
	if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
		remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
	}
}

add_action( 'init', 'appcastle_remove_demo_mode_link' );

// Custom style
require_once( get_template_directory() . '/inc/theme-style.php' );

//Main Navigation
require_once( get_template_directory()  . '/inc/menu/one-page-nav-walker.php');

// Load the TGM init if it exists
if (file_exists( get_template_directory() . '/admin/plugin-config.php')) {
	require_once( get_template_directory() . '/admin/plugin-config.php');
}

// Include the meta box script
require_once (get_template_directory().'/inc/meta-box/meta-box-options.php');


require_once get_template_directory() . '/inc/template-tags.php';



