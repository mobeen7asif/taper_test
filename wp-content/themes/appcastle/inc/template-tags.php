<?php
//----------------------------------------------------------------------
//  Blog Pagination
//----------------------------------------------------------------------

if (!function_exists('appcastle_posts_pagination')) {
	function appcastle_posts_pagination() {
		global $wp_query;
		if ($wp_query->max_num_pages > 1) {
			$big   = 999999999; // need an unlikely integer
			$items = paginate_links(array(
				'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
				'format'    => '?paged=%#%',
				'prev_next' => TRUE,
				'current'   => max(1, get_query_var('paged')),
				'total'     => $wp_query->max_num_pages,
				'type'      => 'array',
				'prev_text' => esc_html__('Previous', 'appcastle'),
				'next_text' => esc_html__('Next', 'appcastle')
			));

			$pagination = "<ul class=\"pagination\">\n\t<li>";
			$pagination .= join("</li>\n\t<li>", $items);
			$pagination . "</li>\n</ul>\n";

			echo $pagination;
		}

		return;
	}
}



//----------------------------------------------------------------------
// Post Meta
//----------------------------------------------------------------------

if (!function_exists('appcastle_posted_on')) {
	function appcastle_posted_on(){
		global $appcastle_option; ?>
		<ul class="list-inline">
			<?php if($appcastle_option['post-author-en']== 1) : ?>
				<li class="posted-by">
					<?php printf('<a href="%1$s"><i class="fa fa-user"></i>%2$s</a>',
						esc_url(get_author_posts_url(get_the_author_meta('ID'))),
						esc_html(get_the_author())
					) ?>
				</li>
			<?php endif; ?>
			<?php if($appcastle_option['post-date-en']==1) : ?>
				<li class="publish-date"><i class="fa fa-clock-o"></i><?php the_time('M d, Y') ?></li>
			<?php endif; ?>
			<?php if($appcastle_option['post-view-count-en']==1) : ?>
				<?php global $post;
				$visitor_count = get_post_meta( $post->ID, '_post_views_count', true);
				if( $visitor_count == '' ){ $visitor_count = 0; }
				if( $visitor_count >= 1000 ){
					$visitor_count = round( ($visitor_count/1000), 2 );
					$visitor_count = $visitor_count.'k';
				} ?>
				<li class="views"><i class="fa fa-eye"></i><?php echo esc_attr( $visitor_count ); ?></li>
			<?php endif; ?>

			<?php if($appcastle_option['post-comment-count-en']==1) : ?>
				<li class="comments"><i class="fa fa-comment-o"></i>
					<?php comments_popup_link( esc_html__( '0', 'appcastle' ), esc_html__( '1', 'appcastle' ), esc_html__( '%', 'appcastle' ) ); ?>
				</li>
			<?php endif; ?>
		</ul>
		<?php
	}
}



//----------------------------------------------------------------------
// Related Post Meta
//----------------------------------------------------------------------

if (!function_exists('appcastle_related_posted_on')) {
	function appcastle_related_posted_on(){
		global $appcastle_option; ?>
		<ul class="list-inline">
			<?php if($appcastle_option['post-date-en']==1) : ?>
				<li class="publish-date"><i class="fa fa-clock-o"></i><?php the_time('M d, Y') ?></li>
			<?php endif; ?>
			<?php if($appcastle_option['post-view-count-en']==1) : ?>
				<?php global $post;
				$visitor_count = get_post_meta( $post->ID, '_post_views_count', true);
				if( $visitor_count == '' ){ $visitor_count = 0; }
				if( $visitor_count >= 1000 ){
					$visitor_count = round( ($visitor_count/1000), 2 );
					$visitor_count = $visitor_count.'k';
				} ?>
				<li class="views"><i class="fa fa-eye"></i><?php echo esc_attr( $visitor_count ); ?></li>
			<?php endif; ?>
		</ul>
		<?php
	}
}



//----------------------------------------------------------------------
//  Post tag list
//----------------------------------------------------------------------

if (!function_exists('appcastle_post_tag_list')) {
	function appcastle_post_tag_list() {
		global $appcastle_option;
		if($appcastle_option['post-tags-en']==1){
			$tags_list = get_the_tag_list( '', ' ' );
			if ( $tags_list ) {
				printf( '<div class="single-post-tags text-uppercase"><span class="tags-links">' . '%1$s' . '</span></div>', $tags_list );
			}
		}
	}
}



//----------------------------------------------------------------------
// Archive title
//----------------------------------------------------------------------

if ( ! function_exists( 'appcastle_archive_title' ) ) :

	function appcastle_archive_title( $before = '', $after = '' ) {
		if ( is_category() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), single_cat_title( '', false ) );
		} elseif ( is_tag() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), single_tag_title( '', false ) );
		} elseif ( is_author() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), '<span class="vcard">' . get_the_author() . '</span>' );
		} elseif ( is_year() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'appcastle' ) ) );
		} elseif ( is_month() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'appcastle' ) ) );
		} elseif ( is_day() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'appcastle' ) ) );
		} elseif ( is_tax( 'post_format' ) ) {
			if ( is_tax( 'post_format', 'post-format-aside' ) ) {
				$title = esc_html_x( 'Asides', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
				$title = esc_html_x( 'Galleries', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
				$title = esc_html_x( 'Images', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
				$title = esc_html_x( 'Videos', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
				$title = esc_html_x( 'Quotes', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
				$title = esc_html_x( 'Links', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
				$title = esc_html_x( 'Statuses', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
				$title = esc_html_x( 'Audio', 'post format archive title', 'appcastle' );
			} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
				$title = esc_html_x( 'Chats', 'post format archive title', 'appcastle' );
			}
		} elseif ( is_post_type_archive() ) {
			$title = sprintf( esc_html__( '%s', 'appcastle' ), post_type_archive_title( '', false ) );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( esc_html__( '%1$s: %2$s', 'appcastle' ), $tax->labels->singular_name, single_term_title( '', false ) );
		} else {
			$title = esc_html__( 'Archives', 'appcastle' );
		}

		/**
		 * Filter the archive title.
		 *
		 * @param string $title Archive title to be displayed.
		 */
		$title = apply_filters( 'get_the_archive_title', $title );

		if ( ! empty( $title ) ) {
			echo $before . $title . $after;
		}
	}
endif;