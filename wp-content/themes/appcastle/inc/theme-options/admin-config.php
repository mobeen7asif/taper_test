<?php

/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: https://docs.reduxframework.com
 * */

if ( ! class_exists( 'Redux_Framework_sample_config' ) ) {

	class Redux_Framework_sample_config {

		public $args = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if ( ! class_exists( 'ReduxFramework' ) ) {
				return;
			}

			// This is needed. Bah WordPress bugs.  ;)
			if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
				$this->initSettings();
			} else {
				add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
			}

		}

		public function initSettings() {

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			// $this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}

			$this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
		}



		/**
		 *
		 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
		 * */
		function change_arguments( $args ) {
			//$args['dev_mode'] = true;

			return $args;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
				remove_filter( 'plugin_row_meta', array(
					ReduxFrameworkPlugin::instance(),
					'plugin_metalinks'
				), null, 2 );

				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
			}
		}



		public function setSections() {


			/**********************************
			 ********* Header ***********
			 ***********************************/

			$this->sections[] = array(
				'title'      => esc_html__( 'Header', 'appcastle' ),
				'icon'       => 'el-hdd',
				'icon_class' => 'el-icon-large',
				'fields'     => array(

					array(
						'id'       => 'menu-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Menu Show', 'appcastle' ),
						'subtitle' => esc_html__( 'Show or scroll menu show', 'appcastle' ),
						'default'  => true,
						'on'  => 'Show',
						'off'  => 'Scroll Show',
					),
					array(
						'id'       => 'fixed-menu',
						'type'     => 'switch',
						'title'    => esc_html__( 'Menu Position', 'appcastle' ),
						'subtitle' => esc_html__( 'Select Fix or normal menu option', 'appcastle' ),
						'default'  => true,
						'on'  => 'Normal',
						'off'  => 'Fix',
						'required' => array( 'menu-en', "=", 1 ),
					),

					array(
						'id'          => 'header-background-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Background Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a header background color.', 'appcastle' ),
						'default'     => '#2180c5',
						'validate'    => 'color',
						'transparent' => false,
					),

					array(
						'id'          => 'header-text-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a header text color.', 'appcastle' ),
						'default'     => '#fff',
						'validate'    => 'color',
						'transparent' => false,
					),


				)
			);


			/**********************************
			 ********* Logo & Favicon ***********
			 ***********************************/

			$this->sections[] = array(
				'title'      => esc_html__( 'All Logo & favicon', 'appcastle' ),
				'icon'       => 'el-icon-leaf',
				'icon_class' => 'el-icon-large',
				'fields'     => array(

					array(
						'id'       => 'favicon',
						'type'     => 'media',
						'desc'     => esc_html__( 'upload favicon image', 'appcastle' ),
						'title'    => esc_html__( 'Favicon', 'appcastle' ),
						'subtitle' => esc_html__( 'Upload favicon image', 'appcastle' ),
						'default'  => array( 'url' => get_template_directory_uri() . '/assets/images/favicon.png' ),
					),
					array(
						'id'       => 'logo-text-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Text Type Logo', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable text type logo', 'appcastle' ),
						'default'  => false,
					),
					array(
						'id'       => 'logo-text',
						'type'     => 'text',
						'title'    => esc_html__( 'Logo Text', 'appcastle' ),
						'subtitle' => esc_html__( 'Use your Custom logo text Ex. AppCastle', 'appcastle' ),
						'default'  => 'AppCastle',
						'required' => array( 'logo-text-en', "=", 1 ),
					),
					array(
						'id'       => 'logo',
						'url'      => false,
						'type'     => 'media',
						'title'    => esc_html__( 'Logo', 'appcastle' ),
						'default'  => array( 'url' => get_template_directory_uri() . '/assets/images/logo.png' ),
						'subtitle' => esc_html__( 'Upload your custom site logo.', 'appcastle' ),
						'desc'     => esc_html__( 'upload your custom site logo.', 'appcastle' ),
						'required' => array( 'logo-text-en', "=", 0 ),
					),



				)
			);


			/**********************************
			 ********* Typography ***********
			 ***********************************/

			$this->sections[] = array(
				'icon'       => 'el-icon-font',
				'icon_class' => 'el-icon-large',
				'title'      => esc_html__( 'Typography', 'appcastle' ),
				'fields'     => array(

					array(
						'id'             => 'body-font',
						'type'           => 'typography',
						'title'          => esc_html__( 'Body Font', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						//'font-size'     => ture,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'body' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Body Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '400',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '15px'
						),
					),

					array(
						'id'             => 'headings-font_h1',
						'type'           => 'typography',
						'title'          => esc_html__( 'Headings Font h1', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						'font-size'      => true,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'h1' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Headings Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '500',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '36px'
						),
					),
					array(
						'id'             => 'headings-font_h2',
						'type'           => 'typography',
						'title'          => esc_html__( 'Headings Font h2', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						'font-size'      => true,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'h2' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Headings Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '500',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '24px'
						),
					),
					array(
						'id'             => 'headings-font_h3',
						'type'           => 'typography',
						'title'          => esc_html__( 'Headings Font h3', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						'font-size'      => true,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'h3' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Headings Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '500',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '20px'
						),
					),
					array(
						'id'             => 'headings-font_h4',
						'type'           => 'typography',
						'title'          => esc_html__( 'Headings Font h4', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						'font-size'      => true,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'h4' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Headings Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '500',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '18px'
						),
					),
					array(
						'id'             => 'headings-font_h5',
						'type'           => 'typography',
						'title'          => esc_html__( 'Headings Font h5', 'appcastle' ),
						'compiler'       => false,
						// Use if you want to hook in your own CSS compiler
						'google'         => true,
						// Disable google fonts. Won't work if you haven't defined your google api key
						'font-backup'    => false,
						// Select a backup non-google font in addition to a google font
						'font-style'     => true,
						// Includes font-style and weight. Can use font-style or font-weight to declare
						'subsets'        => true,
						// Only appears if google is true and subsets not set to false
						'font-size'      => true,
						// 'text-align'    => false,
						'line-height'    => false,
						'word-spacing'   => false,
						// Defaults to false
						'letter-spacing' => false,
						// Defaults to false
						'color'          => true,
						'preview'        => true,
						// Disable the previewer
						'all_styles'     => true,
						// Enable all Google Font style/weight variations to be added to the page
						'output'         => array( 'h5' ),
						'units'          => 'px',
						// Defaults to px
						'subtitle'       => esc_html__( 'Select your website Headings Font', 'appcastle' ),
						'default'        => array(
							'color'       => '#454545',
							'font-weight' => '500',
							'font-family' => 'Raleway',
							'google'      => true,
							'font-size'   => '16px'
						),
					),

				)
			);




			/**********************************
			 ********* General Settings ***********
			 ***********************************/

			$this->sections[] = array(
				'icon'       => 'el-icon-cog',
				'icon_class' => 'el-icon-large',
				'title'      => esc_html__( 'General Settings', 'appcastle' ),
				'fields'     => array(

					array(
						'id'       => 'pre-loader-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Pre-Loader', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable pre-loader', 'appcastle' ),
						'default'  => true,
					),

					array(
						'id'       => 'scroll-up-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Scroll to up', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable Scroll to up button', 'appcastle' ),
						'default'  => true,
					),

					array(
						'id'          => 'scroll-up-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Scroll up background color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a Scroll up background color.', 'appcastle' ),
						'default'     => '#000',
						'validate'    => 'color',
						'transparent' => false,
						'required' => array( 'scroll-up-en', "=", 1 ),
					),

				)
			);



			/**********************************
			 ********* Styling ***********
			 ***********************************/

			$this->sections[] = array(
				'icon'       => 'el-icon-brush',
				'icon_class' => 'el-icon-large',
				'title'      => esc_html__( 'Styling', 'appcastle' ),
				'fields'     => array(

					array(
						'id'          => 'body-background',
						'type'        => 'background',
						'output'      => array( 'body' ),
						'title'       => esc_html__( 'Body Background', 'appcastle' ),
						'subtitle'    => esc_html__( 'You can set Background color or images or patterns for site body tag', 'appcastle' ),
						'default'     => '#fff',
						'transparent' => false,
					),
					array(
						'id'          => 'theme-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Theme Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a theme color.', 'appcastle' ),
						'default'     => '#3a88c7',
						'validate'    => 'color',
						'transparent' => false,
					),
					array(
						'id'          => 'hover-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Hover Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a hover color.', 'appcastle' ),
						'default'     => '#3a88c7',
						'validate'    => 'color',
						'transparent' => false,
					),
					array(
						'id'          => 'breadcrumbs-background',
						'type'        => 'color',
						'title'       => esc_html__( 'Breadcrumbs Background', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a breadcrumbs background color.', 'appcastle' ),
						'default'     => '#3a3a63',
						'validate'    => 'color',
						'transparent' => false,
					),
					array(
						'id'          => 'breadcrumbs-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Breadcrumbs Text Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a breadcrumbs text color.', 'appcastle' ),
						'default'     => '#fff',
						'validate'    => 'color',
						'transparent' => false,
					),
					array(
						'id'          => 'contact-button-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Contact Button Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a form button background color.', 'appcastle' ),
						'default'     => '#3a3a63',
						'validate'    => 'color',
						'transparent' => false,
					),

				)
			);


			/**********************************
			 ********* Blog  ***********
			 ***********************************/

			$this->sections[] = array(
				'icon'       => 'el-icon-edit',
				'icon_class' => 'el-icon-large',
				'title'      => esc_html__( 'Blog', 'appcastle' ),
				'fields'     => array(

					array(
						'id'       => 'post-author-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post Author', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable post author', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'post-date-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post Date', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable post date', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'post-view-count-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post View Count', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable post view count', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'post-comment-count-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post Comment Count', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable post comment count', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'post-tags-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post Tags', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable tags', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'post-comment-box-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Post Comment Box', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable post comment box', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'related-post-en',
						'type'     => 'switch',
						'title'    => esc_html__( 'Related Post', 'appcastle' ),
						'subtitle' => esc_html__( 'Enable or disable related post', 'appcastle' ),
						'default'  => true,
					),
					array(
						'id'       => 'related-post-title',
						'type'     => 'text',
						'title'    => esc_html__( 'Related Post Title', 'appcastle' ),
						'subtitle' => esc_html__( 'Type related post section title', 'appcastle' ),
						'default'  => esc_html__( 'You might also like', 'appcastle' ),
						'required' => array( 'related-post-en', "=", 1 ),
					),


				)
			);



			/**********************************
			 ********* Footer  ***********
			 ***********************************/

			$this->sections[] = array(
				'icon'       => 'el-icon-bookmark',
				'icon_class' => 'el-icon-large',
				'title'      => esc_html__( 'Footer', 'appcastle' ),
				'fields'     => array(

					array(
						'id'          => 'footer-text-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Text Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a footer text color (default: #fff).', 'appcastle' ),
						'default'     => '#fff',
						'validate'    => 'color',
						'transparent' => false,
					),

					array(
						'id'          => 'footer-background-color',
						'type'        => 'color',
						'title'       => esc_html__( 'Background Color', 'appcastle' ),
						'subtitle'    => esc_html__( 'Pick a footer background color (default: #3a3a63).', 'appcastle' ),
						'default'     => '#3a3a63',
						'validate'    => 'color',
						'transparent' => false,
					),

					array(
						'id'       => 'copyright-text',
						'type'     => 'editor',
						'title'    => esc_html__( 'Copyright Text', 'appcastle' ),
						'subtitle' => esc_html__( 'Add Copyright Text', 'appcastle' ),
						'default'  => esc_html__( 'AppCastle &copy; 2016. Developed By ShapedTheme', 'appcastle' ),
					),

				)
			);


			/**********************************
			 ********* Import / Export ***********
			 ***********************************/

			$this->sections[] = array(
				'title'  => esc_html__( 'Import / Export', 'appcastle' ),
				'desc'   => esc_html__( 'Import and Export your Theme Options settings from file, text or URL.', 'appcastle' ),
				'icon'   => 'el-icon-refresh',
				'fields' => array(
					array(
						'id'         => 'opt-import-export',
						'type'       => 'import_export',
						'title'      => esc_html__( 'Import Export', 'appcastle' ),
						'subtitle'   => esc_html__( 'Save and restore your Redux options', 'appcastle' ),
						'full_width' => false,
					),
				),
			);

		}


		/**
		 *
		 * All the possible arguments for Redux.
		 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		 * */
		public function setArguments() {

			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name'           => 'appcastle_option',
				// This is where your data is stored in the database
				// and also becomes your global variable name.
				'display_name'       => $theme->get( 'Name' ),
				// Name that appears at the top of your panel
				'display_version'    => $theme->get( 'Version' ),
				// Version that appears at the top of your panel
				'menu_type'          => 'menu',
				//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'     => true,
				// Show the sections below the admin menu item or not
				'menu_title'         => esc_html__( 'Theme Options', 'appcastle' ),
				'page_title'         => esc_html__( 'Theme Options', 'appcastle' ),
				// You will need to generate a Google API key to use this feature.
				// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
				'google_api_key'     => '',
				// Must be defined to add google fonts to the typography module

				'async_typography'   => false,
				// Use a asynchronous font on the front end or font string
				'admin_bar'          => true,
				// Show the panel pages on the admin bar
				'global_variable'    => '',
				// Set a different name for your global variable other than the opt_name
				'dev_mode'           => false,
				// Show the time the page took to load, etc
				'customizer'         => true,
				// Enable basic customizer support
				//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
				//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

				// OPTIONAL -> Give you extra features
				'page_priority'      => null,
				// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent'        => 'themes.php',
				// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
				'page_permissions'   => 'manage_options',
				// Permissions needed to access the options panel.
				'menu_icon'          => '',
				// Specify a custom URL to an icon
				'last_tab'           => '',
				// Force your panel to always open to a specific tab (by id)
				'page_icon'          => 'icon-themes',
				// Icon displayed in the admin panel next to your menu_title
				'page_slug'          => '_options',
				// Page slug used to denote the panel
				'save_defaults'      => true,
				// On load save the defaults to DB before user clicks save or not
				'default_show'       => false,
				// If true, shows the default value next to each field that is not the default value.
				'default_mark'       => '',
				// What to print by the field's title if the value shown is default. Suggested: *
				'show_import_export' => true,
				// Shows the Import/Export panel when not used as a field.

				// CAREFUL -> These options are for advanced use only
				'transient_time'     => 60 * MINUTE_IN_SECONDS,
				'output'             => true,
				// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag'         => true,
				// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
				// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database'           => '',
				// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'system_info'        => false,
				// REMOVE

				// HINTS
				'hints'              => array(
					'icon'          => 'icon-question-sign',
					'icon_position' => 'right',
					'icon_color'    => 'lightgray',
					'icon_size'     => 'normal',
					'tip_style'     => array(
						'color'   => 'light',
						'shadow'  => true,
						'rounded' => false,
						'style'   => '',
					),
					'tip_position'  => array(
						'my' => 'top left',
						'at' => 'bottom right',
					),
					'tip_effect'    => array(
						'show' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'mouseover',
						),
						'hide' => array(
							'effect'   => 'slide',
							'duration' => '500',
							'event'    => 'click mouseleave',
						),
					),
				)
			);
		}

	}

	global $reduxConfig;
	$reduxConfig = new Redux_Framework_sample_config();
}