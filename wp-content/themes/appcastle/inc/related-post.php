<!-- Related Posts -->
<?php global $appcastle_option, $post;

$orig_post = $post;

$tags = wp_get_post_tags($post->ID);

if ($tags) { ?>

<div class="related-post-section">
    <h1 class="st-section-title"><?php echo esc_html($appcastle_option['related-post-title']); ?></h1>
    <div class="row masonry_area">
        <?php

        if ($tags) {
            $tag_ids = array();
            foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
            $args=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=> 3, // Number of related posts to display.
                'ignore_sticky_posts' => 1,
            );

            $related_query = new wp_query( $args );

            while( $related_query->have_posts() ) {
                $related_query->the_post();
                ?>

                <div class="col-sm-4">
                    <div class="post medium-post">
                        <?php if(has_post_thumbnail()){ ?>
                            <div class="entry-header">
                                <div class="entry-thumbnail">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('appcastle-full', array('class' => 'img-responsive')); ?></a>
                                </div>
                            </div>
                        <?php }?>

                        <div class="post-content">
                            <div class="entry-meta">
                                <?php appcastle_related_posted_on(); ?>
                            </div>
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                        </div>
                    </div><!--/post-->
                </div>

            <?php }
        }

        ?>
    </div>
</div><!--/.section -->

<?php } $post = $orig_post; ?>