<?php

if ( ! function_exists( 'appcastle_color_theme' ) ) :

	function appcastle_color_theme() {
		wp_enqueue_style(
			'appcastle-custom-style', get_template_directory_uri() . '/assets/css/custom-style.css'
		);

		global $appcastle_option;
		$theme_color = esc_html( $appcastle_option['theme-color'] );
		$breadcrumbs_background = esc_html( $appcastle_option['breadcrumbs-background'] );
		$breadcrumbs_text_color = esc_html( $appcastle_option['breadcrumbs-color'] );
		$hover_color = esc_html( $appcastle_option['hover-color'] );
		$header_background = esc_html( $appcastle_option['header-background-color'] );
		$header_text_color = esc_html( $appcastle_option['header-text-color'] );
		$scroll_up_color = esc_html( $appcastle_option['scroll-up-color'] );
		$contact_button_color = esc_html( $appcastle_option['contact-button-color'] );

		// Footer
		$footer_text_color = esc_html( $appcastle_option['footer-text-color'] );
		$footer_background_color = esc_html( $appcastle_option['footer-background-color'] );

		$custom_css = "";

		if ($appcastle_option['fixed-menu'] == false ) {
			$custom_css .= "
				.breadcrumbs-section{
					margin-top: 60px;
				}
			";
		}
		$custom_css .= "

			/* Background Color */
			.sticky sup.featured-post, a.more-link, .pagination li span.current,
			.pagination li a:hover,
			.pagination .active > a,
			.pagination li a:focus,
			.pagination .active > a:hover,
			.pagination li span:hover,
			.widget .tagcloud a:hover, .single-post-tags .tags-links a:hover,
			h1.widget-title:before, .st-section-title:before, .comment-reply-title:before,
			#commentform input.submit,
			input[type='submit'],
			#wp-calendar tbody td:hover,
			#blog-gallery-slider .carousel-control.left,
			#blog-gallery-slider .carousel-control.right{
				background-color: {$theme_color};
			}


			/* Border Color */
			.pagination li span.current,
			.pagination li a:hover,
			.pagination .active > a,
			.pagination li a:focus,
			.pagination .active > a:hover,
			.pagination li span:hover{
				border-color: {$theme_color};
			}


			/* Header */
			.main-navbar,
			.navbar .dropdown .dropdown-menu,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover{
				background-color: {$header_background};
			}
			.navbar-nav li a,
			.navbar .dropdown .dropdown-menu a,
			.nav .open>a, .nav .open>a:focus, .nav .open>a:hover{
				color: {$header_text_color};
			}


			/* Hover Color */
			a:hover, .widget a:hover, a#cancel-comment-reply-link,
			#wp-calendar tfoot a,
			#wp-calendar tbody td a,
			.post-content .entry-meta a:hover,
			#footer-bottom a:hover{
				color: {$hover_color};
			}

			#contact-form .btn-primary{
				background-color: {$contact_button_color};
			}
			#contact-form .btn-primary:hover, #contact-form .btn-primary:focus {
			    box-shadow: 0px 0px 0px 1px {$contact_button_color};
			    border-color: {$contact_button_color};
			}

			.scroll-up a{
				background-color: {$scroll_up_color};
			}

			/* Breadcrumbs */
			.breadcrumbs-section{
				background-color: {$breadcrumbs_background};
				color: {$breadcrumbs_text_color};
			}
			/* Footer */
			div#footer-bottom{
				background-color: {$footer_background_color};
				color: {$footer_text_color};
			}
			#footer-bottom a{
				color: {$footer_text_color};
			}

         ";
		wp_add_inline_style( 'appcastle-custom-style', $custom_css );
	}

	add_action( 'wp_enqueue_scripts', 'appcastle_color_theme' );

endif;
