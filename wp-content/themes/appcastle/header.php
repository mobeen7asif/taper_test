
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php global $appcastle_option; ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php  if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		if(isset($appcastle_option['favicon'])){  ?>
			<link rel="shortcut icon" href="<?php echo esc_url($appcastle_option['favicon']['url']); ?>" type="image/x-icon"/>
		<?php }else{ ?>
			<link rel="shortcut icon" href="<?php echo esc_url(get_template_directory_uri().'/assets/images/favicon.png'); ?>" type="image/x-icon"/>
		<?php }
	}
	?>

	<?php wp_head(); ?>
</head><!--/head-->
<body <?php body_class(); ?>>

<?php if($appcastle_option['pre-loader-en'] == 1) : ?>
<div id="st-preloader">
	<div id="pre-status">
		<div class="preload-placeholder"></div>
	</div>
</div>
<!-- /#st-preloader -->
<?php endif; ?>

<header id="navigation" class="<?php if($appcastle_option['menu-en'] == 0 ) :?><?php endif;
?>">
	<div class="navbar main-navbar <?php if($appcastle_option['menu-en'] == 0 || $appcastle_option['fixed-menu'] == 0) :?>navbar-fixed-top <?php
	endif; ?>" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only"><?php esc_html_e('Toggle navigation', 'appcastle') ?></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
					<?php
					if(isset($appcastle_option['logo'])){
						if($appcastle_option['logo-text-en']){ ?>
							<h1 class="logo-text"><?php echo esc_html($appcastle_option['logo-text']); ?></h1>
						<?php }else{
							if(!empty($appcastle_option['logo']['url'])){ ?>
								<img class="img-responsive" src="<?php echo esc_url($appcastle_option['logo']['url']); ?>" alt="Logo">
							<?php }else{
								echo esc_html(get_bloginfo('name'));
							}
						}
					}else{
						echo esc_html(get_bloginfo('name'));
					}
					?>
				</a>
			</div>
			<nav id="main-menu" class="collapse navbar-collapse navbar-right">
				<?php if(has_nav_menu('primary')): ?>
					<?php wp_nav_menu( array(
						'theme_location' => 'primary',
						'container' => false,
						'menu_class' => 'nav navbar-nav',
						'walker' => new wp_bootstrap_navwalker()
					) ); ?>
				<?php endif; ?>
			</nav>
		</div>
	</div>
</header><!--/#navigation-->