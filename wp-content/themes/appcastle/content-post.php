<div class="entry-blog">
	<?php global $appcastle_option; ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-header">

			<!-- Gallery Post -->
			<?php if(has_post_format('gallery')) : ?>
				<?php if ( function_exists( 'rwmb_meta' ) ) : ?>
					<?php $slides = rwmb_meta('appcastle_gallery_images','type=image_advanced'); ?>
					<?php $count = count($slides); ?>
					<?php if($count > 0): ?>
						<div class="entry-thumbnail">
							<div id="blog-gallery-slider" class="carousel slide" data-ride="carousel">

								<!-- Wrapper for slides -->
								<div class="carousel-inner">

									<?php $slide_no = 1; ?>

									<?php foreach( $slides as $slide ): ?>
										<div class="item <?php if($slide_no == 1){ echo 'active'; }; ?>">
											<?php $images = wp_get_attachment_image_src( $slide['ID'], 'appcastle-full' ); ?>
											<img src="<?php echo $images[0]; ?>" alt="">
										</div>
										<?php $slide_no++ ?>
									<?php endforeach; ?>

								</div>

								<!-- Controls -->
								<a class="left carousel-control" href="#blog-gallery-slider" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a class="right carousel-control" href="#blog-gallery-slider" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</div>
					<?php endif; ?>
				<?php endif; ?>


				<!-- Audio Post -->
			<?php elseif(has_post_format('audio')) : ?>
				<?php if ( function_exists( 'rwmb_meta' ) ) { ?>
					<div class="entry-thumbnail">
						<div class="entry-audio">
							<?php echo rwmb_meta( 'appcastle_audio_code' ); ?>
						</div> <!--/.audio-content -->
					</div> <!--/.entry-thumbnail -->
				<?php } ?>


				<!-- Video Post -->
			<?php elseif(has_post_format('video')) : ?>
				<?php if ( function_exists( 'rwmb_meta' ) ) { ?>
					<div class="entry-thumbnail">
						<div class="entry-video">
							<?php $video_source = rwmb_meta( 'appcastle_video_source' ); ?>
							<?php $video = rwmb_meta( 'appcastle_video' ); ?>

							<?php if($video_source == 1): ?>
								<?php echo rwmb_meta( 'appcastle_video' ); ?>
							<?php elseif ($video_source == 2): ?>
								<?php echo '<iframe width="550" height="310" src="http://www.youtube.com/embed/'.$video.'?rel=0&showinfo=0&modestbranding=1&hd=1&autohide=1&color=white" frameborder="0" allowfullscreen></iframe>'; ?>
							<?php elseif ($video_source == 3): ?>
								<?php echo '<iframe src="http://player.vimeo.com/video/'.$video.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="550" height="310" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'; ?>
							<?php endif; ?>
						</div>
					</div>
				<?php } ?>


				<!-- Standard Post -->
			<?php else : ?>

				<?php if(has_post_thumbnail()) : ?>
					<div class="entry-thumbnail">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('appcastle-full', array('class' => 'img-responsive')); ?></a>
					</div>
				<?php endif; ?>

			<?php endif; ?>
		</div>


		<div class="post-content">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			<?php if ( is_sticky() && is_home() && ! is_paged() ) { ?>
				<sup class="featured-post" title="<?php esc_html_e('Sticky Post', 'appcastle') ?>"><i class="fa
				fa-thumb-tack"></i></sup>
			<?php } ?>
			<div class="entry-meta">
				<?php appcastle_posted_on(); ?>
			</div>
			<div class="entry-content">
				<?php the_content(esc_html__('Read More', 'appcastle'));

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'appcastle' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
				?>
			</div>
		</div>
	</div><!--/post-->
</div>
