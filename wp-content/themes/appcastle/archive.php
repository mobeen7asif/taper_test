<?php get_header(); ?>

	<div class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-title"><?php appcastle_archive_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="site-content">
						<?php if(have_posts()){
							while(have_posts()) : the_post();
								get_template_part('content', 'post');
							endwhile;

							appcastle_posts_pagination();

						}else{
							get_template_part('content', 'none');
						} ?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
