<form role="search" method="get" id="search-form" action="<?php echo esc_url(site_url()); ?>">
	<input type="text" placeholder="<?php esc_html_e('Search on site', 'appcastle'); ?>" name="s" id="s" />
</form>