<?php
/*
 * Template Name: Home Page
 */
get_header('alternative'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="container">
		<?php the_content(); ?>
	</div> <!-- .container -->

<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>