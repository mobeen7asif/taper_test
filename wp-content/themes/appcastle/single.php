<?php get_header();
global $appcastle_option; ?>

	<div class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="entry-single-blog">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part('content', 'single'); ?>

							<?php if($appcastle_option['related-post-en']==1){
								get_template_part('inc/related-post');
							} ?>

							<?php if($appcastle_option['post-comment-box-en']==1){
								// If comments are open or we have at least one comment, load up the comment template
								if (comments_open() || '0' != get_comments_number()) {
									comments_template();
								}
							}?>

							<?php
							if ( is_singular( 'post' ) ){
								$count_post = esc_attr( get_post_meta( $post->ID, '_post_views_count', true) );
								if( $count_post == ''){
									$count_post = 1;
									add_post_meta( $post->ID, '_post_views_count', $count_post);
								}else{
									$count_post = (int)$count_post + 1;
									update_post_meta( $post->ID, '_post_views_count', $count_post);
								}
							}
							?>

						<?php endwhile; // end of the loop. ?>

						<?php else : ?>

							<?php get_template_part('content', 'none'); ?>

						<?php endif; ?>

					</div>
				</div>

				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>