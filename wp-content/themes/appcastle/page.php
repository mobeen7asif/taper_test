<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="page-area">
						<?php if(has_post_thumbnail()) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail('appcastle-full', array('class' => 'img-responsive')); ?>
							</div>
						<?php endif; ?>
						<div class="page-content entry-content">
							<?php
							the_content();

							wp_link_pages( array(
								'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'appcastle' ) . '</span>',
								'after'       => '</div>',
								'link_before' => '<span>',
								'link_after'  => '</span>',
							) );
							?>
						</div>
						<?php
						// If comments are open or we have at least one comment, load up the comment template
						if (comments_open() || '0' != get_comments_number()) {
							comments_template();
						}
						?>
					</div>
				</div>

				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>