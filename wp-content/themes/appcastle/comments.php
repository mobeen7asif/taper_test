<?php
/* This comments template */
if ( post_password_required() ) {
	return;
}
?>
<div id="comments" class="comments-wrapper comments">
	<?php if ( have_comments() ) : ?>
		<h1 class="st-section-title">
			<?php comments_number( esc_html__( 'No Comment', 'appcastle' ), esc_html__( '1 Comment', 'appcastle' ), esc_html__( '% Comments', 'appcastle' ) ); ?>
		</h1>
		<ul class="media-list">

			<?php
			wp_list_comments( array(
				'style'       => 'ul',
				'short_ping'  => true,
				'callback'    => 'appcastle_comment',
				'avatar_size' => 50
			) );
			?>
		</ul><!-- .media-list -->

		<?php
		// Are there comments to navigate through?
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="navigation comment-navigation" role="navigation">
				<h1 class="screen-reader-text section-heading"><?php esc_html_e( 'Comment navigation', 'appcastle' ); ?></h1>

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'appcastle' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'appcastle' ) ); ?></div>
			</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'appcastle' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
	$commenter = wp_get_current_commenter();
	$req       = sanitize_email( get_option( 'require_name_email' ) );
	$aria_req  = ( $req ? " aria-required='true'" : '' );
	$fields    = array(
		'author' => '<div class="col-sm-4"><div class="form-group"><label for="author">' . esc_html__( 'Name', 'appcastle' ) . '</label><input id="author"
name="author" class="form-control" type="text"' . esc_attr( $aria_req ) . '/></div></div>',
		'email'  => '<div class="col-sm-4"><div class="form-group"><label for="email">' . esc_html__( 'Email', 'appcastle' ) . '</label><input id="email" name="email" class="form-control" type="text"' . esc_attr( $aria_req ) . '/></div></div>',
		'url'    => '<div class="col-sm-4"><div class="form-group"><label for="url">' . esc_html__( 'Website', 'appcastle' ) . '</label><input id="url" name="url" class="form-control" type="text" /></div></div>',
	);


	$comments_args = array(
		'fields'               => $fields,
		'comment_notes_before' => '',
		'comment_notes_after'  => '',
		'comment_field'        => '<div class="col-sm-12"><div class="form-group"><label for="comment">' . esc_html__( 'Comment', 'appcastle' ) . '</label><textarea id="comment"
 name="comment" class="form-control" rows="5" aria-required="true"></textarea></div></div>',
		'label_submit'         =>  esc_html__( 'Send Comment', 'appcastle' )
	);
	ob_start();
	comment_form( $comments_args );
	$search  = array( 'class="comment-form"', 'class="form-submit"' );
	$replace = array( 'class="comment-form"', 'class="form-submit"' );
	echo str_replace( $search, $replace, ob_get_clean() );
	?>


</div>