<?php global $appcastle_option; ?>

	<?php if(isset($appcastle_option['copyright-text']) && !empty($appcastle_option['copyright-text'])) : ?>
		<div id="footer-bottom">
			<div class="container text-center">
				<p><?php echo balanceTags($appcastle_option['copyright-text']); ?></p>
			</div>
		</div>
	<?php endif; ?>

	<?php if($appcastle_option['scroll-up-en'] == 1) : ?>
		<div class="scroll-up">
			<a href="#"><i class="fa fa-angle-up"></i></a>
		</div>
	<?php endif; ?>

	<?php wp_footer(); ?>
</body>
</html>
