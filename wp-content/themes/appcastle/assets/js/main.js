jQuery(function ($) {

    'use strict';

	/*==============================================================*/
    // Menu add class
    /*==============================================================*/
	(function () {
		function menuToggle(){
			$(window).on('scroll', function(){
				if( $(window).scrollTop()>61 ){
					$('#navigation.menu-hide-page').removeClass('menu-hide');
				} else {
					$('#navigation.menu-hide-page').addClass('menu-hide');
				}
			});
		}
		menuToggle();
	}());


	/*==============================================================*/
	//jquery scroll spy
	/*==============================================================*/
	$("body").scrollspy({
		target: ".navbar-collapse",
		offset: 95
	});

	// jQuery smooth scroll
	$(".navbar-collapse li a[href*=#]").on("click", function (event) {
		var $anchor = $(this);
		var headerH = "50";
		$("html, body")
			.stop()
			.animate({
				scrollTop: $($anchor.attr("href"))
					.offset()
					.top - headerH + "px"
			}, 1200, "easeOutCirc");

		event.preventDefault();
	});


	/*==============================================================*/
	// Magnific Popup
	/*==============================================================*/

	(function () {
		$('.image-link').magnificPopup({
			gallery: {
				enabled: true
			},
			type: 'image'
		});
		$('.feature-image .image-link').magnificPopup({
			gallery: {
				enabled: false
			},
			type: 'image'
		});
		$('.image-popup').magnificPopup({
			type: 'image'
		});
		$('.video-link').magnificPopup({type:'iframe'});
	}());


	/*==============================================================*/
	// Fitvids
	/*==============================================================*/
	(function () {
		$(".wpb_wrapper").fitVids();
		$(".entry-content").fitVids();
		$(".entry-video").fitVids();
	}());


	/*==============================================================*/
	// Preloader
	/*==============================================================*/
	(function () {
		$(window).load(function() {
			$('#pre-status').fadeOut();
			$('#st-preloader').delay(350).fadeOut('slow');
		});
	}());

	/*==============================================================*/
	// Back To Top
	/*==============================================================*/
	(function () {
		$(window).scroll(function() {
			if ($(this).scrollTop() > 150) {
				$('.scroll-up').fadeIn();
			} else {
				$('.scroll-up').fadeOut();
			}
		});
		$('.scroll-up a').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
	}());
	
	
	
});

