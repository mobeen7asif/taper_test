<?php get_header(); ?>

	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<section class="error-404 not-found text-center">
						<i class="fa fa-frown-o" aria-hidden="true"></i>
						<h1 class="404"><?php esc_html_e('404', 'appcastle') ?></h1>
						<p class="lead"><?php esc_html_e('Sorry, we could not found the page you are looking for!', 'appcastle') ?></p>

						<div class="row">
							<div class="col-sm-4 col-sm-offset-4">
								<?php get_search_form(); ?>
							</div>
						</div>

					</section><!-- .error-404 -->
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>