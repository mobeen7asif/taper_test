<?php
/**
 * Thankyou page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 *
 * @themeskingdom
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<h3 class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></h3>

			<h3 class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php
				if ( is_user_logged_in() )
					_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
				else
					_e( 'Please attempt your purchase again.', 'woocommerce' );
				?></h3>

			<p>
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
			<div class="order-overview-wrap">

				<h3 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
					<?php
						echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you for your purchase.', 'woocommerce' ), $order );
						printf( '<span>%s</span>', __( 'Below are your Order Details', 'woocommerce' ) );
					?>
				</h3>

				<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
					<li class="woocommerce-order-overview__order order">
						<?php _e( 'Order number:', 'woocommerce' ); ?>
						<strong><?php printf( $order->get_order_number() ); ?></strong>
					</li>
					<li class="woocommerce-order-overview__date date">
						<?php _e( 'Date:', 'woocommerce' ); ?>
						<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
					</li>
					<li class="woocommerce-order-overview__total total">
						<?php _e( 'Total:', 'woocommerce' ); ?>
						<strong><?php printf( $order->get_formatted_order_total() ); ?></strong>
					</li>

					<?php if ( $order->get_payment_method_title() ) : ?>

					<li class="woocommerce-order-overview__method method">
						<?php _e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>

					<?php endif; ?>

				</ul>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<h3 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you for your purchase.', 'woocommerce' ), null ); ?></h3>

	<?php endif; ?>

	</div>
