<?php
/**
 * Customizer Header WooCommerce
 *
 * Here you can define WC Header settings
 *
 * @package  Goodz
 */

/* --- Section --- */

// Shop Layout Section
$wp_customize->add_section( 'header_woocommerce_section', array(
    'title' => esc_html__( 'Shop Header Settings', 'goodz' ),
    'panel' => 'wc_page_panel'
) );

/* --- Settings --- */

// Display account and wishlist links in header
$wp_customize->add_setting( 'wc_header_settings', array(
    'default'           => 1,
    'sanitize_callback' => 'goodz_sanitize_select'
) );

$wp_customize->add_control( 'wc_header_settings', array(
    'settings' => 'wc_header_settings',
    'priority' => 0,
    'label'    => esc_html__( 'Display "My Account" and "Saved Items" links in header', 'goodz' ),
    'section'  => 'header_woocommerce_section',
    'type'     => 'checkbox'
) );

// Display minicart in header
$wp_customize->add_setting( 'wc_header_cart_settings', array(
    'default'           => 1,
    'sanitize_callback' => 'goodz_sanitize_select'
) );

$wp_customize->add_control( 'wc_header_cart_settings', array(
    'settings' => 'wc_header_cart_settings',
    'priority' => 1,
    'label'    => esc_html__( 'Display mini cart in header', 'goodz' ),
    'section'  => 'header_woocommerce_section',
    'type'     => 'checkbox'
) );

