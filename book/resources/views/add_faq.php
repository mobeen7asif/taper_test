<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="add-vehicle">
            	<h2 class="main-heading">Add A Faq</h2>
                 <?php if (Session::has('success')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                        <?php echo Session::get('success') ?>
                    </div><?php } ?>
            	<div class="add-vehicle-widget">
                    <form class="add-vehicle-form" method="post" action="<?php echo asset('addfaq')?>">
            			<label class="full-field">
            				<span>Question</span>
                                        <input  type="text" name="question" placeholder="Question" required="">
            			</label>
            			<label class="full-field">
            				<span>Answar</span>
                                        <input type="text" name="answer" placeholder="Answar" required="">
            			</label>
            			<label class="submit">
            				<input type="submit" class="btn btn btn-primary" name="submit" value="Submit">
						</label>
            		</form>
            	</div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <?php include 'includes/js.php'; ?>
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable( {
                    columnDefs: [ {
                            targets: [ 0 ],
                            orderData: [ 0, 1 ]
                    }, {
                            targets: [ 1 ],
                            orderData: [ 1, 0 ]
                    }, {
                            targets: [ 4 ],
                            orderData: [ 4, 0 ]
                    } ]
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
				$('.datetimepicker').datetimepicker({
					onChangeDateTime:logic,
					onShow:logic
				});
				
				var logic = function( currentDateTime ){
					if (currentDateTime && currentDateTime.getDay() == 6){
						this.setOptions({
							minTime:'11:00'
						});
					}else
						this.setOptions({
							minTime:'8:00'
						});
				};
            });
        </script>
    </body>
</html>
