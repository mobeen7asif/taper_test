<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="add-vehicle">
                <h2 class="main-heading">Add A Promotions</h2>
                <?php if (Session::has('success')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
            <?php echo Session::get('success') ?>
        </div><?php } ?>
                <div class="add-vehicle-widget">
                    <form enctype="multipart/form-data" class="add-vehicle-form" method="post" action="<?php echo asset('addpromations') ?>">

                        <label class="full-field">
                            <span>Title</span>
                            <input required="" type="text" name="title" placeholder="Title">
                        </label>
                        <label class="full-field">
                            <span>Image</span>
                            <input type="file" name="image" required="">
                        </label>
                        <label>
                            <span>Dicrpation</span>
                            <textarea required="" name="description" placeholder="Dicrpation"></textarea>
                        </label>
                        <label class="submit">
                            <input  type="submit" class="btn btn btn-primary" name="submit" value="Submit">
                        </label>
                    </form>
                </div>
            </section>
        </main>
        <?php include 'includes/js.php'; ?> 
        <script type="text/javascript" src="<?php echo asset('adminassets/js/jquery.datetimepicker.full.js') ?>"></script>
        <script>
            $(document).ready(function () {
                $('#tableStyle').DataTable({
                    columnDefs: [{
                            targets: [0],
                            orderData: [0, 1]
                        }, {
                            targets: [1],
                            orderData: [1, 0]
                        }, {
                            targets: [4],
                            orderData: [4, 0]
                        }]
                });
                $('header button').click(function () {
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
                $('.datetimepicker').datetimepicker({
                    onChangeDateTime: logic,
                    onShow: logic
                });

                var logic = function (currentDateTime) {
                    if (currentDateTime && currentDateTime.getDay() == 6) {
                        this.setOptions({
                            minTime: '11:00'
                        });
                    } else
                        this.setOptions({
                            minTime: '8:00'
                        });
                };
            });
        </script>
    </body>
</html>
