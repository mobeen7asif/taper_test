<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/sidebar.php'; ?> 

        <main class="main-content">
             <?php include 'includes/header.php'; ?> 
            <section class="home-page">
                <div class="panel-tabs">
                    <ul>
                        <li>
                            <a href="<?php echo asset('promations')?>">
                                <i class="fa fa-bullhorn fw"></i>
                                <h3>Promotions</h3>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo asset('faqs')?>">
                                <i class="fa fa-question-circle fw"></i>
                                <h3>Faqs</h3>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo asset('professionals')?>">
                                <i class="fa fa-cut fw"></i>
                                <h3>All Professionals</h3>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo asset('customers')?>">
                                <i class="fa fa-users fw"></i>
                                <h3>All Customers</h3>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo asset('payments')?>">
                                <i class="fa fa-dollar fw"></i>
                                <h3>All Payments</h3>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <?php include 'includes/js.php';?>
        <script>
            $(document).ready(function () {
                $('#tableStyle').DataTable({
                    columnDefs: [{
                            targets: [0],
                            orderData: [0, 1]
                        }, {
                            targets: [1],
                            orderData: [1, 0]
                        }, {
                            targets: [4],
                            orderData: [4, 0]
                        }]
                });
                $('header button').click(function () {
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
