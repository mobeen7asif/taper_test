<aside class="sidebar">
            <div class="logo">
                <a href="<?php echo asset('dashboard')?>">
                    <img src="<?php echo asset('adminassets/images/logo.png')?>" alt="" width="150">
                </a>
            </div>
            <nav class="main-nav">
                <ul class="tab-list">
                    <li class="active"><a href="<?php echo asset('dashboard')?>">Home</a></li>
                    <li><a href="<?php echo asset('promations')?>">Promotions</a></li>
                    <li><a href="<?php echo asset('faqs')?>">FAQs</a></li>
                    <li><a href="<?php echo asset('professionals')?>">All Professionals</a></li>
                    <li><a href="<?php echo asset('customers')?>">All Customers</a></li>
                    <li><a href="<?php echo asset('payments')?>">All Transactions</a></li>
                    <li><a href="<?php echo asset('professional_types')?>">Professional Types</a></li>
                </ul>
            </nav>
        </aside>