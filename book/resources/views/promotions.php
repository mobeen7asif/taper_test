<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="vehicles-list">
                <div class="vehicles-head">
                    <h3>Promotions</h3>
                    <a href="<?php echo asset('addpromations')?>" class="btn btn-primary pull-right">Add a Promotion</a>
				</div>
                <div class="vehicles-list-content">
                    <div class="vehicles-table">
                        <?php if (Session::has('success')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                        <?php echo Session::get('success') ?>
                    </div><?php } ?>
                        <table id="tableStyle" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($promos as $promo) { ?>
                                <tr>
                                    <td><img src="<?php echo asset('public/images/promo/'.$promo->image)?>" alt=""></td>
                                    <td><?php echo $promo->title;?></td>
                                    <td><?php echo $promo->details;?> </td>
                                    <td>
                                        <a href="<?php echo asset('editpromations/'.$promo->id)?>"><i class="fa fa-edit fa-fw"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#delete_model<?php echo $promo->id;?>"><i class="fa fa-trash fa-fw"></i></a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="delete_model<?php echo $promo->id;?>" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <a class="btn btn-primary" href="<?php echo asset('deletepromo/'.$promo->id)?>">Yes</a>
                                            <a class="btn btn-primary" href="#" data-dismiss="modal">No</a>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
		                    </div>   
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
      <?php include 'includes/js.php'; ?> 
        
		
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable({
                    columnDefs: [{
                        targets: [0],
                        orderData: [0, 1]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }],
                    order: [[ 0, false ]],
                    bSort: false
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
