<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?> 
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="vehicles-list">
                <div class="vehicles-head">
                    <h3>All Barber Images</h3>
                    <?php if (Session::has('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('success') ?>
                        </div><?php } ?>
                    <!--<a href="" class="btn btn-primary pull-right">Export Barbers</a>-->
                    <form id="save_image" method="post" action="<?= asset('add_barber_image')?>" enctype="multipart/form-data">
                        <input type="hidden" name="barber_id" value="<?= $barber_id?>">
                        <input type="hidden"name="_token" value="<?= csrf_token() ?>">
                    <label for="test-input" class="btn btn-primary pull-right">Select Image for Gallery
                        <input id="test-input" type="file" name="photo" style="margin-top: 15px;" accept="image/*"/>
                    </label>
                        </form>
                </div>
                <div class="vehicles-list-content taper-gallery">
                    <?php foreach ($images as $image){ ?>
                    <figure>
                        <div class="fig-img" style="background-image: url(<?php echo asset('public/images/'.$image->image)?>)"></div>
                        <!--<img src="<?php // echo asset('public/images/'.$image->image)?>" alt="Logo">-->
                        <span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#delete_image<?= $image->id?>"></span>
                    </figure>
                    
                    <div class="modal fade" id="delete_image<?= $image->id?>" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        Are You Sure You Want To Delete This Image
                    </div>
                    <div class="modal-body text-center">
                        <a class="btn btn-primary" href="<?= asset('delete_barber_image/'.$image->id)?>">Yes</a>
                        <a class="btn btn-primary" href="#" data-dismiss="modal">No</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>   
                    <?php } ?>
                </div>
            </section>
        </main>
        
        <?php include 'includes/js.php'; ?> 
        <script> 
        $('#test-input').change(function(){
            if($('#test-input').val()){
            $('#save_image').submit();
        }
        }) 
        </script>
    </body>
</html>
