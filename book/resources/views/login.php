<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?> 
    <body>
        <main class="main-content login-page">
            <div class="user-widget">
               	<div class="user-bg">
               		<div class="logo">
						<a href="<?php echo asset('/')?>">
                                                    <img src="<?php echo asset('adminassets/images/logo.png')?>" alt="" width="150">
						</a>
					</div>
          		</div>
            </div>
            <section class="home-page">
                <div class="add-vehicle-widget">
                     <?php if (Session::has('error')) { ?>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                        <?php echo Session::get('error') ?>
                    </div><?php } ?>
                    <form class="add-vehicle-form" method="post" action="<?php echo asset('/');?>">
            			<label>
            				<span>User Name</span>
                                        <input type="text" name="email" placeholder="Email" required="">
            			</label>
            			<label>
            				<span>Password</span>
                                        <input type="password" name="password" placeholder="Password" required="">
            			</label>
            			
            			<label class="submit">
            				<input type="submit" class="btn btn btn-primary" name="Login" value="Submit">
						</label>
            		</form>
            	</div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable( {
                    columnDefs: [ {
                            targets: [ 0 ],
                            orderData: [ 0, 1 ]
                    }, {
                            targets: [ 1 ],
                            orderData: [ 1, 0 ]
                    }, {
                            targets: [ 4 ],
                            orderData: [ 4, 0 ]
                    } ]
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
