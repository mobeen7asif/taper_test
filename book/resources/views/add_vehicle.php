<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Vehicles</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" href="css/bootstrap.min.css" rel="stylesheet" />
        <link type="text/css" href="css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css" />
        <link type="text/css" href="css/theme.css" rel="stylesheet" />
    </head>
    <body>
        <aside class="sidebar">
            <div class="logo">
                <a href="index.html">
                    <img src="images/logo.png" alt="" width="150">
                </a>
            </div>
            <nav class="main-nav">
                <ul class="tab-list">
                    <li><a href="index.html">Home </a></li>
                    <li class="active"><a href="vehicles-list.html">Vehicles </a></li>
                    <li><a href="store.html">Online Store </a></li>
                    <li>
                    	<a href="">Contact a frenchise </a>
                    	<ul>
                    		<li><a href="message.html">Older Messages</a></li>
                    		<li><a href="frenchise.html">Create New Messages</a></li>
                    	</ul>
                    </li>
                    <li><a href="news.html">Newsletters </a></li>
                    <li>
                    	<a href="#">Request Form </a>
                    	<ul>
                    		<li><a href="request-form.html">Create New Request</a></li>
                    		<li><a href="old-request.html">View Older Service Requesr</a></li>
                    	</ul>
                    </li>
                </ul>
            </nav>
        </aside>
        <main class="main-content">
            <div class="user-widget">
               	<div class="user-bg">
					<ul class="ul-items">
						<li><a href="#"><i class="fa fa-facebook fa-fw"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter fa-fw"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin fa-fw"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus fa-fw"></i></a></li>
					</ul>
        			<div class="dropdown user-info">
						<a class="dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">			
							<span>John Thomas</span>
							<figure><img src="images/profile.jpg" alt=""></figure>
							<i class="caret"></i>
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
							<li><a href="#">Edit Profile</a></li>
							<li><a href="#">Messages <span>22</span></a></li>
							<li><a href="#">Logout</a></li>
						</ul>
					</div>
          		</div>
            </div>
            <section class="add-vehicle">
            	<h2 class="main-heading">Add A Vehicle</h2>
            	<div class="add-vehicle-widget">
            		<form class="add-vehicle-form">
            			<label class="half-field">
            				<span>Make</span>
            				<input type="text" name="Make" placeholder="Make">
            			</label>
            			
            			<label class="half-field">
            				<span>Model</span>
            				<input type="text" name="Model" placeholder="Model">
            			</label>
            			
            			<label class="full-field">
            				<span>Vehicle Type</span>
            				<select>
            					<option>Select Vehicle Type</option>
            					<option>Caravan</option>
            					<option>Trailer</option>
            					<option>Horse Float</option>
            					<option>Other</option>
            				</select>
            			</label>
            			
            			<label class="half-field">
            				<span>Year</span>
            				<input type="text" class="datetimepicker" name="year" placeholder="Year"/>
            			</label>
            			
            			<label class="half-field">
            				<span>Year Purchased</span>
            				<input type="text" class="datetimepicker" name="yearPurchased" placeholder="Year Purchased"/>
            			</label>
            			
            			<label class="half-field">
            				<span>Last Service</span>
            				<input type="text" class="datetimepicker" name="lastService" placeholder="Last Service">
            			</label>
            			
            			<label class="half-field">
            				<span>Next Service</span>
            				<input type="text" class="datetimepicker" name="nextService" placeholder="Next Service">
            			</label>
            			
            			<label class="half-field">
            				<span>Registration Number</span>
            				<input type="text" name="registrationNumber" placeholder="Registration Number">
            			</label>
            			
            			<label class="half-field">
            				<span>Registration Expiry</span>
            				<input type="text" class="datetimepicker" name="registrationExpiry" placeholder="Registration Expiry">
            			</label>
            			
            			<label class="half-field">
            				<span>Engine Capacity</span>
            				<input type="text" name="engineCapacity" placeholder="Engine Capacity">
            			</label>
            			
            			<label class="half-field">
            				<span>Number of Axles</span>
            				<input type="text" name="numberAxles" placeholder="Number of Axles">
            			</label>
            			
            			<label>
            				<span>Details</span>
            				<textarea name="" placeholder="Details"></textarea>
            			</label>
            			<label class="submit">
            				<input type="submit" class="btn btn btn-primary" name="submit" value="Submit">
						</label>
            		</form>
            	</div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <script type="text/javascript" language="javascript" src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script>
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable( {
                    columnDefs: [ {
                            targets: [ 0 ],
                            orderData: [ 0, 1 ]
                    }, {
                            targets: [ 1 ],
                            orderData: [ 1, 0 ]
                    }, {
                            targets: [ 4 ],
                            orderData: [ 4, 0 ]
                    } ]
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
				$('.datetimepicker').datetimepicker({
					onChangeDateTime:logic,
					onShow:logic
				});
				
				var logic = function( currentDateTime ){
					if (currentDateTime && currentDateTime.getDay() == 6){
						this.setOptions({
							minTime:'11:00'
						});
					}else
						this.setOptions({
							minTime:'8:00'
						});
				};
            });
        </script>
    </body>
</html>
