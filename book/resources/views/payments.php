<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?> 
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
          <?php include 'includes/header.php'; ?> 
            <section class="vehicles-list">
                <div class="vehicles-head">
                    <h3>All Customers</h3>
				</div>
                <div class="vehicles-list-content">
                        <ul class="total-counts list-none">
                            <li>Total Business<span><?php echo $total?></span></li>

                            <li>Professionals Income <span><?php echo $userincome?></span></li>
                            <li>Pending Payments <span><?php echo $remaining?></span></li>
                            <li>App Payment <span><?php echo $app_earning?></span></li>
                        </ul>
                    <div class="vehicles-table">
                        <table id="tableStyle" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Barber Name</th>
                                    <th>Total Payment</th>
                                    <th>Barber Earning</th>

                                    <th>Pending</th>
                                    <th>Earnings</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($payments as $payment) {  ?>
                                <tr>
                                     <td><?php echo $payment['getBarber']['first_name'].' '.$payment['getBarber']['last_name'];?></td>
                                    <td><?php echo $payment->total_payments;?></td>
                                    <td><?php echo $payment->earning;?></td>
                                    <td><?php echo $payment->app_blance;?></td>

                                    <td><?php echo $payment->total_payments - $payment->earning - $payment->app_blance;?></td>
                                    <td><?php echo date("d-m-Y H:i", strtotime($payment->created_at));?></td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <?php include 'includes/js.php'; ?> 
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable({
                    columnDefs: [{
                        targets: [0],
                        orderData: [0, 1]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }],
                    order: [[ 0, false ]],
                    bSort: false
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
