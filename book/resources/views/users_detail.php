<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?> 
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="vehicle-detail">
                <h2 class="main-heading">Customer Detail</h2>
                <div class="barber-widget">
                    <?php if (Session::has('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('success') ?>
                        </div><?php } ?>
                    <?php if (Session::has('error')) { ?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                            <?php echo Session::get('error') ?>
                        </div><?php } ?>
                    <?php
                    if (Session::has('message')) {
                        $data = json_decode(Session::get('message'));
                        if ($data) {
                            foreach ($data as $errors) {
                                ?>
                                <h6 class="alert alert-danger"> <?php echo $errors[0]; ?></h6>
                                <?php
                            }
                        }
                    }
                    ?>
                    <a href="#delete_model" data-toggle="modal" data-target="#confirm_model" class="h-btn-popup h-btn h-right">Change Password</a>

                    <form action="<?php echo asset('changeuserdetails'); ?>"method="post" class="h-barbar-form h-form">
                        <fieldset>
                            <div class="h-row">
                                <label for="b-name">First Name</label>
                                <input name="first_name" type="text" id="b-name" value="<?php echo $user->first_name ?>">
                            </div>
                            <div class="h-row">
                                <label for="b-name">Last Name</label>
                                <input name="last_name" type="text" id="b-name" value="<?php echo $user->last_name ?>">
                            </div>
                            <input type="hidden" name="id" value="<?php echo $user->id; ?>">

                            <div class="h-row">
                                <label for="h-country">Email</label>
                                <input name="email" type="text" id="h-country" placeholder="Email" value="<?php echo $user->email ?>">
                            </div>

                            <div class="h-row">
                                <label for="h-city">Phone</label>
                                <input name="mobile" type="text" id="h-city" placeholder="Phone" value="<?php echo $user->mobile ?>">
                            </div>
                            <div class="h-row">
                                <input type="submit" value="Save">
                            </div>
                        </fieldset>
                    </form>
                </div>
            </section>
        </main>
        <div class="modal fade" id="confirm_model" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo asset('/changeuserpassword'); ?>" class="h-barbar-form h-form">
                            <fieldset>
                                <div class="h-row">
                                    <label for="h-password">Password</label>

                                    <input required="" name="password" type="password" id="h-password" placeholder="Password">
                                </div>
                                <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                                <div class="h-row">
                                    <label for="h-q-password">Confirm Password</label>
                                    <input required="" name="cpassword" type="password" id="h-q-password" placeholder="Confirm Password">
                                </div>
                                <div class="h-row">
                                    <input type="submit" value="Save">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
<?php include 'includes/js.php'; ?> 
        <script>
            $(document).ready(function () {
                $('#tableStyle').DataTable({
                    columnDefs: [{
                            targets: [0],
                            orderData: [0, 1]
                        }, {
                            targets: [1],
                            orderData: [1, 0]
                        }, {
                            targets: [4],
                            orderData: [4, 0]
                        }]
                });
                $('header button').click(function () {
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
