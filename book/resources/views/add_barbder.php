<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
            <?php include 'includes/header.php'; ?> 
            <section class="add-vehicle">
                <h2 class="main-heading"><?php echo $title?></h2>
                <?php if (Session::has('success')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
            <?php echo Session::get('success') ?>
        </div><?php } ?>
                <?php
                    if (Session::has('message')) {
                        $data = json_decode(Session::get('message'));
                        if ($data) {
                            foreach ($data as $errors) {
                                ?>
                                <h6 class="alert alert-danger"> <?php echo $errors[0]; ?></h6>
                                <?php }
                            }
                        } ?>
                <div class="add-vehicle-widget">
                    <form enctype="multipart/form-data" class="add-vehicle-form" method="post" action="<?php echo asset('addbarber') ?>">

                        <label class="full-field">
                            <span>First Name</span>
                            <input value="<?php echo Illuminate\Support\Facades\Input::old('first_name');?>" required="" type="text" name="first_name" placeholder="First Name">
                        </label>
                        <label class="full-field">
                            <span>Last Name</span>
                            <input value="<?php echo Illuminate\Support\Facades\Input::old('last_name');?>" required="" type="text" name="last_name" placeholder="Last Name">
                        </label>
                        <label class="full-field">
                            <span>User Name</span>
                            <input value="<?php echo Illuminate\Support\Facades\Input::old('username');?>" required="" type="text" name="username" placeholder="User Name">
                        </label>
                        <label class="full-field">
                            <span>Email</span>
                            <input value="<?php echo Illuminate\Support\Facades\Input::old('email');?>" required="" type="email" name="email" placeholder="Email">
                        </label>
                        <label class="full-field">
                            <span>Professional Type</span>
                            <select class="multi_category form-control input-sm" name="user_type[]" required multiple="multiple">
                                <?php foreach ($professional_types as $professional_type) { ?>
                                    <option value="<?php echo $professional_type->name; ?>" <?php if(Illuminate\Support\Facades\Input::old('user_type') == $professional_type->name){ echo 'selected'; } ?>><?php echo $professional_type->name; ?></option>
                                <?php } ?>


                            </select>
                        </label>
                        <label class="full-field">
                            <span>Phone</span>
                            <input value="<?php echo Illuminate\Support\Facades\Input::old('phone');?>" required="" type="text" name="phone" placeholder="Phone">
                        </label>
                        <label class="full-field">
                            <span>Password</span>
                            <input required="" type="password" name="password" placeholder="Password">
                        </label>
                        <label class="submit">
                            <input  type="submit" class="btn btn btn-primary" name="submit" value="Submit">
                        </label>
                    </form>
                </div>
            </section>
        </main>
        <?php include 'includes/js.php'; ?> 
        <script type="text/javascript" src="<?php echo asset('adminassets/js/jquery.datetimepicker.full.js') ?>"></script>
        <script>
            $(document).ready(function () {
                $('#tableStyle').DataTable({
                    columnDefs: [{
                            targets: [0],
                            orderData: [0, 1]
                        }, {
                            targets: [1],
                            orderData: [1, 0]
                        }, {
                            targets: [4],
                            orderData: [4, 0]
                        }]
                });
                $('header button').click(function () {
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
                $('.datetimepicker').datetimepicker({
                    onChangeDateTime: logic,
                    onShow: logic
                });

                var logic = function (currentDateTime) {
                    if (currentDateTime && currentDateTime.getDay() == 6) {
                        this.setOptions({
                            minTime: '11:00'
                        });
                    } else
                        this.setOptions({
                            minTime: '8:00'
                        });
                };
                $('.multi_category').select2();
            });
        </script>
    </body>
</html>
