<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'; ?> 
    <body>
        <?php include 'includes/sidebar.php'; ?> 
        <main class="main-content">
          <?php include 'includes/header.php'; ?> 
            <section class="vehicles-list">
                <div class="vehicles-head">
                    <h3>All Customers</h3>
                    <a href="<?php echo asset('export/users')?>" class="btn btn-primary pull-right">Export Customers</a>
				</div>
                <div class="vehicles-list-content">
                    <div class="vehicles-table">
                        <?php if (Session::has('success')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                        <?php echo Session::get('success') ?>
                    </div><?php } ?>
                        <table id="tableStyle" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user) {  ?>
                                <tr>
                                     <td><?php echo $user->first_name;?></td>
                                    <td><?php echo $user->last_name;?></td>
                                    <td><?php echo $user->username;?></td>
                                    <td><?php echo $user->email;?></td>
                                    <td><?php echo $user->mobile;?></td>
                                    <td><a href="<?php echo asset('customerdetail/'.$user->id)?>">View Detail</a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
        <?php include 'includes/js.php'; ?> 
        <script>
            $(document).ready(function(){
                $('#tableStyle').DataTable({
                    columnDefs: [{
                        targets: [0],
                        orderData: [0, 1]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }, {
                        targets: [1],
                        orderData: [1, 0]
                    }],
                    order: [[ 0, false ]],
                    bSort: false
                });
                $('header button').click(function(){
                    $('aside').toggleClass('custom-menu');
                    $('main').toggleClass('main-margin');
                });
            });
        </script>
    </body>
</html>
