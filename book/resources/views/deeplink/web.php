<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/', function () {
    if (Auth::check()) {
        return Redirect::to('/dashboard');
    } else {
        $data['title'] = 'Taper';
        return view('login', $data);
    }
});
Route::get('/deeplink/barber/{id}', function () {
        return view('deeplink.example');
    
});
Route::get('/deeplink/other', function () {
        return view('deeplink.index');
    
});
Route::post('/', 'AuthController@adminLogin');
Route::group(['middleware' => 'nocache'], function () {
    Auth::check();
    Route::get('dashboard', 'AdminController@dashboard');

    Route::get('faqs', 'AdminController@faqs');
    Route::get('addfaq', 'AdminController@addFaq');
    Route::post('addfaq', 'AdminController@storeFaq');
    Route::get('editfaq/{id}', 'AdminController@editFaq');
    Route::get('deletefaq/{id}', 'AdminController@deleteFaq');

    Route::get('promations', 'AdminController@promations');
    Route::get('addpromations', 'AdminController@addPromations');
    Route::post('addpromations', 'AdminController@storePromations');
    Route::get('editpromations/{id}', 'AdminController@editPromations');
    Route::get('deletepromo/{id}', 'AdminController@deletePromo');

    Route::get('users', 'AdminController@users');
    Route::get('professionals', 'AdminController@barbaers');
    Route::get('professionalsdetails/{id}', 'AdminController@barbaersDetails');

    Route::get('adminlogout', function() {
        Auth::logout();
        return Redirect::to(URL::previous());
    });
});
