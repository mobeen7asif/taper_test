<!DOCTYPE html>
<html>
<head>
	<title>Taper</title>
</head>
<body>
	<h2>Hi {!! $name !!},</h2>
    <h2 style="text-align: center;"><span>Get the Taper App</span></h2>
    <p style="text-align: center;"><span>Taper&nbsp;is available for iPhone and Android on the Apple App Store and Google Play Store for free.</span></p>
    <p>
        <span style="display: inline;margin-right: 20px;"><a href="https://itunes.apple.com/us/app/taper/id1228369422?mt=8" target="_self"><img width="182" height="54" src="https://taperup.com/wp-content/uploads/2017/04/taper-app.png" alt=""></a></span>
        <span style="display: inline;margin-right: 20px;"><a href="https://play.google.com/store/apps/details?id=com.taper.cp" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey"><img width="182" height="54" src="https://taperup.com/wp-content/uploads/revslider/home-24/button-1.png" class="vc_single_image-img attachment-full" alt=""></a></span>
    </p> 
</body>
</html>