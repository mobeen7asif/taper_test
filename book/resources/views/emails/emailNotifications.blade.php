<!DOCTYPE html>
<html>
<head>
	<title>Taper</title>
</head>
<body>
	<h2>Hi {!! $notificationData->mailOtherData->full_name !!},</h2>
        <p>{!! $notificationData->mailData->message !!}</p>
        @if($notificationData->mailOtherData->location)
            <p><label>Location: </label>{!! $notificationData->mailOtherData->location !!}</p>
        @endif
        @if($notificationData->mailOtherData->service_at)
            <p><label>Service At: </label>{!! $notificationData->mailOtherData->service_at !!}</p>
        @endif
        @if($notificationData->mailOtherData->start_time)
            <p><label>Start Time: </label>{!! $notificationData->mailOtherData->start_time !!}</p>
        @endif
        @if($notificationData->mailOtherData->service_time)
            <p><label>Service Time: </label>{!! $notificationData->mailOtherData->service_time !!}</p>
        @endif
        @if($notificationData->mailOtherData->price)
            <p><label>Price: </label>{!! $notificationData->mailOtherData->price !!}</p>
        @endif
        @if($notificationData->mailOtherData->services)
            <p><label>Services: </label>{!! $notificationData->mailOtherData->services !!}</p>
        @endif
        @if($notificationData->mailOtherData->date)
            <p><label>Date: </label>{!! $notificationData->mailOtherData->date !!}</p>
        @endif
        @if($notificationData->mailOtherData->payment)
            <p><label>Payment: </label>{!! $notificationData->mailOtherData->payment !!}</p>
        @endif  
</body>
</html>