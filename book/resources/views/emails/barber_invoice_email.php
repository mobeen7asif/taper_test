<!DOCTYPE html>
<html lang="en">
    <head>
        <title>taper</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table style="margin: auto; padding: 15px;">
            <tr>
                <td>
                    <?php if($user->profile_pic == '' || $user->profile_pic == NULL){ ?>
                    <img src="<?php echo asset('public/images/default-user.png')?>" alt="img" style="border-radius: 50%; border: 1px solid #76020a;" width="100" height="100" />
                    <?php } else{ ?>
                    <img src="<?php echo asset('public/images/'.$user->profile_pic)?>" alt="img" style="border-radius: 50%; border: 1px solid #76020a;" width="100" height="100" />
                    <?php }  ?>
                </td>
                <td>
                    <h3 style="margin: 0; color: #9eafbb; font-weight: 300;"><?php echo $user->first_name.' '.$user->last_name; ?></h3>
<!--                    <h2 style="margin: 0; color: #000000;"><?php //echo $user->username; ?></h2>
                    <h3 style="margin: 0; color: #76020a; font-weight: 300;"><?php //echo $user->shop_name; ?></h3>-->
                    <h1 style="margin: 0; color: #000000; font-weight: 300; font-size: 26px;">
                        <img src="<?php echo asset('adminassets/images/location.png')?>" alt="img" width="15" height="15"/>
                        <?php echo $user->location; ?>
                    </h1>
                </td>
            </tr>
            <?php foreach ($services as $service){ ?>
            <tr>
                <td colspan="2">
                    <div style="border-bottom: 1px solid #a5a5a8; padding-bottom: 5px; font-size: 20px;">
                        <span style="color: #a5a5a8; font-weight: 300;"> <?= $service ?></span>
                        <!--<span style="color: #76020a; font-weight: bolder; float: right;">$40</span>-->
                    </div>
                </td>
            </tr>
            <?php  } ?>
<!--            <tr>
                <td colspan="2">
                    <div style="border-bottom: 1px solid #a5a5a8; padding-bottom: 5px; font-size: 20px;">
                        <span style="color: #a5a5a8; font-weight: 300;">Service Fee</span>
                        <span style="color: #76020a; font-weight: bolder; float: right;">$3.10</span>
                    </div>
                </td>
            </tr>-->
<!--            <tr>
                <td colspan="2"><span style="color: #a5a5a8; font-weight: 300; text-align: center; display: block;">No card attached. Please attach card to proceed with booking.</span></td>
            </tr>
-->            <tr>
                <td colspan="2"><span style="color: #76020a; font-weight: bolder; text-align: center; display: block;">$<?= $price ?></span></td>
            </tr><!--
            <tr>
                <td colspan="2"><span style="color: #a5a5a8; font-weight: 300; text-align: center; display: block;">*General time for the service added in 30 min approx</span></td>
            </tr>-->
        </table>
    </body>
</html>