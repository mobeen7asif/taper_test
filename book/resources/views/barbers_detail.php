<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<body>
<?php include 'includes/sidebar.php'; ?>
<main class="main-content">
    <?php include 'includes/header.php'; ?>
    <section class="vehicle-detail">
        <h2 class="main-heading">Professional Detail</h2>
        <div class="barber-widget">
            <?php if (Session::has('success')) { ?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                <?php echo Session::get('success') ?>
                </div><?php } ?>
            <?php if (Session::has('error')) { ?>
                <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times</a>
                <?php echo Session::get('error') ?>
                </div><?php } ?>
            <?php
            if (Session::has('message')) {
                $data = json_decode(Session::get('message'));
                if ($data) {
                    foreach ($data as $errors) {
                        ?>
                        <h6 class="alert alert-danger"> <?php echo $errors[0]; ?></h6>
                        <?php
                    }
                }
            }
            ?>
            <a href="#delete_model" data-toggle="modal" data-target="#confirm_model" class="h-btn-popup h-btn h-right">Change Password</a>
            <a href="<?= asset('barber_images/' . $barber->id) ?>"class="h-btn-popup h-btn h-right" style="margin-right: 15px;">Images</a>
            <form action="<?php echo asset('changebarbderdetails'); ?>"method="post" class="h-barbar-form h-form">
                <fieldset>
                    <div class="h-row">
                        <label for="b-name">First Name</label>
                        <input name="first_name" type="text" id="b-name" value="<?php echo $barber->first_name ?>">
                    </div>
                    <div class="h-row">
                        <label for="l-name">Last Name</label>
                        <input name="last_name" type="text" id="l-name" value="<?php echo $barber->last_name ?>">
                    </div>
                    <input type="hidden" name="id" value="<?php echo $barber->id; ?>">
                    <div class="h-row">
                        <label for="s-username">User Name</label>
                        <input name="username" type="text" id="s-username" value="<?php echo $barber->username ?>">
                    </div>
                    <div class="h-row">
                        <label for="s-name">Shop Name</label>
                        <input name="shopname" type="text" id="s-name" value="<?php echo $barber->shop_name ?>">
                    </div>

                    <div class="h-row">
                        <label for="f-address">Shop Address</label>
                        <input name="shop_location" type="text" id="f-address" value="<?php echo $barber->shop_location ?>">
                    </div>
                    <div class="h-row">
                        <label for="announcment">Announcement</label>
                        <input name="announcment" type="text" id="announcment" value="<?php echo $barber->announcment ?>">
                    </div>
                    <div class="h-row">
                        <label for="cancel_fee">Change Cancellation Policy</label>
                        <select name="cancel_fee" id="cancel_fee">
                            <option value="0" <?php
                            if ($barber->cancel_fee == 0) {
                                echo 'selected';
                            }
                            ?>>0%</option>
                            <option value="10" <?php
                            if ($barber->cancel_fee == 10) {
                                echo 'selected';
                            }
                            ?>>10%</option>
                            <option value="20" <?php
                            if ($barber->cancel_fee == 20) {
                                echo 'selected';
                            }
                            ?>>20%</option>
                            <option value="50" <?php
                            if ($barber->cancel_fee == 50) {
                                echo 'selected';
                            }
                            ?>>50%</option>
                        </select>
                    </div>
                    <div class="h-row">
                        <label for="f-profession">Professional Type</label>
                        <!--                                <input readonly="" name="shop_location" type="text" id="f-profession" value="--><?php //echo $barber->user_type ?><!--">-->
                        <select class="multi_category form-control input-sm" name="user_type[]" required multiple="multiple">

                            <?php foreach ($professional_types as $professional_type) { ?>
                                <option value="<?php echo $professional_type->name; ?>" <?php if(in_array($professional_type->name,$types)){echo 'selected';} ?>><?php echo $professional_type->name; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="h-row">
                        <label for="is_cash">Account Type</label>
                        <select name="is_cash" id="is_cash" onchange="selectchange(this)">
                            <option value="0" <?php
                            if ($barber->is_subscribed == 0) {
                                echo 'selected';
                            }
                            ?>>Taper Zero</option>
                            <option value="1" <?php
                            if ($barber->is_subscribed == 1) {
                                echo 'selected';
                            }
                            ?>>Taper Plus</option>
                        </select>
                    </div>
                    <div class="h-row">
                        <label for="p_cash">Payment Type</label>
                        <select  name="payment_type" id="p_cash">
                            <option id="show0" class="show0" <?php if ($barber->is_cash != 0) { ?>style="display: none" <?php } ?> value="0" <?php
                            if ($barber->is_cash == 0) {
                                echo 'selected';
                            }
                            ?>>Credit Card Only</option>
                            <option class="show1" <?php if ($barber->is_cash == 0) { ?>style="display: none" <?php } ?> value="3" <?php
                            if ($barber->is_cash == 3) {
                                echo 'selected';
                            }
                            ?>>Cash Only</option>
                            <option id="cashcard"  class="show1 cashcard"<?php if ($barber->is_cash == 0) { ?>style="display: none" <?php } ?> value="1" <?php
                            if ($barber->is_cash == 1) {
                                echo 'selected';
                            }
                            ?>>Credit Card And Cash</option>

                        </select>
                    </div>
                    <input id="is_subscribed" type="hidden" value="" name="subscription_type">
                    <div class="h-row">
                        <label for="h-country">Email</label>
                        <input name="email" type="text" id="h-country" placeholder="Email" value="<?php echo $barber->email ?>">
                    </div>

                    <div class="h-row">
                        <label for="h-city">Phone</label>
                        <input name="mobile" type="text" id="h-city" placeholder="Phone" value="<?php echo $barber->mobile ?>">
                    </div>
                    <div class="h-row">
                        <input type="submit" value="Save">
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="barber-widget">
            <strong class="widget-title">Edit Business Hours</strong>
            <form method="post" action="<?php echo asset('updatehours'); ?>" class="h-barbar-form h-form off-day">
                <fieldset>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Monday</span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $barber->id; ?>">
                        <div class="h-col">
                            <select name="mon[]">

                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->mon, 'Off') === false) {
                                        $checkbox = FALSE;
                                        $twodats = explode('-', $bussiness->mon);
                                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                } else {
                                    $checkbox = FALSE;
                                    $startingarray = array('', '');
                                    $endingarray = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarray[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="mon[]">
                                <option <?php
                                if ($startingarray[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarray[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="mon[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarray[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="mon[]">
                                <option <?php
                                if ($endingarray[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarray[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday1">Off day</label>
                                <input type="checkbox" id="offday1" name="mon[]" <?php if ($checkbox) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Tuesday</span>
                        </div>
                        <div class="h-col">
                            <select name="tue[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->tue, 'Off') === false) {
                                        $checkboxtue = FALSE;
                                        $twodats = explode('-', $bussiness->tue);
                                        $startingarraytue = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarraytue = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxtue = true;
                                        $startingarraytue = array('', '');
                                        $endingarraytue = array('', '');
                                    }
                                } else {
                                    $checkboxtue = FALSE;
                                    $startingarraytue = array('', '');
                                    $endingarraytue = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarraytue[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?> value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="tue[]">
                                <option <?php
                                if ($startingarraytue[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarraytue[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="tue[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarraytue[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="tue[]">
                                <option <?php
                                if ($endingarraytue[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarraytue[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday2">Off day</label>
                                <input type="checkbox" id="offday2" name="tue[]" <?php if ($checkboxtue) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Wednesday</span>
                        </div>
                        <div class="h-col">
                            <select name="wed[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->wed, 'Off') === false) {
                                        $checkboxwed = FALSE;
                                        $twodats = explode('-', $bussiness->wed);
                                        $startingarraywed = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarraywed = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxwed = true;
                                        $startingarraywed = array('', '');
                                        $endingarraywed = array('', '');
                                    }
                                } else {
                                    $checkboxwed = FALSE;
                                    $startingarraywed = array('', '');
                                    $endingarraywed = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarraywed[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="wed[]">
                                <option <?php
                                if ($startingarraywed[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarraywed[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="wed[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarraywed[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="wed[]">
                                <option <?php
                                if ($endingarraywed[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarraywed[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday3">Off day</label>
                                <input type="checkbox" id="offday3" name="wed[]" <?php if ($checkboxwed) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Thursday</span>
                        </div>
                        <div class="h-col">
                            <select name="thus[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->thus, 'Off') === false) {
                                        $checkboxthus = FALSE;
                                        $twodats = explode('-', $bussiness->thus);
                                        $startingarraythus = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarraythus = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxthus = true;
                                        $startingarraythus = array('', '');
                                        $endingarraythus = array('', '');
                                    }
                                } else {
                                    $checkboxthus = FALSE;
                                    $startingarraythus = array('', '');
                                    $endingarraythus = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarraythus[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?> value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="thus[]">
                                <option <?php
                                if ($startingarraythus[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarraythus[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="thus[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarraythus[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="thus[]">
                                <option <?php
                                if ($endingarraythus[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarraythus[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday4">Off day</label>
                                <input type="checkbox" id="offday4" name="thus[]" <?php if ($checkboxthus) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Friday</span>
                        </div>
                        <div class="h-col">
                            <select name="fri[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->fri, 'Off') === false) {
                                        $checkboxfri = FALSE;
                                        $twodats = explode('-', $bussiness->fri);
                                        $startingarrayfri = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarrayfri = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxfri = true;
                                        $startingarrayfri = array('', '');
                                        $endingarrayfri = array('', '');
                                    }
                                } else {
                                    $checkboxfri = false;
                                    $startingarrayfri = array('', '');
                                    $endingarrayfri = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarrayfri[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>   value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="fri[]">
                                <option <?php
                                if ($startingarrayfri[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarrayfri[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="fri[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarrayfri[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="fri[]">
                                <option <?php
                                if ($endingarrayfri[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarrayfri[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday5">Off day</label>
                                <input type="checkbox" id="offday5" name="fri[]" <?php if ($checkboxfri) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Saturday</span>
                        </div>
                        <div class="h-col">
                            <select name="sat[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->sat, 'Off') === false) {
                                        $checkboxsat = FALSE;
                                        $twodats = explode('-', $bussiness->sat);
                                        $startingarraysat = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarraysat = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxsat = true;
                                        $startingarraysat = array('', '');
                                        $endingarraysat = array('', '');
                                    }
                                } else {
                                    $checkboxsat = false;
                                    $startingarraysat = array('', '');
                                    $endingarraysat = array('', '');
                                }
                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarraysat[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>   value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="sat[]">
                                <option <?php
                                if ($startingarraysat[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarraysat[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="sat[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option  <?php
                                    if ($endingarraysat[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="sat[]">
                                <option <?php
                                if ($endingarraysat[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarraysat[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday6">Off day</label>
                                <input type="checkbox" id="offday6" name="sat[]" <?php if ($checkboxsat) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <div class="h-col">
                            <span>Sunday</span>
                        </div>
                        <div class="h-col">
                            <select name="sun[]">
                                <?php
                                if ($bussiness) {
                                    if (strpos($bussiness->sun, 'Off') === false) {
                                        $checkboxsun = FALSE;
                                        $twodats = explode('-', $bussiness->sun);
                                        $startingarraysun = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                        $endingarraysun = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                    } else {
                                        $checkboxsun = true;
                                        $startingarraysun = array('', '');
                                        $endingarraysun = array('', '');
                                    }
                                } else {
                                    $checkboxsun = false;
                                    $startingarraysun = array('', '');
                                    $endingarraysun = array('', '');
                                }

                                for ($hours = 1; $hours <= 12; $hours++) {
                                    ?>
                                    <option <?php
                                    if ($startingarraysun[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>  value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="sun[]">
                                <option <?php
                                if ($startingarraysun[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($startingarraysun[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <em>-</em>
                            <select name="sun[]">
                                <?php for ($hours = 1; $hours <= 12; $hours++) { ?>
                                    <option <?php
                                    if ($endingarraysun[0] == $hours) {
                                        echo "selected";
                                    }
                                    ?>   value="<?php echo $hours; ?>"><?php echo $hours; ?></option>
                                <?php } ?>
                            </select>
                            <select name="sun[]">
                                <option <?php
                                if ($endingarraysun[1] == 'AM') {
                                    echo "selected";
                                }
                                ?>>AM</option>
                                <option <?php
                                if ($endingarraysun[1] == 'PM') {
                                    echo "selected";
                                }
                                ?>>PM</option>
                            </select>
                            <div class="h-off-day">
                                <label for="offday7">Off day</label>
                                <input type="checkbox" id="offday7" name="sun[]" <?php if ($checkboxsun) { ?> checked <?php } ?>>
                            </div>
                        </div>
                    </div>
                    <div class="h-row">
                        <input type="submit" value="Save">
                    </div>
                </fieldset>
            </form>
        </div>


        <div class="barber-widget">
            <header class="h-header">
                <a href="#" data-toggle="modal" data-target="#add_category_model" class="h-btn-popup h-btn h-right text-center">Add Category</a>
                <h2 class="main-heading">Category</h2>
            </header>
            <?php foreach ($categories as $category) { ?>
                <form action="#" class="h-barbar-form cat-form-repeater">
                    <fieldset>
                        <div class="h-row">
                            <div class="col">
                                <label for ="cut1">Category</label>
                                <input value=" <?php echo str_replace('_', ' ', $category->category); ?>" type="text" placeholder="Cutting" readonly="">
                            </div>
                            <div class="col">
                                <label for ="time1">Description</label>
                                <input value="<?php echo $category->description; ?>" type="text" id="time1" placeholder="Description" readonly>
                            </div>
                            <div class="col last">
                                <a href="#" data-toggle="modal" data-target="#edit_model1<?php echo $category->id ?>"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="#" data-toggle="modal" data-target="#delete_model_service<?php echo $category->id ?>"><i class="fa fa-trash fa-fw"></i></a>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div class="modal fade" id="edit_model1<?php echo $category->id ?>" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo asset('updatecategory'); ?>" class="h-barbar-form h-form" method="post">
                                    <fieldset>

                                        <div class="h-row">
                                            <label for="service-type">Category</label>
                                            <input type="hidden" name="id" value="<?php echo $category->id; ?>">
                                            <input name="category" value="<?php echo $category->category; ?>" type="text" id="service-type" placeholder="Category Title">
                                        </div>

                                        <div class="h-row">
                                            <label for="p-time1">Description</label>
                                            <input name="description" value="<?php echo $category->description; ?>"type="text" id="p-time1" placeholder="Description">
                                        </div>
                                        <div class="h-row">
                                            <input type="submit" value="Save">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="delete_model_service<?php echo $category->id ?>" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body text-center">
                                <a class="btn btn-primary" href="<?php echo asset('deletecategory/' . $category->id); ?>">Yes</a>
                                <a class="btn btn-primary" href="#" data-dismiss="modal">No</a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <header>
                <a href="#" data-toggle="modal" data-target="#add_service_model" class="h-btn-popup h-btn h-right text-center">Add Service</a>
                <h2 class="main-heading">Services</h2>
            </header>
            <?php foreach ($services as $service) { ?>
                <form action="#" class="h-barbar-form form-repeater">
                    <fieldset>
                        <div class="h-row">
                            <div class="col">
                                <label for ="cut1">Category</label>
                                <input value=" <?php echo isset($service->getCategory->category) ?$service->getCategory->category : ''; ?>" type="text" placeholder="Cutting" readonly="">
                            </div>
                            <div class="col">
                                <label for ="cut1">Service Type</label>
                                <input value=" <?php echo $service->service_type; ?>" type="text" placeholder="Cutting" readonly="">
                            </div>
                            <div class="col">
                                <label for ="price1">Price</label>
                                <input value=" <?php echo $service->price; ?>" type="text" id="price1" placeholder="25" readonly>
                            </div>
                            <div class="col">
                                <label for ="time1">Time</label>
                                <input value="<?php echo $service->time; ?>" type="text" id="time1" placeholder="45" readonly>
                            </div>
                            <!--                                    <div class="col">
                                                                            <label for ="time1">Description</label>
                                                                            <input value="<?php // echo $service->description;    ?>" type="text" id="time1" placeholder="Description" readonly>
                                                                        </div>-->
                            <div class="col last">
                                <a href="#" data-toggle="modal" data-target="#edit_model1<?php echo $service->id ?>"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="#" data-toggle="modal" data-target="#delete_model_service<?php echo $service->id ?>"><i class="fa fa-trash fa-fw"></i></a>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div class="modal fade" id="edit_model1<?php echo $service->id ?>" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo asset('updateservice'); ?>" class="h-barbar-form h-form" method="post">
                                    <fieldset>
                                        <div class="h-row">
                                            <label for="service-type">Category</label>
                                            <select name="category_id" class="form-control" style="color: #093273;background: #E5E9EC;">

                                                <?php if (isset($categories)) {
                                                    foreach ($categories as $key => $value) {
                                                        ?>
                                                        <option value="<?= $value['id'] ?>" <?= ($service->category_id == $value['id']) ? 'selected' : ''; ?>><?= $value['category'] ?></option>
                                                    <?php }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="h-row">
                                            <label for="service-type">Service Type</label>
                                            <input type="hidden" name="id" value="<?php echo $service->id; ?>">
                                            <input name="service" value="<?php echo $service->service_type; ?>" type="text" id="service-type" placeholder="Cutting">
                                        </div>

                                        <div class="h-row">
                                            <label for="p-price">Price</label>
                                            <input name="price" value="<?php echo $service->price; ?>" type="number" id="p-price" placeholder="25">
                                        </div>
                                        <div class="h-row">
                                            <label for="p-time">Time</label>
                                            <input name="time" value="<?php echo $service->time; ?>"type="number" id="p-time" placeholder="45">
                                        </div>
                                        <div class="h-row">
                                            <label for="p-time1">Description</label>
                                            <input name="description" value="<?php echo $service->description; ?>"type="text" id="p-time1" placeholder="Description">
                                        </div>
                                        <div class="h-row">
                                            <input type="submit" value="Save">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="delete_model_service<?php echo $service->id ?>" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body text-center">
                                <a class="btn btn-primary" href="<?php echo asset('deleteservice/' . $service->id); ?>">Yes</a>
                                <a class="btn btn-primary" href="#" data-dismiss="modal">No</a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
</main>
<div class="modal fade" id="confirm_model" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo asset('/changebarberpassord'); ?>" class="h-barbar-form h-form">
                    <fieldset>
                        <div class="h-row">
                            <label for="h-password">Password</label>

                            <input required="" name="password" type="password" id="h-password" placeholder="Password">
                        </div>
                        <input type="hidden" name="id" value="<?php echo $barber->id; ?>">
                        <div class="h-row">
                            <label for="h-q-password">Confirm Password</label>
                            <input required="" name="cpassword" type="password" id="h-q-password" placeholder="Confirm Password">
                        </div>
                        <div class="h-row">
                            <input type="submit" value="Save">
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_service_model" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo asset('addservice'); ?>" class="h-barbar-form h-form">
                    <fieldset>
                        <input type="hidden" name="id" value="<?php echo $barber->id; ?>">
                        <div class="h-row">
                            <label for="service-type">Category</label>
                            <select name="category_id" class="form-control" style="color: #093273;background: #E5E9EC;">
                                <?php if (isset($categories)) {
                                    foreach ($categories as $key => $value) {
                                        ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['category'] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="h-row">
                            <label for="service-type">Service Type</label>
                            <input name="service" type="text" id="service-type" placeholder="Cutting">
                        </div>

                        <div class="h-row">
                            <label for="p-price">Price</label>
                            <input name="price" type="number" min="0" id="p-price" placeholder="25">
                        </div>
                        <div class="h-row">
                            <label for="p-time2">Time</label>
                            <input type="number" id="p-time2" placeholder="45" name="time" min="0">
                        </div>
                        <div class="h-row">
                            <label for="p-time3">Description</label>
                            <input type="text" id="p-time3" placeholder="description" name="description">
                        </div>
                        <div class="h-row">
                            <input type="submit" value="Save">
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_category_model" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo asset('addcategory'); ?>" class="h-barbar-form h-form">
                    <fieldset>
                        <input type="hidden" name="id" value="<?php echo $barber->id; ?>">

                        <div class="h-row">
                            <label for="service-type">Category</label>
                            <input name="category" type="text" id="category" placeholder="Category Title">
                        </div>
                        <div class="h-row">
                            <label for="p-time3">Description</label>
                            <input type="text" id="description" placeholder="Description" name="description">
                        </div>
                        <div class="h-row">
                            <input type="submit" value="Save">
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
<?php include 'includes/js.php'; ?>
<script>
    $(document).ready(function () {
        $('#tableStyle').DataTable({
            columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [4],
                orderData: [4, 0]
            }]
        });
        $('header button').click(function () {
            $('aside').toggleClass('custom-menu');
            $('main').toggleClass('main-margin');
        });
        $('.multi_category').select2();

        var is_subscribed = "<?php echo $barber->is_subscribed ?>";
        if(is_subscribed == 1){
            $('.show0').show();
            $('.show1').show();
            $("#is_subscribed").val("1");
        }else {
            $('.show0').show();
            $("#is_subscribed").val("0");
        }
    });
    function selectchange(ele) {
        if (ele.value == 0) {
            $('.show0').show();
            $('.show1').hide();
            $("#p_cash").val("0");
            $("#is_subscribed").val("0");

        } else {
            $('.show0').show();
            $('.show1').show();
            $("#p_cash").val("1");
            $("#is_subscribed").val("1");
        }
    }
</script>
</body>
</html>
