<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$barber->first_name.' '.$barber->last_name;?> - Taper</title>
	<link rel="stylesheet" href="<?php echo asset('frontendassets/css/lightbox.css')?>">
	<link rel="stylesheet" href="<?php echo asset('frontendassets/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo asset('frontendassets/css/theme.css')?>">
	<link rel="icon" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-180x180.png" />
	<meta name="msapplication-TileImage" content="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-270x270.png" />
</head>
<body>
	<header class="header">
		<div class="logo">
			<a href="<?=url('/');?>">
				<img src="<?php echo asset('frontendassets/images/taper-logo.png')?>" alt="">
			</a>
		</div>
		<a href="#" class="toggle"><i class="fa fa-navicon"></i></a>
		<a href="<?=url('/download');?>" class="download">Download</a>
		<nav class="mainNav">
			<ul>
				<li><a href="<?=url('/');?>">Home</a></li>
				<li><a href="https://taperhelp.zendesk.com">Help</a></li>
				<li><a href="http://blog.taperup.com">Blog</a></li>
			</ul>
		</nav>
	</header>
	<?php $coverPic = ($barber->cover) ? asset('public/images/'.$barber->cover) : asset('frontendassets/images/defaultCover.png');?>
	<section class="topBanner detailBanner" style="<?php if($coverPic!=''){?>background: url(<?=$coverPic;?>);<?php } ?>">
		<div class="container">
			<figure>
				<img src="<?php echo ($barber->login_type == 'fb') ? $barber->social_pic : (($barber->profile_pic) ? asset('public/images/'.$barber->profile_pic) : asset('frontendassets/images/userDefault.png')); ?>" alt="">
			</figure>
			<div class="barberDetail">
				<h4><?=$barber->first_name.' '.$barber->last_name;?></h4>
				<?php $rating = ($barber->getRating)?$barber->getRating->rating : 0;?>
				<span class="rating">
				<?php for($rate=1; $rate<=5;$rate++){
					if($rate <= $rating){?>
						<i class="fa fa-star"></i>
					<?php }elseif(($rate > $rating) && ($rating > ($rate - 1))){?>
						<i class="fa fa-star-half-o"></i>
					<?php }else{ ?>
						<i class="fa fa-star-o"></i>
					<?php }
				}?>							
				</span>
				<p><?=$barber->shop_location;?></p>
			</div>
			<div class="time">
				<i class="fa fa-map-marker"></i>
				<span><?php 
                                        $distance = round($barber->distance, 1);
                                        if(!$barber->distance){
                                            $distance = thousandsCurrencyFormat(round(distance($lat, $lon, $barber->lat, $barber->lng, "M"), 1));
                                        }?>
                                        <?=$distance;?> mi
                                </span>
			</div>
		</div>
	</section>
	<section class="listDetail">
		<div class="container">
			<ul id="tabs">
				<li><a href="#tab1">Book</a></li>
				<li><a href="#tab2">Gallery</a></li>
				<li><a href="#tab3">Reviews</a></li>
				<li><a href="#tab4">Information</a></li>
			</ul>
			<div class="tabItem bookWidget" id="tab1">
				<h2>Book Services</h2>
				<ul>
					<?php 
					if(!$services->isEmpty()){
						foreach ($services as $service) { ?>
						 <li>
							<h4><?php echo $service->service_type; ?> <?php echo $service->time; ?> <span>$<?php echo $service->price; ?></span></h4>
							<p><?php echo $service->description; ?></p>
						</li>
	                    <?php }
					}else{?>
						<li>No Service added Yet by this Professional.</li>
					<?php }  ?>
				</ul>
				<a href="<?=url('/download');?>" class="bookNow">Book Now</a>
			</div>
			<div class="tabItem gallery" id="tab2">
				<h2>Gallery</h2>
				<?php 
					if(!$images->isEmpty()){
						foreach ($images as $imageKey => $image) { ?>
						<div class="itemImg">
							<a class="example-image-link" href="<?php echo asset('/').'public/images/'.$image->image; ?>" data-lightbox="example-1">
								<img class="example-image" src="<?php echo asset('/').'public/images/'.$image->image; ?>" alt="image-1" />
							</a>
						</div>
	                <?php }
					}else{?>
						<p>No Images added Yet by this Professional.</p>
					<?php }  ?>
			</div>
			<div class="tabItem reviews" id="tab3">
				<h2>Reviews</h2>
				<ul>
					<?php 
					if(!$reviews->isEmpty()){
						foreach ($reviews as $review) { ?>
						<li>
							<figure>
								<img src="<?php echo asset('/').'public/images/'.$review->getUser->profile_pic; ?>" alt="">
							</figure>
							<figcaption>
								<span><?php echo $review->getUser->first_name.' '.$review->getUser->last_name; ?> <?php echo $review->getUser->created_at->diffForHumans(); ?></span>
								<p><?php echo $review->text; ?></p>
							</figcaption>
						</li>  
	                    <?php }
					}else{ ?>
						<li>No Reviews Yet for this Professional.</li>
					<?php }  ?>
				</ul>
			</div>
			<div class="tabItem info" id="tab4">
				<div class="infoLeft">
					<?php if($barber->mobile){ ?>
						<span class="number">
							<i class="fa fa-phone"></i>
							<?=$barber->mobile;?>
						</span>
					<?php } ?>
					<b>Business Hours</b>
					<ul>
						<li>Monday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->mon, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->mon);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';
                                    ?></li>
						<li>Tuesday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->tue, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->tue);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
						<li>Wednesday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->wed, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->wed);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
						<li>Monday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->thus, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->thus);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
						<li>Friday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->fri, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->fri);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
						<li>Saturday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->sat, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->sat);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
						<li>Sunday: <?php
                                    if ($bussiness) {
                                        if (strpos($bussiness->sun, 'Off') === false) {
                                            $checkbox = FALSE;
                                            $twodats = explode('-', $bussiness->sun);
                                            $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                                            $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                                        } else {
                                            $checkbox = true;
                                            $startingarray = array('', '');
                                            $endingarray = array('', '');
                                        }
                                    } else {
                                        $checkbox = true;
                                        $startingarray = array('', '');
                                        $endingarray = array('', '');
                                    }
                                    if(!$checkbox)
                                        echo $startingarray[0].' '.$startingarray[1].' <em>-</em> '.$endingarray[0].' '.$endingarray[1];
                                    else
                                        echo 'Closed';?></li>
					</ul>
				</div>
				<div class="rightMap">
					
					<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3399.3885377471347!2d74.30590821538496!3d31.568391551927302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39191ca8f5a906f1%3A0xc5dad8adc056fa92!2sLahore+Museum%2C+Lahore!5e0!3m2!1sen!2s!4v1505398320960" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
				</div>
			</div>
		</div>
	</section><div id="professionalMap"></div>
</body>
</html>

<script type='text/javascript' src='<?php echo asset('frontendassets/js/jquery.min.js')?>'></script>
<script type='text/javascript' src='<?php echo asset('frontendassets/js/lightbox.js')?>'></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCqc-BLADMwDPJa5RX03ZDGl4XYEB57ATs"></script>
<script src="<?php echo asset('frontendassets/js/gmaps/gmaps.min.js')?>"></script>

<script>
	
	$(document).ready(function() {    

	$('#tabs li a:not(:first)').addClass('inactive');
	$('.tabItem').hide();
	$('.tabItem:first').show();
	$('#tabs li a').click(function(){
		var t = $(this).attr('href');
		$('#tabs li a').addClass('inactive');        
		$(this).removeClass('inactive');
		$('.tabItem').hide();
		$(t).fadeIn('slow');
		return false;
	})

	if($(this).hasClass('inactive')){ //this is the start of our condition 
		$('#tabs li a').addClass('inactive');         
		$(this).removeClass('inactive');
		$('.tabItem').hide();
		$(t).fadeIn('slow');    
	}

	var map = new GMaps({
                    el: '#professionalMap',
                    lat: "<?=$barber->lat;?>",
                    lng: "<?=$barber->lng;?>"
                  });
                  
	map.addMarker({
	    lat: "<?=$barber->lat;?>",
	    lng: "<?=$barber->lng;?>",
	    title: "<?=$barber->first_name.' '.$barber->last_name;?>",
	//                    click: function(e) {
	//                      alert('You clicked in this marker');
	//                    },
	    infoWindow: {
	        content: '<p>'+"<?=$barber->shop_location;?>"+'</p>'
	      }
	  });

	});

</script>