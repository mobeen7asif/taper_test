<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Professionals - Taper</title>
	<link rel="stylesheet" href="<?php echo asset('frontendassets/css/theme.css')?>">
	<link rel="stylesheet" href="<?php echo asset('frontendassets/css/font-awesome.min.css')?>">
	<link rel="icon" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-32x32.png" sizes="32x32" />
	<link rel="icon" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-180x180.png" />
	<meta name="msapplication-TileImage" content="https://taperup.com/wp-content/uploads/2017/02/cropped-TAPER-ICON8-270x270.png" />
</head>
<body>
	<header class="header">
		<div class="logo">
			<a href="<?=url('/');?>">
				<img src="<?php echo asset('frontendassets/images/taper-logo.png')?>" alt="">
			</a>
		</div>
		<a href="#" class="toggle"><i class="fa fa-navicon"></i></a>
		<a href="<?=url('/download');?>" class="download">Download</a>
		<nav class="mainNav">
			<ul>
				<li><a href="<?=url('/');?>">Home</a></li>
				<li><a href="https://taperhelp.zendesk.com">Help</a></li>
				<li><a href="http://blog.taperup.com">Blog</a></li>
			</ul>
		</nav>
	</header>
	<section class="topBanner">
		<div class="container">
			<h1>Culture, Forward</h1>
			<p>Taper Prioritizes the importance of the user experience.</p>
		</div>
	</section>
	<section class="barberListing">
		<div class="container">
			<div class="barberSearch">
                            <form action="">
                                <input type="search" name="term" value="<?=$key;?>" placeholder="Search Professional">
                                <input type="hidden" name="lat" value="<?=$lat;?>"/>
                                <input type="hidden" name="lon" value="<?=$lon;?>"/>
				<input type="submit" value="Search">
                            </form>
			</div>
			<ul>
			<?php foreach($barbers as $barber){ ?>
                            <li>
                                <div class="listItem" style="background: url(<?php echo ($barber->cover) ? asset('public/images/'.$barber->cover) : asset('frontendassets/images/defaultCover.png');?>)">
                                    <a href="<?php echo url('/professional/'.$barber->id);?>">
                                        <figure>
                                                <img src="<?php echo ($barber->login_type == 'fb') ? $barber->social_pic : (($barber->profile_pic) ? asset('public/images/'.$barber->profile_pic) : asset('frontendassets/images/userDefault.png'));?>" alt="">
                                        </figure>
                                        <div class="barberDetail">
                                            <h4><?php echo $barber->first_name.' '.$barber->last_name;?></h4>
                                            <?php $rating = ($barber->getRating) ? $barber->getRating->rating : 0;?>
                                            <span class="rating">
                                            <?php for($rate=1; $rate<=5;$rate++){
                                                    if($rate <= $rating){?>
                                                            <i class="fa fa-star"></i>
                                                    <?php }elseif(($rate > $rating) && ($rating > ($rate - 1))){?>
                                                            <i class="fa fa-star-half-o"></i>
                                                    <?php }else{ ?>
                                                            <i class="fa fa-star-o"></i>
                                                    <?php }
                                            }?>							
                                            </span>
                                            <p><?=$barber->shop_location;?></p>
                                        </div>
                                        <div class="time">
                                                <i class="fa fa-map-marker"></i>
                                                <span><?php 
                                                $distance = round($barber->distance, 1);
                                                if(!$barber->distance){
                                                    $distance = thousandsCurrencyFormat(round(distance($lat, $lon, $barber->lat, $barber->lng, "M"), 1));
                                                }?><?=$distance;?> mi</span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
			</ul>
		</div>
		<div class="pagination">
			<ul>
                            <?php if($offset!=0){?>
                                    <li>
                                        <a href="<?=url('/professionals?term='.$key.'&lat='.$lat.'&lon='.$lon.'&offset='.($offset-1));?>">
                                                Previous
                                        </a>
                                    </li>
                            <?php } ?>
                            <?php $nextCounter = 0;
                            foreach ($pagination as $index => $value) {
                                    $nextCounter++;
                                    if($value < ($limitPage - 1)){?>
                                            <li <?php if($index == $offset) echo 'class="active"';?>>
                                            <a href="<?=url('/professionals?term='.$key.'&lat='.$lat.'&lon='.$lon.'&offset='.($value-1));?>">
                                                    <?=$value;?>
                                            </a>
                                    </li>
                                    <?php } ?>
                            <?php } ?>
                            <?php if($nextCounter < ($limitPage-1)){?>
                                    <li>
                                            <a href="<?=url('/professionals?term='.$key.'&lat='.$lat.'&lon='.$lon.'&offset='.($offset+1));?>">
                                                    Next
                                            </a>
                                    </li>
                            <?php } ?>
			</ul>
		</div>
	</section>
</body>
</html>