<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes group
  |--------------------------------------------------------------------------
  | Its to make cross check app key (checkAppKey)
  | And To solve crosss domain issue  (headersmid)
 */
 Route::get('test_notification/{device_id}', 'AuthController@notificationTest');
 Route::get('test_notifications/{device_id}', 'AuthController@notificationTests');
Route::group(['prefix' => 'v1', 'middleware' => ['headersmid', 'checkAppKey']], function () {
    Route::get('get_barber_link/{id}', 'UserController@getBarberDetailsUsername');
    Route::get('/andriod', 'UserController@android');
    Route::post('/testandriod', 'UserController@testandroid');
    Route::post('/testios', 'UserController@iostest');
//  ******************* Auth Section ************************* //
    /*
      |--------------------------------------------------------------------------
      | API Routes register
      |--------------------------------------------------------------------------
      |Post Register Route To Register User and barber also via fb and twitter
     */
    Route::post('/register', 'AuthController@register');
    /*
      |--------------------------------------------------------------------------
      | API Routes Forget Mail
      |--------------------------------------------------------------------------
      |Post Login  Route To Login in email users
     */
    Route::post('/forgetmail', 'AuthController@forgetPasswordMial');
    /*
      |--------------------------------------------------------------------------
      | API Routes Lgin
      |--------------------------------------------------------------------------
      |Post Login  Route To Login in email users
     */
    Route::post('/login', 'AuthController@login');
    /*
      |--------------------------------------------------------------------------
      | API Routes checksociallogin
      |--------------------------------------------------------------------------
      |Post checksociallogin  Route  to validate email for social login
     */
    Route::post('/checksociallogin', 'AuthController@CheckSocialLogin');

    /*
      |--------------------------------------------------------------------------
      | API Logout User
      |--------------------------------------------------------------------------
      |
     */
    Route::get('logoutuser/{id}', 'AuthController@userLogout');
    
            /*
          |--------------------------------------------------------------------------
          | API Routes Get All Barber Bookings
          |--------------------------------------------------------------------------
          |
         */
        Route::post('get_barber_bookings', 'UserController@getBarberBookings');
                /*
          |--------------------------------------------------------------------------
          | API Routes GEt Barbers
          |--------------------------------------------------------------------------
          |Get all barber with data
         */
        Route::get('barbers', 'UserController@barbers');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Get Barber Details
          |--------------------------------------------------------------------------
          |Is read of Notificaion
         */
        Route::get('get_barber_details/{id}', 'UserController@getBarberDetails');
                /*
          |--------------------------------------------------------------------------
          | API Routes search
          |--------------------------------------------------------------------------
          |Change search
         */
        Route::get('search', 'UserController@search');
    //  ******************* User Section ************************* //
    //  ******************* User Section ************************* //

    Route::group(['middleware' => ['checkSeesion']], function () {


        /*
          |--------------------------------------------------------------------------
          | API Routes Change password
          |--------------------------------------------------------------------------
          |Change password
         */
        Route::post('changepassword', 'UserController@changePassword');

        /*
          |--------------------------------------------------------------------------
          | API Routes GEt Notifications Limited
          |--------------------------------------------------------------------------
          |Get Notifications for dropdown with count
         */
        Route::get('notifications/{id}', 'UserController@getNotification');
        /*
          |--------------------------------------------------------------------------
          | API Routes Add card
          |--------------------------------------------------------------------------
          |
         */
        Route::post('addcard', 'UserController@addCard');
        /*
          |--------------------------------------------------------------------------
          | API Routes Delete card
          |--------------------------------------------------------------------------
          |
         */
        Route::get('deletecard/{id}', 'UserController@deleteCard');
        /*
          |--------------------------------------------------------------------------
          | API Routes GEt All Notifications
          |--------------------------------------------------------------------------
          |Get all barber with data
         */
        Route::get('all_notifications/{id}', 'UserController@getAllNotification');
        /*
          |--------------------------------------------------------------------------
          | API Routes Update  Notifcation
          |--------------------------------------------------------------------------
          |Is read of Notificaion
         */
        Route::get('readnoti/{id}', 'UserController@markNotificationRead');


        /*
          |--------------------------------------------------------------------------
          | API Routes Get All Faqs
          |--------------------------------------------------------------------------
          |
         */
        Route::get('faq', 'UserController@getFaqs');
        /*
          |--------------------------------------------------------------------------
          | API Routes Get All Promotions
          |--------------------------------------------------------------------------
          |Is read of Notificaion
         */
        Route::get('promo', 'UserController@getProm');
        /*
          |--------------------------------------------------------------------------
          | API Routes appiontments
          |--------------------------------------------------------------------------
          |
         */
        Route::get('appointments', 'UserController@appointments');
        /*
          |--------------------------------------------------------------------------
          | API Routes checkOverLap
          |--------------------------------------------------------------------------
          |To check the overlap betwwen secdule of a particaular barbaer
         */
        Route::post('checkoverlap', 'UserController@checkOverLap');

        /*
          |--------------------------------------------------------------------------
          | API Routes Add Booking From User
          |--------------------------------------------------------------------------
          |Add Booking Via Card of via stripe and cash
         */
        Route::post('addbooking', 'UserController@addBooking');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Add Custom Booking From User
          |--------------------------------------------------------------------------
          |Add Custom Booking Via Card of via stripe and cash
         */
        Route::post('addcustombooking', 'UserController@addCustomBooking');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Update Booking From User
          |--------------------------------------------------------------------------
          |Update Booking Via Card of via stripe and cash
         */
        Route::post('updatebooking', 'UserController@updateBooking');
        /*
          |--------------------------------------------------------------------------
          | API Routes To add rating and review
          |--------------------------------------------------------------------------
          |
         */
        Route::post('reviewbarber', 'UserController@addReviewRating');
        /*
          |--------------------------------------------------------------------------
          | API Routes to update profile of user and barbder
          |--------------------------------------------------------------------------
          |
         */
        Route::post('updateprofile', 'UserController@updateProfile');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Add Reporting by user
          |--------------------------------------------------------------------------
          |
         */
        Route::post('addreporting', 'UserController@addReporting');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Cancel Appointment by user
          |--------------------------------------------------------------------------
          |
         */
        Route::post('canceluserappointment', 'UserController@cancelAppointment');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Add Favorite by user
          |--------------------------------------------------------------------------
          |
         */
        Route::post('addfavorite', 'UserController@addFavorite');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Remove Favorite by user
          |--------------------------------------------------------------------------
          |
         */
        Route::post('removefavorite', 'UserController@removeFavorite');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Get Favorite by user
          |--------------------------------------------------------------------------
          |
         */
        Route::get('getfavorite', 'UserController@getFavorite');
        
        /*
          |--------------------------------------------------------------------------
          | API Routes Send Invite by user to Professional
          |--------------------------------------------------------------------------
          |
         */
        Route::post('sendInvite', 'UserController@sendInvite');
        
                /*
          |--------------------------------------------------------------------------
          | API Routes Delete Booking
          |--------------------------------------------------------------------------
          |
         */
Route::get('delete_booking/{booking_id}', 'UserController@deleteBooking');

  /*
          |--------------------------------------------------------------------------
          | API Routes Delete Booking
          |--------------------------------------------------------------------------
          |
         */
Route::get('get_user_details/{user_id}', 'UserController@getUserDetails');

//    ********************** Barber Section ********** //

        /*
          |--------------------------------------------------------------------------
          | API Routes Add  Pic
          |--------------------------------------------------------------------------
          |Add cover and profile pic
         */
        Route::post('addcover', 'BarberController@addCover');

        Route::post('addcategory', 'BarberController@addCategory');


        /*
          |--------------------------------------------------------------------------
          | API Routes Update addresss and shop name
          |--------------------------------------------------------------------------
          |
         */
        Route::post('updatedetails', 'BarberController@updateDetails');
        /*
          |--------------------------------------------------------------------------
          | API Routes add Gallery Image
          |--------------------------------------------------------------------------
          |
         */

        Route::post('addimage', 'BarberController@addGallery');
        /*
          |--------------------------------------------------------------------------
          | API Routes aTo Add Services
          |--------------------------------------------------------------------------
          |
         */
        Route::post('addservices', 'BarberController@addServices');

        /*
          |--------------------------------------------------------------------------
          | API Routes aTo Add Services
          |--------------------------------------------------------------------------
          |
         */
        Route::post('addservices', 'BarberController@addServices');
        /*
          |--------------------------------------------------------------------------
          | API Routes add Gallery Image
          |--------------------------------------------------------------------------
          |
         */
        Route::post('deleteimage', 'BarberController@deleteImage');
        /*
          |--------------------------------------------------------------------------
          | API Routes add Gallery Image
          |--------------------------------------------------------------------------
          |
         */
        Route::post('deleteservice', 'BarberController@deleteService');
        /*
          |--------------------------------------------------------------------------
          | API Routes Get Baraber Secdules
          |--------------------------------------------------------------------------
          |
         */
        Route::get('bookings/{id}', 'BarberController@getBooking');
        /*
          |--------------------------------------------------------------------------
          | API Routes Update Phone
          |--------------------------------------------------------------------------
          |
         */
        Route::post('updatephone', 'BarberController@updatePhone');
        /*
          |--------------------------------------------------------------------------
          | API Routes Update Bussiness Hours
          |--------------------------------------------------------------------------
          |
         */
        Route::post('updatehours', 'BarberController@updateHours');
        /*
          |--------------------------------------------------------------------------
          | API Routes add Manual Booking
          |--------------------------------------------------------------------------
          |
         */
        Route::post('manulbooking', 'BarberController@manulBooking');

        /*
          |--------------------------------------------------------------------------
          | API Routes updateApproval
          |--------------------------------------------------------------------------
          |Approve or reject Appoitment
         */
        Route::post('updateapproval', 'BarberController@updateApproval');
        /*
          |--------------------------------------------------------------------------
          | API Routes savelegaldetails
          |--------------------------------------------------------------------------
          |
         */
        Route::post('saveacceptance', 'BarberController@updateAcceptance');
        /*
          |--------------------------------------------------------------------------
          | API Routes savelegaldetails
          |--------------------------------------------------------------------------
          |
         */
        Route::post('savelegaldetails', 'BarberController@saveLegalDetails');
        /*
          |--------------------------------------------------------------------------
          | API Routes Cancel
          |--------------------------------------------------------------------------
          |
         */
        Route::post('cancelappiontment', 'BarberController@cancelAppiontment');
        /*
          |--------------------------------------------------------------------------
          | API Routes completejob
          |--------------------------------------------------------------------------
          |
         */
        Route::post('completejob', 'BarberController@completeJob');
        /*
          |--------------------------------------------------------------------------
          | API Charging barbder
          |--------------------------------------------------------------------------
          |
         */
        Route::post('charge', 'BarberController@charge');
        /*
          |--------------------------------------------------------------------------
          | API Get Barber Status
          |--------------------------------------------------------------------------
          |
         */
        Route::post('get_account_status', 'BarberController@getStatus');
        /*
          |--------------------------------------------------------------------------
          | API Routes Update Payment Types
          |--------------------------------------------------------------------------
          |
         */
        Route::post('updatePaymentType', 'BarberController@updatePaymentType');

        
	    Route::post('barber_update_time', 'BarberController@updateBarberTime');

        Route::post('request_status', 'BarberController@requestStatus');
    });

});