<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('test', 'BarberController@test');


Route::get('/', function () {
//    return view('emails.user_invoice_email');
    if (Auth::check()) {
        return Redirect::to('/dashboard');
    } else {
        $data['title'] = 'Taper';
        return view('login', $data);
    }
});
Route::get('updateAddressProfessionals', 'BarberController@updateAddressProfessionals');
Route::get('updateAddressProfessionals/{barberId}', 'BarberController@updateAddressProfessionals');
Route::get('/cronForNotification', 'BarberController@cronForNotification');
Route::get('/cron_barber_notification', 'BarberController@barberCron');
Route::get('/cron_reset_send_status_barber', 'BarberController@resetSendStatusBarber');
Route::get('/deeplink/barber/{id}', function () {
    return view('deeplink.example');
});
Route::get('/deeplink/other', function () {
    return view('deeplink.index');
});
Route::get('/professionals', 'BarberController@professionals');
Route::get('/professional/{id}', 'BarberController@professionalDetail');
Route::post('/', 'AuthController@adminLogin');
Route::group(['middleware' => 'nocache'], function () {
    Auth::check();
    Route::get('dashboard', 'AdminController@dashboard');
    Route::get('testAction', 'AdminController@testAction');
    Route::get('faqs', 'AdminController@faqs');
    Route::get('addfaq', 'AdminController@addFaq');
    Route::post('addfaq', 'AdminController@storeFaq');
    Route::get('editfaq/{id}', 'AdminController@editFaq');
    Route::get('deletefaq/{id}', 'AdminController@deleteFaq');

    Route::get('promations', 'AdminController@promations');
    Route::get('addpromations', 'AdminController@addPromations');
    Route::post('addpromations', 'AdminController@storePromations');
    Route::get('editpromations/{id}', 'AdminController@editPromations');
    Route::get('deletepromo/{id}', 'AdminController@deletePromo');

    Route::get('customers', 'AdminController@users');
    Route::get('customerdetail/{id}', 'AdminController@usersDetails');
    Route::post('changeuserpassword', 'AdminController@changeBarbderPassword');
    Route::post('changeuserdetails', 'AdminController@changeUserDetails');
    Route::get('professionals', 'AdminController@barbaers');
    Route::get('addbarber', 'AdminController@addBarbder');
    Route::post('addbarber', 'AdminController@storeBarber');
    Route::get('professionaldetail/{id}', 'AdminController@barbersDetails');
    Route::get('deletebarder/{id}', 'AdminController@deleteBarder');
    Route::get('barber_images/{id}', 'AdminController@barberImages');
    Route::get('delete_barber_image/{id}', 'AdminController@deleteBarberImage');
    Route::post('add_barber_image', 'AdminController@addBarberImage');

    
    Route::post('changebarberpassord', 'AdminController@changeBarbderPassword');
    Route::post('changebarbderdetails', 'AdminController@changeBarberDetails');
    Route::post('addservice', 'AdminController@addService');
    Route::post('addcategory', 'AdminController@addCategory');
    Route::post('updateservice', 'AdminController@updateService');
    Route::post('updatecategory', 'AdminController@updateCategory');
    Route::get('deleteservice/{id}', 'AdminController@deleteService');
    Route::get('deletecategory/{id}', 'AdminController@deleteCategory');
    Route::post('updatehours', 'AdminController@updateHours');

    Route::get('payments', 'AdminController@payments');
    Route::get('export/{type}', 'AdminController@export');

    Route::get('adminlogout', function() {
        Auth::logout();
        return Redirect::to('/');
    });
//    Route::get('/{id}', function() {
//        return Redirect::to('https://taperup.com/download');
//    });
    
    //professional routes
    Route::get('professional_types', 'AdminController@getProfessionalTypes');
    Route::get('add_professional_type', 'AdminController@addProfessionalTypeView');
    Route::post('add_professional_type', 'AdminController@addProfessionalType');
    Route::get('edit_professional_type/{id}', 'AdminController@editProfessionalTypeView');
    Route::post('update_professional_type/{id}', 'AdminController@updateProfessionalType');
    Route::get('delete_professional_type/{id}', 'AdminController@deleteProfessionalType');
});


Route::get('/invoice', function () {
    return view('emails.invoice_email');
});