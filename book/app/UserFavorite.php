<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFavorite extends Model
{
    protected $table='user_favorites';
    
    public function barber(){
        return $this->hasOne('App\User','id','barber_id');
    }
}
