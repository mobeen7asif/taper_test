<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function getCategory() {
        return $this->hasOne('App\Category','id','category_id');
        
    }
}
