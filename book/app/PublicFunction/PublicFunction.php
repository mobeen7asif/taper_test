<?php 
use App\Mail\EmailNotification;
function android($device_id, $user_id, $message) {
    set_time_limit(0);
//    $savenotification = new UserNotifications;
//    $savenotification->user_id = $user_id;
//    $savenotification->nType = $type;
//    $savenotification->type_id = $type_id;
//    $savenotification->message = $message;
//    $savenotification->nTime = Carbon::now();
//    $savenotification->save();
    $gcmKey = "AIzaSyCrsDlYWqDaEcexx9vA07yys97Ubh_JXBM";
    $url = 'https://android.googleapis.com/gcm/send';
    $registatoin_ids = array($device_id);
    $data = array( 'message' => $message, 'payload' => array('user_id' => $user_id, 'text' => 'There is Update in Event', 'timeago' => 'Just Now', 'badge' => 1));

    $fields = array(
        'registration_ids' => $registatoin_ids,
        'data' => $data
    );
    $headers = array(
        'Authorization: key=' . $gcmKey,
        'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return TRUE;
}

function emailNotification($emailData){
    Mail::to($emailData['data']['mailData']['email'])->send(new EmailNotification($emailData));
}

function checkSetValue($array, $key=false){
    if($key)
      return (isset($array[$key]) && $array[$key]) ? $array[$key] : '';
    else
       return (isset($array) && $array) ? $array : '';
}

function Pagination($data, $limit = null, $current = null, $adjacents = null)
{
    $result = array();

    if (isset($data, $limit) === true)
    {
        $result = range(1, ceil($data / $limit));

        if (isset($current, $adjacents) === true)
        {
            if (($adjacents = floor($adjacents / 2) * 2 + 1) >= 1)
            {
                $result = array_slice($result, max(0, min(count($result) - $adjacents, intval($current) - ceil($adjacents / 2))), $adjacents);
            }
        }
    }

    return $result;
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
      return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
  } else {
      return $miles;
  }
}

function thousandsCurrencyFormat($num) {
  $x = round($num);
  $x_number_format = number_format($x);
  if($num>=1000){
    $x_array = explode(',', $x_number_format);
    $x_parts = array('k', 'mi', 'b', 't');
    $x_count_parts = count($x_array) - 1;
    $x_display = $x;
    $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
    $x_display .= $x_parts[$x_count_parts - 1];
  }else{
      $x_display = $num;
  }
  return $x_display;
}

function getAddressByLatLon($lat, $lon)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lon.'&sensor=false'
    ));
    $address = json_decode(curl_exec($curl));
    curl_close($curl);
    if(isset($address->results[0]->address_components)){
        $country = $state = $city = "";
        foreach ($address->results[0]->address_components as $key) {
          if($key->types[0] == 'locality')
          {
            $city = $key->long_name;
          }
          if($key->types[0] == 'administrative_area_level_1')
          {
            $state = $key->short_name;
          }
          if($key->types[0] == 'country')
          {
            $country = $key->short_name;
          }
        }
        return ['country' => $country, 'state' => $state, 'city' => $city];
    }else{
        return false;
    }
}