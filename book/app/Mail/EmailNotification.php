<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotification extends Mailable
{
    use Queueable, SerializesModels;
    
    public $notificationData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailData)
    {
        $this->notificationData = new \stdClass;
        $this->notificationData = json_decode(json_encode($emailData['data']));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->notificationData);
        return $this->from('info@taperup.com', 'Taper')
                ->subject($this->notificationData->mailData->subject)
                ->view('emails.emailNotifications');
    }
}
