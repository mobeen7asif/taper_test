<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use App\Notification;
use App\Booking;
use App\Faq;
use App\Promotion;
use Carbon\Carbon;
use App\UserStripe;
use App\Rating;
use App\Review;
use App\UserFavorite;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\History;
use App\UserReporting;
use Illuminate\Support\Facades\Mail;
use App\Service;
use App\Category;
use App\BussnessHour;
use App\Gallery;
use App\ProfessionalType;
class UserController extends Controller {

    function barbers() {
//         \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
//        $account = \Stripe\Account::retrieve('acct_1AMhBqGXdYCE2VsK');
//        $account->delete();
//        return Response::json($account);
        $skip = 15 * $_GET['offset'];
        $lat = $_GET['lat'];
        $lng = $_GET['lng'];
        $barbers = User::with(array('getReviews', 'getReviews.getUser', 'getBookings','getImages'))
                ->selectRaw("*,
            ( 6371 * acos( cos( radians($lat) ) *
            cos( radians(shop_lat) ) *
            cos( radians(shop_long) - radians($lng) ) + 
            sin( radians($lat) ) *
            sin( radians(shop_lat) ) ) ) 
            AS distance")
                ->where('is_login', 1)
                ->having("distance", "<", 100)
                ->orderBy("distance", "asc")
                ->where('user_type', '!=', 'User')
                ->take(15)->skip($skip)
                ->get();

        foreach ($barbers as $b) {
            $rating = Rating::where('barber_id', $b->id)->first();
            if ($rating) {
                $b->rating = $rating->rating;
            } else {
                $b->rating = 0;
            }
            $b->favorites = UserFavorite::where(array('barber_id' => $b->id))->get();
        }

        return Response::json(array('status' => 'success', 'successData' => $barbers));
    }

    function getNotification($id) {
        $data['notifications'] = Notification::with('getUser')->where('to_user', $id)->take(10)->orderBy('created_at', 'desc')->get();
        Notification::where('to_user', $id)->update(array('is_read' => 1));
        $data['unread'] = Notification::where('to_user', $id)->where('is_read', 0)->count();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function getAllNotification($id) {
        $data['notifications'] = Notification::where('to_user', $id)->orderBy('created_at', 'desc')->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }
function getUserDetails($user_id) {
         $check_user = User::find($user_id);
         if ($check_user->user_type != 'User') {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $user = UserStripe::where('barber_id', $check_user->id)->first();
                $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
                $user->stripe_payout_account_info = json_encode($account);
                $user->save();
                $check_user->account = json_encode($account);
                $check_user->account_status = $account->legal_entity->verification->status;
                if (isset($account->external_accounts->data[0])) {
                    $check_user->account_last_four = $account->external_accounts->data[0]->last4;
                    $check_user->account_card_brand = $account->external_accounts->data[0]->brand;
                } else {
                    $check_user->account_last_four = '';
                    $check_user->account_card_brand = '';
                }
            }
            $check_user->services = Service::where('barber_id', $check_user->id)->get();
            $check_user->categories = Category::where('barber_id', $check_user->id)->get();
            $check_user->bussiness_hours = BussnessHour::where('barber_id', $check_user->id)->first();
            $check_user->gallery_images = Gallery::where('barber_id', $check_user->id)->orderBy('created_at', 'desc')->get();
            $check_user->bookings = Booking::where('date', '>=', date('Y-m-d'))->where('barber_id', $check_user->id)->with('User')->orderBy('date', 'desc')->get();
            $check_user->notificationcount = Notification::where(array('to_user' => $check_user->id, 'is_read' => 0))->count();

            $check_user->professional_types = ProfessionalType::all();
            return Response::json(array('status' => 'success', 'successData' => $check_user));
    }
    function markNotificationRead($id) {
        Notification::where('to_user', $id)->update(array('is_read' => 1));
        return Response::json(array('status' => 'success', 'successMessage' => "Updated"));
    }

    function getBarberDetails($id) {
        $barbers = User::with(array('getTimes', 'getImages', 'getService', 'getReviews', 'getReviews.getUser', 'getBookings'))->where('id', $id)->get();
        foreach ($barbers as $b) {
            if($b->announcment_date){
               $b->announcment_date= $this->timeago($b->announcment_date);
            }else{
                 $b->announcment_date='';
            }
            $rating = Rating::where('barber_id', $b->id)->first();
            if ($rating) {
                $b->rating = $rating->rating;
            } else {
                $b->rating = 0;
            }
        }
        $data['barbers'] = $barbers;
        $data['slots'] = Booking::where(array('barber_id' => $id, 'date' => date('Y-m-d')))->get();
        $data['favorites'] = UserFavorite::where(array('barber_id' => $id))->get();
        $data['category'] = Category::where('barber_id', $id)->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function getBarberDetailsUsername($username) {
        $barbers = User::with(array('getTimes', 'getImages', 'getService', 'getReviews', 'getReviews.getUser', 'getBookings'))->where('username', $username)->get();
        foreach ($barbers as $b) {
            $rating = Rating::where('barber_id', $b->id)->first();
            if ($rating) {
                $b->rating = $rating->rating;
            } else {
                $b->rating = 0;
            }
            $b->favorites = UserFavorite::where(array('barber_id' => $b->id))->get();
        }
        $data['barbers'] = $barbers;
        $data['favorites'] = UserFavorite::where(array('barber_id' => $b->id))->get();
        $data['category'] = Category::where('barber_id', $b->id)->get();
        $data['slots'] = Booking::where(array('barber_id' => $barbers[0]->id, 'date' => date('Y-m-d')))->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function getBarberBookings() {

        $data['slots'] = Booking::where(array('barber_id' => $_POST['barber_id'], 'date' => $_POST['date']))->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function getFaqs() {
        $data['faqs'] = Faq::orderBy('created_at', 'desc')->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function getProm() {
        $data['promo'] = Promotion::orderBy('created_at', 'desc')->get();
        return Response::json(array('status' => 'success', 'successData' => $data));
    }

    function checkOverLap() {
        $endTime = $_POST['end_time'];
        $time = strtotime($_POST['start_time']);
        $endTime = date("H:i", strtotime("+$endTime minutes", $time));
        $check_booking_overlap = Booking::where('status', '!=', 'Rejected')->where(array('date' => $_POST['date'], 'barber_id' => $_POST['user_id']))->get();
        $overlaps = 0;
        $date = $_POST['date'];
        $startTime = $_POST['start_time'];
        $userStartTime = "$date $startTime";
        $ust = Carbon::parse($userStartTime)->format('Y-m-d H:i');
        $userEndTime = "$date $endTime";
        $uet = Carbon::parse($userEndTime)->format('Y-m-d H:i');
        foreach ($check_booking_overlap as $boookings) {
            $back_endtime = "$date $boookings->end_time";
            $bet = Carbon::parse($back_endtime)->format('Y-m-d H:i');
            $back_starttime = "$date $boookings->start_time";
            $bst = Carbon::parse($back_starttime)->format('Y-m-d H:i');
            if ($bet > $ust && $uet > $bst) {
                $overlaps ++;
            }
        }
        if ($overlaps > 0) {
            return Response::json(array('status' => 'error', 'errorMessage' => 'This Timing OverLaps The Secdule'));
        } else {
            return Response::json(array('status' => 'success', 'successMessage' => 'No OverLap', 'successData' => $endTime));
        }
    }

    function addBooking() {
        $barber = User::find($_POST['barber_id']);
        $user = User::find($_POST['user_id']);

        if ($_POST['payment'] == 'stripe') {

//            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
//            $barber_stripe = UserStripe::where('barber_id', $_POST['barber_id'])->first();
//            $account = \Stripe\Account::retrieve($barber_stripe->stripe_payout_account_id);
////            return Response::json($account);exit;
//            if ($account->legal_entity->verification->status != 'verified') {
//                return Response::json(array('status' => 'error', 'errorMessage' => 'Owner Does Not Have this Source Please Chose AlterNative Way of Payment'));
//            }
            $add_stripe_booking = new Booking;
            $add_stripe_booking->barber_id = $_POST['barber_id'];
            $add_stripe_booking->start_time = $_POST['start_time'];
            $add_stripe_booking->end_time = $_POST['end_time'];
            $add_stripe_booking->date = $_POST['date'];
            $add_stripe_booking->service_at = $_POST['service_at'];
            $add_stripe_booking->status = 'Pending';
            $add_stripe_booking->price = $_POST['price'];
            $add_stripe_booking->payment = 'Stripe';
            $add_stripe_booking->services = $_POST['services'];
            $add_stripe_booking->service_time = $_POST['service_time'];
            $add_stripe_booking->user_id = $_POST['user_id'];
            $add_stripe_booking->save();
            $originalDate = $_POST['date'];
            $newDate = date("l", strtotime($originalDate));
            if ($user->profile_pic) {
                $pic = $user->profile_pic;
                //$pic = asset('public/images/' . $user->profile_pic);
            } else {
                $pic = $user->social_pic;
            }
            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));

            //Send notifications
            $notificatinText = $user->first_name . ' ' . $user->last_name . ' requested a new appiontment for ' . $newDate . ' at ' . $time;

            $notificatinSubject = 'New Appointment';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $pic, $add_stripe_booking->id, 'Stripe');
//            emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Stripe');
            }
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Stripe');
            }
            $this->userInvoiceMail($user, $barber, $_POST['services'], $_POST['price']);
            $this->barberInvoiceMail($user, $barber, $_POST['services'], $_POST['price']);
            return Response::json(array('status' => 'success', 'successData' => $add_stripe_booking, 'successMessage' => 'Stripe Booking Added'));
        }
        if ($_POST['payment'] == 'cash') {

            if ($barber->cash_count < 3) {
                //return Response::json(array('status' => 'error', 'errorMessage' => 'Cash Method Not Available For This Barber Please Use Card'));
            }

            $add_stripe_booking = new Booking;
            $add_stripe_booking->barber_id = $_POST['barber_id'];
            $add_stripe_booking->start_time = $_POST['start_time'];
            $add_stripe_booking->end_time = $_POST['end_time'];
            $add_stripe_booking->date = $_POST['date'];
            $add_stripe_booking->service_at = $_POST['service_at'];
            $add_stripe_booking->status = 'Pending';
            $add_stripe_booking->price = $_POST['price'];
            $add_stripe_booking->payment = 'Cash';
            $add_stripe_booking->services = $_POST['services'];
            $add_stripe_booking->service_time = $_POST['service_time'];
            $add_stripe_booking->user_id = $_POST['user_id'];
            $add_stripe_booking->save();

            if ($user->profile_pic) {
                $pic = $user->profile_pic;
                //$pic = asset('public/images/' . $user->profile_pic);
            } else {
                $pic = $user->social_pic;
            }

            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));
            $newDate = date("l", strtotime($_POST['date']));

            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));
            $newDate = date("l", strtotime($_POST['date']));

            //Send notifications
            $notificatinText = $user->first_name . ' ' . $user->last_name . ' requested a new appiontment for ' . $newDate . ' at ' . $time;
            $notificatinSubject = 'New Appointment';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $pic, $add_stripe_booking->id, 'Cash');
//            emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Cash');
            }
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Cash');
            }
            
            $this->userInvoiceMail($user, $barber, $_POST['services'], $_POST['price']);
            $this->barberInvoiceMail($user, $barber, $_POST['services'], $_POST['price']);
            return Response::json(array('status' => 'success', 'successData' => $add_stripe_booking, 'successMessage' => 'Cash Booking Added'));
        }
    }
    
    function userInvoiceMail($user, $barber, $services, $price){
        $viewData['barber'] = $barber;
        $viewData['services'] = explode(',', $services);
        $viewData['price'] = $price;
//        Mail::send('emails.user_invoice_email', $viewData, function ($m) use ($user) {
//            $m->from('Info@taperup.com', 'Taper');
//            $m->to($user->email, $user->first_name.' '.$user->last_name)->subject('Your New Booking');
//        });
    }
    
    function barberInvoiceMail($user, $barber, $services, $price){
        $viewData['user'] = $user;
        $viewData['services'] = explode(',', $services);
        $viewData['price'] = $price;
//        Mail::send('emails.barber_invoice_email', $viewData, function ($m) use ($barber) {
//            $m->from('Info@taperup.com', 'Taper');
//            $m->to($barber->email, $barber->username)->subject('Your New Booking');
//        });
    }
            
    function addCustomBooking() {
        $barber = User::find($_POST['barber_id']);
        $user = User::find($_POST['user_id']);

        if ($_POST['payment'] == 'stripe') {

//            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
//            $barber_stripe = UserStripe::where('barber_id', $_POST['barber_id'])->first();
//            $account = \Stripe\Account::retrieve($barber_stripe->stripe_payout_account_id);
////            return Response::json($account);exit;
//            if ($account->legal_entity->verification->status != 'verified') {
//                return Response::json(array('status' => 'error', 'errorMessage' => 'Owner Does Not Have this Source Please Chose AlterNative Way of Payment'));
//            }
            $add_stripe_booking = new Booking;
            $add_stripe_booking->barber_id = $_POST['barber_id'];
            $add_stripe_booking->start_time = $_POST['start_time'];
            $add_stripe_booking->end_time = $_POST['end_time'];
            $add_stripe_booking->date = $_POST['date'];
            $add_stripe_booking->service_at = $_POST['service_at'];
            $add_stripe_booking->status = 'Custom';
            $add_stripe_booking->price = $_POST['price'];
            $add_stripe_booking->payment = 'Stripe';
            $add_stripe_booking->services = $_POST['services'];
            $add_stripe_booking->description = $_POST['description'];
            $add_stripe_booking->service_time = 0;
            $add_stripe_booking->user_id = $_POST['user_id'];
            $add_stripe_booking->save();
            $originalDate = $_POST['date'];
            $newDate = date("l", strtotime($originalDate));
            if ($user->profile_pic) {
                $pic = asset('public/images/' . $user->profile_pic);
            } else {
                $pic = $user->social_pic;
            }
            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));

            //Send notifications
            $notificatinText = $user->first_name . ' ' . $user->last_name . ' requested a new custom appiontment for ' . $newDate . ' at ' . $time;
            $notificatinSubject = 'New Appointment';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $pic, $add_stripe_booking->id, 'Stripe');
//            emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Stripe');
            }
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Stripe');
            }
            return Response::json(array('status' => 'success', 'successData' => $add_stripe_booking, 'successMessage' => 'Stripe Booking Added'));
        }
        if ($_POST['payment'] == 'cash') {

            if ($barber->cash_count < 3) {
                //return Response::json(array('status' => 'error', 'errorMessage' => 'Cash Method Not Available For This Barber Please Use Card'));
            }
            
            $add_stripe_booking = new Booking;
            $add_stripe_booking->barber_id = $_POST['barber_id'];
            $add_stripe_booking->start_time = $_POST['start_time'];
            $add_stripe_booking->end_time = $_POST['end_time'];
            $add_stripe_booking->date = $_POST['date'];
            $add_stripe_booking->service_at = $_POST['service_at'];
            $add_stripe_booking->status = 'Custom';
            $add_stripe_booking->price = $_POST['price'];
            $add_stripe_booking->payment = 'Cash';
            $add_stripe_booking->services = $_POST['services'];
            $add_stripe_booking->description = $_POST['description'];
            $add_stripe_booking->service_time = 0;
            $add_stripe_booking->user_id = $_POST['user_id'];
            $add_stripe_booking->save();

            if ($user->profile_pic) {
                $pic = asset('public/images/' . $user->profile_pic);
            } else {
                $pic = $user->social_pic;
            }

            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));

            //Send notifications
            $notificatinText = ' requested a new appiontment';
            $notificatinSubject = 'New Appointment';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $pic, $add_stripe_booking->id, 'Cash');
//            emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Cash');
            }
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $add_stripe_booking->id, $barber->id, $_POST['date'], 'Cash');
            }
            return Response::json(array('status' => 'success', 'successData' => $add_stripe_booking, 'successMessage' => 'Cash Booking Added'));
        }
    }
    
    function updateBooking() {
        $barber = User::find($_POST['barber_id']);
        $user = User::find($_POST['user_id']);
        if (isset($_POST['booking_id'])) {
            $bookingId = $_POST['booking_id'];
            $update_booking = Booking::find($bookingId);
            $update_booking->start_time = $_POST['start_time'];
            $update_booking->end_time = $_POST['end_time'];
            $update_booking->date = $_POST['date'];
            $update_booking->service_at = $_POST['service_at'];
            $update_booking->status = 'Pending';
            $update_booking->price = $_POST['price'];
            $update_booking->services = $_POST['services'];
            $update_booking->service_time = $_POST['service_time'];
            $bookingBy = 'Stripe';
            if ($_POST['payment'] == 'stripe') {
                $update_booking->payment = 'Stripe';
            } elseif ($_POST['payment'] == 'cash') {
                $update_booking->payment = 'Cash';
                $bookingBy = 'Cash';
            }
            $update_booking->save();

            $originalDate = $_POST['date'];
            $newDate = date("l", strtotime($originalDate));
            if ($user->profile_pic) {
                $pic = asset('public/images/' . $user->profile_pic);
            } else {
                $pic = $user->social_pic;
            }
            $app_time = $_POST['start_time'];
            $time = date("g:i a", strtotime("$app_time"));

            //Send notifications
            $notificatinText = $user->first_name . ' ' . $user->last_name . ' updated appiontment for ' . $newDate . ' at ' . $time;
            $notificatinSubject = 'Update Appointment';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $pic, $add_stripe_booking->id, 'Stripe');
//            emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $update_booking->id, $barber->id, $_POST['date'], $bookingBy);
            }
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $_POST['user_id'], $notificatinText, $user->first_name, $user->location, $_POST['service_at'], $_POST['start_time'], $_POST['service_time'], $_POST['price'], $_POST['services'], $pic, $update_booking->id, $barber->id, $_POST['date'], $bookingBy);
            }
            return Response::json(array('status' => 'success', 'successData' => $update_booking, 'successMessage' => $bookingBy . ' Booking Updated'));
        } else {
            return Response::json(array('status' => 'success', 'successData' => '', 'successMessage' => 'Please Provide Valid Booking Id.'));
        }
    }

    function changePassword() {
        $validator = Validator::make(
                        array(
                    'password' => $_POST['password'],
                    'new_password' => $_POST['new_password'],
                    'user_id' => $_POST['user_id']
                        ), array(
                    'user_id' => 'required',
                    'new_password' => 'required',
                    'password' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        }
        $user = User::find($_POST['user_id']);
        $password = $user->password;
        if (Hash::check($_POST['password'], $password)) {
            $newpass = Hash::make($_POST['new_password']);
            User::where('id', '=', $_POST['user_id'])->update(['password' => $newpass]);
            return Response::json(array('status' => 'success', 'successMessage' => 'Password Updated Successfully'));
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'Invalid Old Password'));
        }
    }

    function deleteCard($id) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $cus = User::find($id);

        $customer = \Stripe\Customer::retrieve($cus->stripe_id);
        if (isset($customer->sources->data[0]->id)) {
            $customer->sources->retrieve($customer->sources->data[0]->id)->delete();
            $cus->card_brand = '';
            $cus->card_last_four = '';
            $cus->save();
            return Response::json(array('status' => 'success', 'successMessage' => 'Card Is Deleted'));
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'No Card Is Attached'));
        }
    }

    function addCard() {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $stripeToken = $_POST['stripeToken'];
        $cus = User::find($_POST['user_id']);
        if ($cus->stripe_id) {
            $cus->updateCard($stripeToken);
        } else {
            $cus->newSubscription('taper', 'taper')->create($stripeToken);
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Card Is Added', 'stripe_id' => $cus->stripe_id, 'card_last_four' => $cus->card_last_four, 'card_brand' => $cus->card_brand));
    }

    function addReviewRating() {
        $validator = Validator::make(
                        array(
                    'rating' => $_POST['rating'],
                    'review' => $_POST['review'],
                    'user_id' => $_POST['user_id'],
                    'barber_id' => $_POST['barber_id'],
                    'booking_id' => $_POST['booking_id'],
                    'tip' => $_POST['tip']
                        ), array(
                    'user_id' => 'required',
                    'rating' => 'required',
                    'user_id' => 'required',
                    'barber_id' => 'required',
                    'booking_id' => 'sometimes|required',
                    'tip' => 'sometimes|required',
                        )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        }
        $check_rating = Rating::where('barber_id', $_POST['barber_id'])->first();
        if ($check_rating) {
            $point = $_POST['rating'] + $check_rating->all_points;
            $users = $check_rating->users + 1;
            $check_rating->rating = $point / $users;
            $check_rating->all_points = $point;
            $check_rating->users = $users;
            $check_rating->save();
        } else {
            $add_rating = new Rating;
            $add_rating->barber_id = $_POST['barber_id'];
            $add_rating->rating = $_POST['rating'];
            $add_rating->all_points = $_POST['rating'];
            $add_rating->users = 1;
            $add_rating->save();
        }
        $check_review = Review::where(array('booking_id' => $_POST['booking_id'], 'reviewed_by' => $_POST['user_id']))->first();
        if (!$check_review) {
            $add_review = new Review;
            $add_review->reviewed_by = $_POST['user_id'];
            $add_review->barber_id = $_POST['barber_id'];
            $add_review->text = $_POST['review'];
            $add_review->booking_id = $_POST['booking_id'];
            $add_review->save();
        }
        $user = User::find($_POST['user_id']);
        $barber = User::find($_POST['barber_id']);

        // Charge if Tip Made
        if (isset($_POST['booking_id'])) {
            if (isset($_POST['tip']) && $_POST['tip'] != '') {
                $appointmentId = $_POST['booking_id'];
                $booking = Booking::find($appointmentId);
                if ($booking->user_id)
                    $user = User::find($booking->user_id);
                else
                    $user = '';

                $add_history = new History;
                $add_history->barber_id = $booking->barber_id;
                $add_history->total_payments = $booking->price;
                $add_history->appiontment_id = $booking->id;
                $add_history->payment_type = 'Stripe';
                $get_history_count = History::where('barber_id', $booking->barber_id)->sum('app_blance');

                $booking_price = ($booking->price * $_POST['tip'] / 100);

                //$app_fee = round(floatval(($booking_price * 7 / 100) + 0.30), 2);
                $barber_stripe = UserStripe::where('barber_id', $booking->barber_id)->first();
                $app_fee = 0;
                if ($booking_price > 0) {
                    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                    try {
                        \Stripe\Charge::create(array(
                            "amount" => intval(($booking_price + $app_fee) * 100),
                            "currency" => "usd",
                            "customer" => "$user->stripe_id", // obtained with Stripe.js
                            "description" => "Charge for Appointment Tip",
                            "destination" => "$barber_stripe->stripe_payout_account_id",
                            "application_fee" => $app_fee * 100
                        ));
                    } catch (\Stripe\Error\Card $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (\Stripe\Error\RateLimit $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (\Stripe\Error\Authentication $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (\Stripe\Error\ApiConnection $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (\Stripe\Error\Base $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    } catch (Exception $e) {
                        return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                    }

                    $add_history->earning = ($booking_price > 0) ? ($booking_price - $app_fee) : 0;
                }

                $add_history->app_blance = 0;
                $add_history->save();
            }
        }

        //Send notifications
        $notificatinText = $user->first_name . ' has given ' . $_POST['rating'] . ' rating and review is ' . $_POST['review'];
        $notificatinSubject = 'New Review';
//            $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//            $emailData['data']['mailOtherData'] = $this->mapData($user, $barber);
//            emailNotification($emailData);

        if ($barber->platform == 'android') {
            $this->androidReview($barber->device_id, $_POST['user_id'], $notificatinText, $barber->id);
        }
        if ($barber->platform == 'ios') {
            $this->iosReview($barber->device_id, $_POST['user_id'], $notificatinText, $barber->id);
        }
        return Response::json(array('status' => 'success', 'successMessage' => "Feedback Added"));
    }

    function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        return $miles = $dist * 60 * 1.1515;
    }

    function search() {
        $key = $_GET['key'];
        $skip = 15 * $_GET['offset'];
        $lat = $_GET['lat'];
        $lng = $_GET['lng'];
        $userType = (isset($_GET['user_type'])) ? $_GET['user_type'] : '';
        $services_user=Service::select('barber_id')->where('service_type','like', "%$key%")->groupBy('barber_id')->get()->toArray();
        $barbers = User::where(function ($query) use($key,$services_user) {
                    $query->where('shop_name', 'like', "%$key%")
                    ->orWhere('username', 'like', "%$key%")
                    ->orWhere('shop_location', 'like', "%$key%")
                     ->orWhereIn('id', $services_user)
                    //->orWhere('first_name', 'like', "%$key%")
                    //->orWhere('last_name', 'like', "%$key%")
                    ;
                })
                ->selectRaw("*,
            ( 6371 * acos( cos( radians($lat) ) *
            cos( radians(lat) ) *
            cos( radians(lng) - radians($lng) ) + 
            sin( radians($lat) ) *
            sin( radians(lat) ) ) ) 
            AS distance")
                ->with(array('getReviews', 'getReviews.getUser', 'getUserFavourites.barber','getImages'))
                ->take(15)->skip($skip)
                ->where('user_type', '!=', 'User')
                ->where('is_login', 1);
        if ($userType != '') {
            $barbers->where('user_type', 'like', '%' . $userType . '%');
        }
        $barbers = $barbers->get();
        foreach ($barbers as $b) {
            $rating = Rating::where('barber_id', $b->id)->first();
            if ($rating) {
                $b->rating = $rating->rating;
            } else {
                $b->rating = 0;
            }
            $b->favorites = UserFavorite::where(array('barber_id' => $b->id))->get();
        }
        return Response::json(array('status' => 'success', 'successData' => $barbers));
    }

    function android($device_id, $user_id, $message, $first_name, $location, $service_at, $start_time, $service_time, $price, $services, $pic, $booking_id, $barber_id, $date, $payment) {

        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $gcmKey = "AIzaSyCenDLL9Pb1nayFHzcdUEg5bXB3mkU85vE";
        $url = 'https://android.googleapis.com/gcm/send';
        $registatoin_ids = array($device_id);
        $user = User::find($user_id);
        $data = array('message' => $message, 'payload' => array('payment' => $payment, 'user' => $user,'date' => $date, 'booking_id' => $booking_id, 'user_id' => $user_id, 'service_time' => $service_time, 'price' => $price, 'services' => $services, 'pic' => $pic, 'name' => $first_name, 'location' => $location, 'service_at' => $service_at, 'start_time' => $start_time, 'location' => $location, 'timeago' => 'Just Now', 'badge' => 1));
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $data
        );
        $headers = array(
            'Authorization: key=' . $gcmKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return TRUE;
    }

    function appointments() {
        if ($_GET['user_type'] != 'User') {
            $booking = Booking::where(array('barber_id' => $_GET['user_id']))
                            ->where('status', '!=', 'Pending')
                            ->with('User')->orderBy('created_at', 'desc')->get();
            foreach ($booking as $book) {
                $can_review = Review::where('booking_id', $book->id)->first();
                $book->can_review = 1;
                if ($can_review) {
                    $book->can_review = 0;
                }
            }
            $bookingpending = Booking::where(array('barber_id' => $_GET['user_id'], 'status' => 'Pending'))
                            //->where('created_at', '>', \Carbon\Carbon::now()->subHour())
                            ->with('User')->orderBy('created_at', 'desc')->get();
            $bookings = $booking->merge($bookingpending);
            $total_earning = History::where('barber_id', $_GET['user_id'])->sum('earning');
            $app_blance = History::where('barber_id', $_GET['user_id'])->sum('app_blance');
            $total_jobs = History::where('barber_id', $_GET['user_id'])->count();
            return Response::json(array('status' => 'success', 'successData' => $bookings, 'total_earning' => $total_earning, 'app_blance' => $app_blance, 'total_jobs' => $total_jobs));
        }
        if ($_GET['user_type'] == 'User') {
//            $bookings = Booking::where('user_id', $_GET['user_id'])->with(array('Barber', 'Rating'))->get();
            $booking = Booking::where(array('user_id' => $_GET['user_id']))
                            ->where('status', '!=', 'Pending')
                            ->with('Barber')->orderBy('created_at', 'desc')->get();
            foreach ($booking as $book) {
                $can_review = Review::where('booking_id', $book->id)->first();
                $book->can_review = 1;
                if ($can_review) {
                    $book->can_review = 0;
                }
            }
            $bookingpending = Booking::where(array('user_id' => $_GET['user_id'], 'status' => 'Pending'))
                            //->where('created_at', '>', \Carbon\Carbon::now()->subHour())
                            ->with('Barber')->orderBy('created_at', 'desc')->get();
            $bookings = $booking->merge($bookingpending);
           // Log::info(print_r($bookings->toArray(),true));

            return Response::json(array('status' => 'success', 'successData' => $bookings));
        }
    }

    function updateProfile(Request $request) {

        $validatorValues = array(
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'user_id' => $request->input('user_id')
        );
        $validatorRules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'user_id' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->input('user_id'),
            'username' => 'unique:users,username,' . $request->input('user_id')
        );

        $validator = Validator::make($validatorValues, $validatorRules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $user = User::find($request->input('user_id'));
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->mobile = $request->input('phone');
            $user->email = $request->input('email');
            $user->username = $request->input('username');
            if(isset($request['send_notifiction'])){
            $user->send_notifiction = $request->input('send_notifiction');
            }
            $user->save();
            return Response::json(array('status' => 'success', 'successData' => $user));
        }
    }

    function ios($device_id, $user_id, $message, $first_name, $location, $service_at, $start_time, $service_time, $price, $services, $pic, $booking_id, $barber_id, $date, $payment) {
        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $deviceToken = $device_id;

//    echo $message;exit;
        $user = User::find($user_id);
        $data = array('payment' => $payment, 'date' => $date, 'user' => $user, 'booking_id' => $booking_id, 'user_id' => $user_id, 'service_time' => $service_time, 'price' => $price, 'services' => $services, 'pic' => $pic, 'name' => $first_name, 'location' => $location, 'service_at' => $service_at, 'start_time' => $start_time, 'location' => $location, 'timeago' => 'Just Now');
        // Put your private key's passphrase here:
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
//            'ssl://gateway.push.apple.com:2195', $err,
                //'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return TRUE;
    }

    function addReporting() {
        $add_appiontment = new UserReporting;
        $add_appiontment->message = $_POST['message'];
        $add_appiontment->subject = $_POST['subject'];
        $add_appiontment->user_id = $_POST['user_id'];
        $add_appiontment->reporting_type = $_POST['reporting_type'];
        if ($_POST['reporting_type'] == 'appointment') {
            $add_appiontment->appiontment_id = $_POST['appiontment_id'];
            $booking = Booking::find($_POST['appiontment_id']);
            $add_appiontment->reported_id = $booking->barber_id;
        }
        $add_appiontment->save();
        return Response::json(array('status' => 'success', 'successMessage' => 'Your issue has been reported we will responce you via email '));
    }

    function iosReview($device_id, $user_id, $message, $barber_id) {
        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $deviceToken = $device_id;
//    echo $message;exit;
        $data = array('message' => $message, 'user_id' => $user_id, 'timeago' => 'Just Now', 'text' => 'review');
        // Put your private key's passphrase here:
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
//            'ssl://gateway.push.apple.com:2195', $err,
//                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return TRUE;
    }

    function androidReview($device_id, $user_id, $message, $barber_id) {
        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $gcmKey = "AIzaSyCenDLL9Pb1nayFHzcdUEg5bXB3mkU85vE";
        $url = 'https://android.googleapis.com/gcm/send';
        $registatoin_ids = array($device_id);
        $data = array('message' => $message, 'payload' => array('message' => $message, 'user_id' => $user_id, 'timeago' => 'Just Now', 'text' => 'review', 'badge' => 1));
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $data
        );
        $headers = array(
            'Authorization: key=' . $gcmKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return TRUE;
    }

    function mapData($user = '', $barber = '', $pic = '', $bookinId = '', $payment = '') {
        return $notificationEmailData = ['user_id' => checkSetValue($_POST, 'user_id'),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'location' => $user->location,
            'service_at' => checkSetValue($_POST, 'service_at'),
            'start_time' => checkSetValue($_POST, 'start_time'),
            'service_time' => checkSetValue($_POST, 'service_time'),
            'price' => checkSetValue($_POST, 'price'),
            'services' => checkSetValue($_POST, 'services'),
            'date' => checkSetValue($_POST, 'date'),
            'pic' => checkSetValue($pic),
            'bookinId' => checkSetValue($bookinId),
            'payment' => checkSetValue($payment),
            'barberData' => json_encode($barber)];
    }

    //User cancel Appointment
    function cancelAppointment(Request $request) {
        $appointmentId = $request->input('appointmentId');

        $booking = Booking::find($appointmentId);
        $barber = User::where('id', $booking->barber_id)->with('getRating')->first();

        if ($booking->user_id)
            $user = User::find($booking->user_id);
        else
            $user = '';

        $add_history = new History;
        $add_history->barber_id = $booking->barber_id;
        $add_history->total_payments = $booking->price;
        $add_history->appiontment_id = $booking->id;
        $add_history->payment_type = 'Stripe';
        $get_history_count = History::where('barber_id', $booking->barber_id)->sum('app_blance');

        $booking_price = ($booking->price * $barber->cancel_fee / 100);

        $app_fee = round(floatval(($booking_price * 7 / 100) + 0.30), 2);
        $add_history->earning = $booking_price - $app_fee;
        $add_history->app_blance = 0;

        $barber_stripe = UserStripe::where('barber_id', $booking->barber_id)->first();
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
            \Stripe\Charge::create(array(
                "amount" => intval(($booking_price + $app_fee) * 100),
                "currency" => "usd",
                "customer" => "$user->stripe_id", // obtained with Stripe.js
                "description" => "Charge for Cancel appointment",
                "destination" => "$barber_stripe->stripe_payout_account_id",
                "application_fee" => $app_fee * 100
            ));
        } catch (\Stripe\Error\Card $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\RateLimit $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\InvalidRequest $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\Authentication $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\ApiConnection $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\Base $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (Exception $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        }

        $add_history->save();

        $booking->status = 'User Rejected';
        $booking->save();
        $originalDate = $booking->date;
        $newDate = date("l", strtotime($originalDate));
        $app_time = $booking->start_time;
        $time = date("g:i a", strtotime("$app_time"));
        if ($barber->is_login == 1) {
            //Send notifications
            $notificatinText = $user->first_name . ' ' . $user->last_name . ' has cancelled appointment for ' . $newDate . ' at ' . $time;
            $notificatinSubject = 'Booking Cancelled';
//                $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $booking->id);
//                emailNotification($emailData);

            if ($barber->platform == 'android') {
                $this->cancelAppointmentAndroid($barber->device_id, $barber->id, $notificatinText, $booking->user_id);
                //$this->android($barber->device_id, $barber->id, $notificatinText, json_encode($user), $booking->user_id, $booking->id);
            }
            if ($barber->platform == 'ios') {
                $this->cancelAppointmentIos($barber->device_id, $barber->id, $notificatinText, $booking->user_id);
                //$this->ios($barber->device_id, $barber->id, $notificatinText , $booking->user_id, $booking->id);
            }
        }
        if ($barber->cash_count > 0) {
            $barber->cash_count = $barber->cash_count - 1;
            $barber->save();
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Booking Cancelled Successfully'));
    }

    function cancelAppointmentIos($device_id, $barber_id , $message , $user_id) {
        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $deviceToken = $device_id;
//    echo $message;exit;
        $data = '';
        // Put your private key's passphrase here:
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
//            'ssl://gateway.push.apple.com:2195', $err,
            'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return TRUE;
    }

    function cancelAppointmentAndroid($device_id, $barber_id , $message , $user_id) {

        set_time_limit(0);
        $savenotification = new Notification;
        $savenotification->from_user = $user_id;
        $savenotification->to_user = $barber_id;
        $savenotification->text = $message;
        $savenotification->date = date('Y-m-d');
        $savenotification->is_read = 0;
        $savenotification->save();
        $gcmKey = "AIzaSyCenDLL9Pb1nayFHzcdUEg5bXB3mkU85vE";
        $url = 'https://android.googleapis.com/gcm/send';
        $registatoin_ids = array($device_id);
        $data = array('message' => $message);
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $data
        );
        $headers = array(
            'Authorization: key=' . $gcmKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return TRUE;
    }




    //Add User Favorite Professional
    function addFavorite(Request $request) {
        $validatorValues = array(
            'user_id' => $request->input('user_id'),
            'barber_id' => $request->input('barber_id')
        );
        $validatorRules = array(
            'user_id' => 'required',
            'barber_id' => 'required'
        );
        $validator = Validator::make($validatorValues, $validatorRules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            //dd($request->input('user_id'));
            $userFavorite = new UserFavorite;
            $userFavorite->user_id = $request->input('user_id');
            $userFavorite->barber_id = $request->input('barber_id');
            $userFavorite->save();
            return Response::json(array('status' => 'success', 'successData' => 'Professional Successfully Added to Favorites.'));
        }
    }

    //Remove User Favorite Professional
    function removeFavorite(Request $request) {
        $validatorValues = array(
            'user_id' => $request->input('user_id'),
            'barber_id' => $request->input('barber_id')
        );
        $validatorRules = array(
            'user_id' => 'required',
            'barber_id' => 'required'
        );
        $validator = Validator::make($validatorValues, $validatorRules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $userFavorite = new UserFavorite;
            UserFavorite::where(['barber_id' => $request->input('barber_id'), 'user_id' => $request->input('user_id')])->delete();
            return Response::json(array('status' => 'success', 'successData' => 'Professional Successfully Removed from Favorites.'));
        }
    }

    //Get User Favorite Professional
    function getFavorite(Request $request) {
        $validatorValues = array(
            'user_id' => $request->input('user_id')
        );
        $validatorRules = array(
            'user_id' => 'required'
        );
        $validator = Validator::make($validatorValues, $validatorRules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $showResult = 10;
            $offset = ($request->input('offset')) ? $request->input('offset') : 0;
            $skip = $showResult * $offset;
            $userFavorite = new UserFavorite;
            $favorites = UserFavorite::with('barber','barber.getImages')->where(['user_id' => $request->input('user_id')])
                    ->take($showResult)->skip($skip)->get();
            return Response::json(array('status' => 'success', 'successData' => $favorites));
        }
    }
    
    //Send Invite to Professional
    function sendInvite(Request $request) {
        $validatorValues = array(
            'name' => $request->input('name'),
            'user_id' => $request->input('user_id'),
            'email' => $request->input('email')
        );
        $validatorRules = array(
            'name' => 'required',
            'user_id' => 'required|numeric',
            'email' => 'required|email'
        );
        $validator = Validator::make($validatorValues, $validatorRules);

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $postData = ['name' => $request->input('name'),
                'user_id' => $request->input('user_id'),
                'email' => $request->input('email')];
//            Mail::send('emails.emailInvite', $postData, function($message) use ($postData)
//            {
//                $user = User::find($postData['user_id']);
//                $message->from($user->email, $user->first_name.' '.$user->last_name);
//                $message->subject("Taper Invitation!");
//                $message->to($postData['email']);
//            });
            return Response::json(array('status' => 'success', 'successData' => 'Professional Successfully Invited.'));
        }
    }
    function timeago($ptime) {
    $difference = time() - strtotime($ptime);
    if ($difference) {
        $periods = array("second", "minute", "hour", "day", "week", "month", "years", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
        for ($j = 0; $difference >= $lengths[$j]; $j++)
            $difference /= $lengths[$j];

        $difference = round($difference);
        if ($difference != 1)
            $periods[$j] .= "s";

        $text = "$difference $periods[$j] ago";


        return $text;
    }
}
    function deleteBooking($id){
        Booking::where('id',$id)->delete();
        return Response::json(array('status' => 'success', 'successData' => 'Booking deleted Successfully.'));
    }
}