<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\User;
use App\Gallery;
use App\Service;
use App\Booking;
use App\BussnessHour;
use Carbon\Carbon;
use App\UserStripe;
use App\Notification;
use App\History;
use App\Rating;
use Intervention\Image\Facades\Image;
use Stripe\Stripe;
use Stripe\StripeObject;

class BarberController extends Controller {

    function professionals() {
        //echo request()->ip();

        if (!isset($_GET['lat']) && !isset($_GET['lon']))
            $visitorLocation = geoip()->getLocation(request()->ip());

        $showResult = 15;
        $key = isset($_GET['term']) ? $_GET['term'] : '';
        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $skip = $showResult * $offset;
        $lat = isset($_GET['lat']) ? $_GET['lat'] : $visitorLocation->lat;
        $lng = isset($_GET['lon']) ? $_GET['lon'] : $visitorLocation->lon;

        //Professionals Count
        $allProfessionals = User::where('is_login', 1)->where('user_type', '!=', 'User');

        $professionals = User::with(array('getReviews', 'getReviews.getUser', 'getBookings', 'getRating', 'getUserFavourites'))
            ->selectRaw("*,
            ( 6371 * acos( cos( radians($lat) ) *
            cos( radians(shop_lat) ) *
            cos( radians(shop_long) - radians($lng) ) + 
            sin( radians($lat) ) *
            sin( radians(shop_lat) ) ) ) 
            AS distance")
            ->where('is_login', 1)
            ->orderBy("distance", "asc")
            ->where('user_type', '!=', 'User')
            ->take($showResult)->skip($skip);

        if ($key != '') {
            $professionals->where(function ($query) use($key) {
                $query->where('shop_name', 'like', "%$key%")
                    ->orWhere('username', 'like', "%$key%")
                    ->orWhere('shop_location', 'like', "%$key%")
                    //->orWhere('first_name', 'like', "%$key%")
                    //->orWhere('last_name', 'like', "%$key%")
                ;
            });
        }
        $professionals = $professionals->get();
        //dd($professionals);
        $data['key'] = $key;
        $data['offset'] = $offset;
        $data['lat'] = $lat;
        $data['lon'] = $lng;
        $data['limitPage'] = floor($allProfessionals->count() / $showResult);
        $data['pagination'] = Pagination($allProfessionals->count(), $showResult, $offset, 4);
        $data['title'] = 'Professionals';
        $data['barbers'] = $professionals;
        //dd($data);
        return view('frontend.professionals', $data);
    }

    function professionalDetail($id) {
        $professional = User::with(array('getTimes', 'getImages', 'getService', 'getReviews', 'getReviews.getUser', 'getBookings', 'getRating', 'getUserFavourites'))->where('id', $id)->get();
        $professional[0]->rating = ($professional[0]->getRating) ? $professional[0]->getRating : 0;
        $data['barber'] = $professional[0];
        $data['services'] = $professional[0]->getService;
        $data['images'] = $professional[0]->getImages;
        $data['reviews'] = $professional[0]->getReviews;
        $data['bussiness'] = $professional[0]->getTimes;
        $data['slots'] = $professional[0]->getBookings;
        $data['favorites'] = $professional[0]->getUserFavourites;
        $data['title'] = $professional[0]->first_name . ' ' . $professional[0]->last_name . ' - Taperup';

        // Get Visitor Location
        $visitorLocation = geoip()->getLocation(request()->ip());
        $data['lat'] = $visitorLocation->lat;
        $data['lon'] = $visitorLocation->lon;

        return view('frontend.professionalDetail', $data);
    }

    function addCover() {
        $file = Input::file('photo');
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'photo' => $file,
                'type' => $_POST['type']
            ), array(
                'user_id' => 'required',
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                'type' => 'required',
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $user = User::find($_POST['user_id']);
            $image_destination_path = 'public/images'; // upload path
            $image_extension = Input::file('photo')->getClientOriginalExtension(); // getting image extension
            $image_fileName = 'profile_' . Str::random(15) . '.' . $image_extension; // renameing image
            Input::file('photo')->move($image_destination_path, $image_fileName);
            if ($_POST['type'] == 'cover') {
                $user->cover = $image_fileName;
            }if ($_POST['type'] == 'profile_pic') {
                $user->profile_pic = $image_fileName;
            }
            $user->save();
            return Response::json(array('status' => 'success', 'successData' => $user, 'successMessage' => 'Picture Changed'));
        }
    }

    protected function addCategory()
    {
        $validator = Validator::make(
            array(
                'category' => $_POST['category'],
                'barber_id' => $_POST['barber_id']
            ), array(
                'barber_id' => 'required',
                'category' => 'required',

            )
        );

        if ($validator->fails()) {

            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $barber_id = $_POST['barber_id'];

            if(Category::where('barber_id',$barber_id)->where('category', $_POST['category'])->count()){
                return Response::json(array('status' => 'error', 'errorMessage' => 'Category already exist'));
            }
            $cat = new Category();
            $cat->barber_id= $barber_id;
            $cat->category = $_POST['category'];
            $cat->description = isset($_POST['description']) ? $_POST['description'] : '';
            $cat->save();
            $category_list = Category::where('barber_id',$barber_id)->get();
            return Response::json(array('status' => 'success', 'successData' => $category_list, 'successMessage' => 'Data Updated'));
        }
    }

    function updateDetails() {
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'address' => $_POST['address'],
                'shop_name' => $_POST['shop_name'],
                'lat' => $_POST['lat'],
                'lng' => $_POST['long']
            ), array(
                'user_id' => 'required',
                'address' => 'required',
                'shop_name' => 'required',
                'lat' => 'required',
                'lng' => 'required'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $user = User::find($_POST['user_id']);
            $user->user_type = (isset($_POST['user_type'])) ? $_POST['user_type'] : $user->user_type;
            $user->shop_location = $_POST['address'];
            $user->shop_name = $_POST['shop_name'];
            $user->shop_long = $_POST['long'];
            $user->shop_lat = $_POST['lat'];

            $user->cancel_fee = $_POST['cancel_fee'];
            if (isset($_POST['announcment'])) {
                if ($user->announcment != $_POST['announcment']) {
                    $user->announcment = $_POST['announcment'];
                    $user->announcment_date = Carbon::now();
                    $this->sendNotification($user);
                }
            }
            if (isset($_POST['user_type'])) {
                $user->user_type = $_POST['user_type'];
            }
            //mobeen change
            $user->is_subscribed = (isset($_POST['subscription_type'])) ? $_POST['subscription_type']  : $user->is_subscribed;
//            if($_POST['subscription_type']){
//                $user->is_cash = $_POST['subscription_type'];
//            }
            //$user->is_cash = (isset($_POST['subscription_type'])) ? $_POST['subscription_type'] : $user->is_cash;


            //Update City and State
            if($_POST['long'] == 0.0 || $_POST['lat'] == 0.0){
                $address = getAddressByLatLon($user->lat, $user->lng);
                if ($address) {
                    $user->shop_city = $address['city'];
                    $user->shop_state = $address['state'];
                }
            }
            else {
                $address = getAddressByLatLon($_POST['lat'], $_POST['long']);
                if ($address) {
                    $user->shop_city = $address['city'];
                    $user->shop_state = $address['state'];
                }
            }


            if (isset($_POST['username'])) {
                $checkusername = User::where('username', $_POST['username'])->first();
                if ($checkusername) {
                    return Response::json(array('status' => 'error', 'errorMessage' => "username already taken"));
                }
                $user->username = $_POST['username'];
            }
            $user->save();
            if ($_POST['services']) {
                $services = json_decode($_POST['services']);
                foreach ($services as $service) {
                    if ($service->id) {
                        $update_serice = Service::find($service->id);
                    } else {
                        $update_serice = new Service;
                        $update_serice->barber_id = $_POST['user_id'];
                    }
                    $update_serice->service_type = $service->service_type;
                    $update_serice->price = $service->price;
                    $update_serice->time = $service->time;
                    $update_serice->category_id = $service->category_id;
                    $update_serice->description = $service->description;
                    $update_serice->save();
                }
            }
            $user->services = Service::where('barber_id', $_POST['user_id'])->get();
            $user->categories = Category::where('barber_id', $_POST['user_id'])->get();
            $user->bussiness_hours = BussnessHour::where('barber_id', $user->id)->first();
            $user->gallery_images = Gallery::where('barber_id', $user->id)->orderBy('created_at', 'desc')->get();
            $user->bookings = Booking::where('date', '>=', date('Y-m-d'))->where('barber_id', $user->id)->with('User')->orderBy('date', 'desc')->get();
            if ($user->user_type != 'User') {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $user_stripe = UserStripe::where('barber_id', $user->id)->first();
                $account = \Stripe\Account::retrieve($user_stripe->stripe_payout_account_id);
                $user->account_status = $account->legal_entity->verification->status;
                $user_stripe->stripe_payout_account_info = json_encode($account);
                $user_stripe->save();
                if (isset($account->external_accounts->data[0])) {
                    $user->account_last_four = $account->external_accounts->data[0]->last4;
                    $user->account_card_brand = $account->external_accounts->data[0]->brand;
                    $user->account = json_encode($account);
                } else {
                    $user->account_last_four = '';
                    $user->account_card_brand = '';
                }
            }
            return Response::json(array('status' => 'success', 'successData' => $user, 'successMessage' => 'Data Updated'));
        }
    }

    function addGallery() {
        $file = Input::file('photo');
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'photo' => $file
            ), array(
                'user_id' => 'required',
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $image_destination_path = 'public/images'; // upload path
            $image_extension = Input::file('photo')->getClientOriginalExtension(); // getting image extension
            $image_fileName = 'gallery_' . Str::random(15) . '.' . $image_extension; // renameing image
            Input::file('photo')->move($image_destination_path, $image_fileName);
            $add_image = new Gallery;
            $add_image->barber_id = $_POST['user_id'];
            $add_image->image = $image_fileName;
            $add_image->save();
            return Response::json(array('status' => 'success', 'successData' => $add_image, 'successMessage' => 'Image Added'));
        }
    }

    function deleteImage() {
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'image_id' => $_POST['image_id']
            ), array(
                'user_id' => 'required',
                'image_id' => 'required'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $gallery_image = Gallery::find($_POST['image_id']);
            if ($gallery_image) {
                $gallery_image->delete();
            }
            return Response::json(array('status' => 'success', 'successMessage' => 'Image Deleted'));
        }
    }

    function deleteService() {
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'service_id' => $_POST['service_id']
            ), array(
                'user_id' => 'required',
                'service_id' => 'required'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $service = Service::find($_POST['service_id']);
            if ($service) {
                $service->delete();
            }
            return Response::json(array('status' => 'success', 'successMessage' => 'Service Deleted'));
        }
    }

    function getBooking($id) {
        $bookings = Booking::where('barber_id', $id)->with('User')->orderBy('date', 'desc')->get();
        return Response::json(array('status' => 'success', 'successData' => $bookings));
    }

    function updatePhone() {
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'phone' => $_POST['phone']
            ), array(
                'user_id' => 'required',
                'phone' => 'required'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $user = User::find($_POST['user_id']);
            $user->mobile = $_POST['phone'];
            $user->save();
            return Response::json(array('status' => 'success', 'successData' => $user, 'successMessage' => 'Data Updated'));
        }
    }

    function updateHours() {
        $check_biz_hours = BussnessHour::where('barber_id', $_POST['user_id'])->first();
        if (!$check_biz_hours) {
            $check_biz_hours = new BussnessHour;
            $check_biz_hours->barber_id = $_POST['user_id'];
        }
        $check_biz_hours->mon = $_POST['mon'];
        $check_biz_hours->tue = $_POST['tue'];
        $check_biz_hours->wed = $_POST['wed'];
        $check_biz_hours->thus = $_POST['thus'];
        $check_biz_hours->fri = $_POST['fri'];
        $check_biz_hours->sat = $_POST['sat'];
        $check_biz_hours->sun = $_POST['sun'];
        $check_biz_hours->save();
        return Response::json(array('status' => 'success', 'successData' => $check_biz_hours, 'successMessage' => 'Data Updated'));
    }

    function manulBooking() {
        $check_booking_overlap = Booking::where('status', '!=', 'Rejected')->where(array('date' => $_POST['date'], 'barber_id' => $_POST['user_id']))->get();
        $overlaps = 0;
        $date = $_POST['date'];
        $startTime = $_POST['start_time']; // note the extra '0' in front
        $endTime = $_POST['end_time'];

        $userStartTime = "$date $startTime";
        $ust = Carbon::parse($userStartTime)->format('Y-m-d H:i');
        $userEndTime = "$date $endTime";
        $uet = Carbon::parse($userEndTime)->format('Y-m-d H:i');
        foreach ($check_booking_overlap as $boookings) {
            $back_endtime = "$date $boookings->end_time";
            $bet = Carbon::parse($back_endtime)->format('Y-m-d H:i');
            $back_starttime = "$date $boookings->start_time";
            $bst = Carbon::parse($back_starttime)->format('Y-m-d H:i');
            if ($bet > $ust && $uet > $bst) {
                $overlaps ++;
            }
        }
        if ($overlaps > 0) {
            return Response::json(array('status' => 'error', 'errorMessage' => 'This Timing OverLaps The Secdule'));
        }
        $add_manual_booking = new Booking;
        $add_manual_booking->barber_id = $_POST['user_id'];
        $add_manual_booking->start_time = $_POST['start_time'];
        $add_manual_booking->end_time = $_POST['end_time'];
        $add_manual_booking->date = $_POST['date'];
        $add_manual_booking->service_at = $_POST['service_at'];
        $add_manual_booking->status = 'Manual';
        $add_manual_booking->price = $_POST['price'];
        $add_manual_booking->payment = 'Cash';
        $add_manual_booking->save();
        return Response::json(array('status' => 'success', 'successData' => $add_manual_booking, 'successMessage' => 'Data Updated'));
    }

    function updateApproval() {
        $booking = Booking::where(array('id' => $_POST['appiontment_id']))->where(array('status' => 'Pending'))->orWhere(array('status' => 'Custom'))->first();
        $originalDate = $booking->date;
        $newDate = date("l", strtotime($originalDate));
        if ($booking) {
            $user = User::find($_POST['user_id']);
            $barber = User::find($booking->barber_id);
            if ($_POST['status'] == 'Rejected') {
                $booking->status = $_POST['status'];
                $booking->save();
                if ($user->is_login == 1) {
                    $app_time = $booking->start_time;
                    $time = date("g:i a", strtotime("$app_time"));
                    $barber_data = json_encode(User::find($booking->barber_id));

                    //Send notifications
                    $notificatinText = $barber->first_name . ' ' . $barber->last_name . ' rejected your appointment for ' . $newDate . ' at ' . $time;
                    $notificatinSubject = 'Request Rejected';
//                    $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                    $emailData['data']['mailOtherData'] = $this->mapData($user, $barber);
//                    emailNotification($emailData);

                    if ($user->platform == 'android') {
                        $this->android($user->device_id, $user->id, $notificatinText, $barber_data, $booking->barber_id);
                    }
                    if ($user->platform == 'ios') {
                        $this->ios($user->device_id, $user->id, $notificatinText, $barber_data, $booking->barber_id);
                    }
                }
                return Response::json(array('status' => 'success', 'successMessage' => 'Booking ' . $_POST['status'] . ' Successfully'));
            }
            if ($_POST['status'] == 'Approved') {
                $booking->status = $_POST['status'];
                $booking->save();
                if ($user->is_login == 1) {
                    $app_time = $booking->start_time;
                    $time = date("g:i a", strtotime("$app_time"));
                    $barber_data = json_encode(User::find($booking->barber_id));
                    //Send notifications
                    $notificatinText = $barber->first_name . ' ' . $barber->last_name . ' approved your appointment for ' . $newDate . ' at ' . $time;
                    $notificatinSubject = 'Request Approved';
//                    $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                    $emailData['data']['mailOtherData'] = $this->mapData($user, $barber);
//                    emailNotification($emailData);

                    if ($user->platform == 'android') {
                        $this->android($user->device_id, $user->id, $notificatinText, $barber_data, $booking->barber_id);
                    }
                    if ($user->platform == 'ios') {
                        $this->ios($user->device_id, $user->id, $notificatinText, $barber_data, $booking->barber_id);
                    }
                }
                return Response::json(array('status' => 'success', 'successMessage' => 'Booking ' . $_POST['status'] . ' Successfully'));
            }
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'This appointment has been expired'));
        }
    }

    function getStatus() {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = UserStripe::where('barber_id', $_POST['user_id'])->first();
        $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
        $data['account_status'] = $account->legal_entity->verification->status;
        $data['account'] = json_encode($account);
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Status', 'successData' => $data));
    }

    function saveLegalDetails() {






        $txt = json_encode($_POST) . ' User id ' . $_POST['user_id'] . ' Date ' . date('Y-m-d H:i:s');
        file_put_contents('stripefile.txt', $txt . PHP_EOL, FILE_APPEND | LOCK_EX);
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = UserStripe::where('barber_id', $_POST['user_id'])->first();
        $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
        $user_details = User::find($_POST['user_id']);
        $user_details->is_subscribed = (isset($_POST['subscription_type'])) ? $_POST['subscription_type']  : $user_details->is_subscribed;



        if (isset($_POST['stripe_token'])) {
            $account->external_accounts->create(array("external_account" => $_POST['stripe_token']));
            $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
            $data['last4'] = $account->external_accounts->data[0]->last4;
            $data['brand'] = $account->external_accounts->data[0]->brand;
            $data['account_status'] = $account->legal_entity->verification->status;
            $data['account'] = json_encode($account);
            $user->stripe_payout_account_info = json_encode($account);
            $user->save();

            //mobeen code
            //$user_details->card_last_four = $account->external_accounts->data[0]->last4;

//mobeen code
            if(isset($_POST['create_customer_token']) and $user_details->is_subscribed == 0){
                $stripe_customer = \Stripe\Customer::create(array(
                    "description" => "$user_details->email",
                    "source" => $_POST['create_customer_token'] // obtained with Stripe.js
                ));
                $user_details->card_brand = $account->external_accounts->data[0]->brand;
                $user_details->card_last_four = $account->external_accounts->data[0]->last4;
                $user_details->stripe_id = $stripe_customer->id;
            }

            $user_details->save();

//            //Make Barber Subscriptions
//            if (isset($_POST['stripe_token_subscription'])) {
//                if ($user_details->is_cash == 1) {
//                    $user_details->newSubscription('Cash Subscription', 'cashsubscription')->create($_POST['stripe_token_subscription']);
//                } elseif ($user_details->is_cash == 2) {
//                    $user_details->newSubscription('Promo Subscription', 'promosubscription')->create($_POST['stripe_token_subscription']);
//                }
//            }
            //mobeen code
            if (isset($_POST['stripe_token_subscription'])) {
                if ($user_details->is_subscribed == 1) {
                    $user_details->newSubscription('Cash Subscription', 'cashsubscription')->create($_POST['stripe_token_subscription']);
                } elseif ($user_details->is_subscribed == 2) {
                    $user_details->newSubscription('Promo Subscription', 'promosubscription')->create($_POST['stripe_token_subscription']);
                }
            }
//            on update Subscriptions
            if (isset($_POST['stripe_token_update'])) {
                if ($_POST['is_subscribed'] == 1) {
                    User::where('id', $user_details->id)->update(['is_subscribed' => $_POST['is_subscribed']]);
                    $user_details->newSubscription('Cash Subscription', 'cashsubscription')->create($_POST['stripe_token_update']);
                } elseif ($_POST['is_subscribed'] == 2) {
                    User::where('id', $user_details->id)->update(['is_subscribed' => $_POST['is_subscribed']]);
                    $user_details->newSubscription('Promo Subscription', 'promosubscription')->create($_POST['stripe_token_update']);
                }
            }

            return Response::json(array('status' => 'success', 'successMessage' => 'Extrnal Account Updated', 'successData' => $data));
        }
        $arr = explode('-', $_POST['dob']);
        $account->tos_acceptance->date = time();
        if (count($arr) == 3) {
            list($y, $m, $d) = $arr;
            $account->legal_entity->dob->year = $y;
            $account->legal_entity->dob->month = $m;
            $account->legal_entity->dob->day = $d;
        }
        $account->tos_acceptance->ip = $_POST['ip'];
//        $account->tos_acceptance->ip_address= $_POST['ip'];
        if (isset($_POST['first_name'])) {
            $account->legal_entity->first_name = $_POST['first_name'];
        } else {
            $account->legal_entity->first_name = $user_details->first_name;
        }
        if (isset($_POST['last_name'])) {
            $account->legal_entity->last_name = $_POST['last_name'];
        } else {
            $account->legal_entity->last_name = $user_details->last_name;
        }
        $account->legal_entity->type = 'individual';
        $account->legal_entity->address->state = $_POST['state'];
        $account->legal_entity->address->city = $_POST['city'];
        $account->legal_entity->address->line1 = $_POST['line1'];
        $account->legal_entity->address->postal_code = $_POST['zipcode'];
        $account->legal_entity->ssn_last_4 = $_POST['ssn_last_4'];
//        $account->transfer_schedule->delay_days = 2;
//        $account->transfer_schedule->interval = "daily";
        $account->legal_entity->personal_id_number = $_POST['person_id'];
        $file = Input::file('legal_personal_id_image');
        if ($file->getClientOriginalExtension() != 'exe') {
            $type = $file->getClientMimeType();
            if ($type == 'image/jpg' || $type == 'image/jpeg' || $type == 'image/png' || $type == 'image/bmp') {
                $ver_destinationPath = 'public/images/verification'; // upload path
                $ver_extension = $file->getClientOriginalExtension(); // getting image extension
                $ver_fileName = 'profile_' . Str::random(15) . '.' . $ver_extension; // renameing image
                $img = Image::make($file->getRealPath());
                $size = $img->filesize();
                if ($size > 2000) {
                    $height = $img->height();
                    $get_width = $img->width() / 720;
                    $new_height = $height / $get_width;
                    $img->resize(720, $new_height)->save($ver_destinationPath . '/' . $ver_fileName);
                } else {
                    Input::file('legal_personal_id_image')->move($ver_destinationPath, $ver_fileName);
                }
            }
//                    echo $_SERVER['DOCUMENT_ROOT'];exit;

            $filename = $_SERVER['DOCUMENT_ROOT'] . '/book/public/images/verification/' . $ver_fileName;
//
            $chmod = 0644;
            chmod($filename, $chmod);
            $fp = fopen($filename, 'r');

            $stripe_file = \Stripe\FileUpload::create(array(
                'purpose' => 'identity_document',
                'file' => $fp
            ));
            $account->legal_entity->verification->document = $stripe_file->id;
        }
        try {
            $account->save();
        } catch (\Stripe\Error\Card $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\RateLimit $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\InvalidRequest $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\Authentication $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\ApiConnection $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (\Stripe\Error\Base $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        } catch (Exception $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        }
        $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
        $user->image = $filename;
        $user->stripe_payout_account_info = json_encode($account);
        $user->save();
        $data['account_status'] = $account->legal_entity->verification->status;
        $data['account'] = json_encode($account);
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $data));
    }

    function updateAcceptance() {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = UserStripe::where('barber_id', $_POST['user_id'])->first();
        $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
        $account->tos_acceptance->ip = $_POST['ip'];
        $account->tos_acceptance->date = time();
        $account->save();
        $data['account'] = json_encode($account);
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $data));
    }

    function android($device_id, $user_id, $message, $data = '', $barber_id, $booking_id = '', $is_save = 1) {
        set_time_limit(0);
        if ($is_save == 1) {
            $savenotification = new Notification;
            $savenotification->to_user = $user_id;
            $savenotification->from_user = $barber_id;
            $savenotification->text = $message;
            $savenotification->date = date('Y-m-d');
            $savenotification->is_read = 0;
            $savenotification->save();
        }
        $gcmKey = "AIzaSyCenDLL9Pb1nayFHzcdUEg5bXB3mkU85vE";
        $url = 'https://android.googleapis.com/gcm/send';
        $registatoin_ids = array($device_id);
        $data = array('message' => $message, 'payload' => array('barber_data' => $data, 'text' => $message, 'user_id' => $user_id, 'timeago' => 'Just Now', 'booking_id' => $booking_id, 'badge' => 1));

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $data
        );
        $headers = array(
            'Authorization: key=' . $gcmKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return TRUE;
    }

    function ios($deviceToken, $user_id, $message, $data = '', $barber_id, $booking_id = '', $is_save = 1) {
        set_time_limit(0);
        if ($is_save == 1) {
            $savenotification = new Notification;
            $savenotification->to_user = $user_id;
            $savenotification->from_user = $barber_id;
            $savenotification->text = $message;
            $savenotification->date = date('Y-m-d');
            $savenotification->is_read = 0;
            $savenotification->save();
        }

        $data = array('barber_data' => $data, 'text' => $message, 'user_id' => $user_id, 'booking_id' => $booking_id, 'timeago' => 'Just Now');
        // Put your private key's passphrase here:
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client(
//                'ssl://gatew  ay.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return TRUE;
    }

    function completeJob() {
        $cash_type = $_POST['cash_type'];
        $booking = Booking::find($_POST['appiontment_id']);
        $barber = User::where('id', $booking->barber_id)->with('getRating')->first();
        if ($booking->user_id) {
            $user = User::find($booking->user_id);
        } else {
            $user = '';
        }
        if ($cash_type == 'Cash') {
            $booking->status = 'Completed';
            $booking->payment = 'Cash';
            $booking->save();
            $add_history = new History;
            $add_history->barber_id = $booking->barber_id;
            $add_history->total_payments = $booking->price;
            $add_history->appiontment_id = $booking->id;
            $add_history->payment_type = 'Cash';
            $add_history->earning = $booking->price - ($booking->price * 20 / 100);
            $add_history->app_blance = '-' . $booking->price * 20 / 100;
            $barber->cash_count = $barber->cash_count + 1;
            $barber->save();
            if ($user) {
                if ($user->is_login == 1) {
                    //Send notifications
                    $notificatinText = ' completed his appointment';
                    $notificatinSubject = 'Job Completed';
//                    $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                    $emailData['data']['mailOtherData'] = $this->mapData($user, $barber);
//                    emailNotification($emailData);

                    if ($user->platform == 'android') {
                        //$this->android($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id);
                    }
                    if ($user->platform == 'ios') {
                        //$this->ios($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id);
                    }
                }
            }
            return Response::json(array('status' => 'success', 'successMessage' => 'Booking Completed Successfully'));
        }
        if ($cash_type == 'Stripe') {
            $add_history = new History;
            $add_history->barber_id = $booking->barber_id;
            $add_history->total_payments = $booking->price;
            $add_history->appiontment_id = $booking->id;
            $add_history->payment_type = 'Stripe';
            $get_history_count = History::where('barber_id', $booking->barber_id)->sum('app_blance');
//            if ($get_history_count < 0) {
//                $app_fee = floor($booking->price * 7 / 100);
//
//                $remaing = $booking->price - $app_fee;
//                $get_total = $remaing + $get_history_count;
//                if ($get_total > 0) {
//                    $booking_price = intval($booking->price * 100);
//                    $app_fee = intval(($remaing - $get_total) * 100);
//                    $add_history->earning = $remaing;
//                    $add_history->app_blance = ($remaing - $get_total);
//                } else {
//                    $app_fee = intval($remaing * 100);
//                    $add_history->earning = $remaing;
//                    $add_history->app_blance = $remaing;
//                }
//            }
//            else {
            $booking_price = intval($booking->price * 100);
            $app_fee = ($booking_price > 0) ? round(floatval(($booking->price * 7 / 100) + 0.30), 2) : 0.30;
            $add_history->earning = ($booking_price > 0) ? ($booking->price - $app_fee) : 0;
            $add_history->app_blance = 0;
//            }
            $add_history->save();
//             return Response::json(array('status' => 'success', 'successMessage' => $booking->price + $app_fee));exit;
//            echo $booking->price + $app_fee;exit;
            $barber_stripe = UserStripe::where('barber_id', $booking->barber_id)->first();
            if ($booking_price > 0) {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                try {
                    \Stripe\Charge::create(array(
                        "amount" => intval(($booking->price + $app_fee) * 100),
                        "currency" => "usd",
                        "customer" => "$user->stripe_id", // obtained with Stripe.js
                        "description" => "Charge for professional services",
                        "destination" => "$barber_stripe->stripe_payout_account_id",
                        "application_fee" => $app_fee * 100
                    ));
                } catch (\Stripe\Error\Card $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (\Stripe\Error\RateLimit $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (\Stripe\Error\InvalidRequest $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (\Stripe\Error\Authentication $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (\Stripe\Error\ApiConnection $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (\Stripe\Error\Base $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                } catch (Exception $e) {
                    return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
                }
            }
            $booking->status = 'Completed';
            $booking->save();
            $originalDate = $booking->date;
            $newDate = date("l", strtotime($originalDate));
            $app_time = $booking->start_time;
            $time = date("g:i a", strtotime("$app_time"));
            if ($user->is_login == 1) {
                //Send notifications
                $notificatinText = $barber->first_name . ' ' . $barber->last_name . ' completed your appointment for ' . $newDate . ' at ' . $time;
                $notificatinSubject = 'Job Completed';
//                $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, $booking->id);
//                emailNotification($emailData);

                if ($user->platform == 'android') {
                    //$this->android($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id, $booking->id);
                }
                if ($user->platform == 'ios') {
                    //$this->ios($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id, $booking->id);
                }
            }
            if ($barber->cash_count > 0) {
                $barber->cash_count = $barber->cash_count - 1;
                $barber->save();
            }
            return Response::json(array('status' => 'success', 'successMessage' => 'Booking Completed Successfully'));
        }
    }

    function charge() {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $cus = User::find($_POST['barber_id']);
        if (isset($_POST['stripeToken'])) {
            $stripeToken = $_POST['stripeToken'];
            if ($cus->stripe_id) {
                $cus->updateCard($stripeToken);
            } else {
                $cus->newSubscription('taper', 'taper')
                    ->create($stripeToken);
            }
        }
        $get_cash = History::where('barber_id', $_POST['barber_id'])->sum('app_blance');
        $amount = intval(- ($get_cash * 100));
        try {
            $cus->charge($amount);
            $cus->cash_count = 0;
            $cus->save();
            History::where('barber_id', $_POST['barber_id'])->update(array('app_blance' => 0));
        } catch (Exception $e) {
            return Response::json(array('status' => 'error', 'errorMessage' => $e->getMessage()));
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Thanks :) You have been charged $' . $amount / 100, 'card_last_four' => $cus->card_last_four, 'card_brand' => $cus->card_brand));
    }

    function cancelAppiontment() {
        if ($_POST['appointment_id']) {
            $booking = Booking::find($_POST['appointment_id']);
            if ($booking) {
                $user = User::find($booking->user_id);
                $barber = User::find($booking->barber_id);
                $originalDate = $booking->date;
                $newDate = date("l", strtotime($originalDate));
//                $barber->first_name.' '.$barber->last_name.  ' completed your appointment for '.$newDate.' at '.$booking->start_time
                if ($user->is_login == 1) {
                    $booking->status = 'Rejected';
                    $booking->save();
                    $app_time = $booking->start_time;
                    $time = date("g:i a", strtotime("$app_time"));
                    //Send notifications
                    $notificatinText = $barber->first_name . ' ' . $barber->last_name . ' has cancelled appointment for ' . $newDate . ' at ' . $time;
                    $notificatinSubject = 'Appointment Cancelled';
//                    $emailData['data']['mailData'] = ['email' => $user->email, 'subject' => $notificatinSubject, 'message' => $notificatinText];
//                    $emailData['data']['mailOtherData'] = $this->mapData($user, $barber);
//                    emailNotification($emailData);

                    if ($user->platform == 'android') {
                        $this->android($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id);
                    }
                    if ($user->platform == 'ios') {
                        $this->ios($user->device_id, $user->id, $notificatinText, json_encode($barber), $booking->barber_id);
                    }
                    return Response::json(array('status' => 'success', 'successMessage' => 'Booking Rejected Successfully'));
                }
                return Response::json(array('status' => 'success', 'successMessage' => 'User Was Logged Out'));
            } else {
                return Response::json(array('status' => 'error', 'errorMessage' => 'Invalid appiontment id.'));
            }
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'Appiontment id is required'));
        }
    }

    //mobeen code
    function updatePaymentType() {
        $validator = Validator::make(
            array(
                'user_id' => $_POST['user_id'],
                'payment_type' => $_POST['payment_type']
            ), array(
                'user_id' => 'required',
                'payment_type' => 'required'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $update_user = '';
            $user = User::find($_POST['user_id']);
            $payment_type = $_POST['payment_type'];
            $is_subscription = $_POST['subscription_type'];

//            $is_subscription = 1;
//            if($_POST['payment_type'] == 0){
//                $is_subscription = 0;
//            }
            if($user->is_subscribed == $is_subscription){
                $update_user = User::where('id', $user->id)->update(['is_cash' => $payment_type]);
            }
            else {
                if($user->is_subscribed == 0 and $is_subscription == 1){
                    $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
                    $update_user = User::where('id', $user->id)->update(['is_cash' => $payment_type , 'is_subscribed' => $is_subscription]);
                }
                else if($user->is_subscribed == 1 and $is_subscription == 0) {
                    if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
                        $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
                    }
                    $update_user = User::where('id', $user->id)->update(['cancel_fee' => 0 ,'is_cash' => 0, 'is_subscribed' => $is_subscription]);
                }
            }




//             elseif (($user->is_cash == 3 and $payment_type == 1) || ($user->is_cash == 1 and $payment_type == 3)) {
//                $update_user = User::where('id', $user->id)->update(['is_cash' => $payment_type]);

//                //promo to paid
//            } else if ($user->is_cash == 2){
//                if($payment_type == 0){
//                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//                }if($payment_type == 1){
//                    $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
//                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//                }if($payment_type == 3){
//                    $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
//                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//                }
//            }
//            //paid to promo
//            else if($user->is_cash == 0 && $payment_type == 2){
//                User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//            }else if($user->is_cash == 1 && $payment_type == 2){
//                if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
//                    $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
//                }
//            }else if($user->is_cash == 3 && $payment_type == 2){
//                if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
//                    $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
//                }
//                User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//            }
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $update_user));
    }
//    function updatePaymentType() {
//        $validator = Validator::make(
//            array(
//                'user_id' => $_POST['user_id'],
//                'payment_type' => $_POST['payment_type']
//            ), array(
//                'user_id' => 'required',
//                'payment_type' => 'required'
//            )
//        );
//        if ($validator->fails()) {
//            $messages = $validator->messages()->all();
//            $messages = join(',', $messages);
//            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
//        } else {
//            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
//            $update_user = '';
//            $user = User::find($_POST['user_id']);
//            $payment_type = $_POST['payment_type'];
//
////            $is_subscription = 1;
////            if($_POST['payment_type'] == 0){
////                $is_subscription = 0;
////            }
//
//            if ($user->is_cash == 0 and ( $payment_type == 1 || $payment_type == 3)) {
//                $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
//                $update_user = User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//            } elseif (($user->is_cash == 1 or $user->is_cash == 3) and $payment_type == 0) {
//                if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
//                    $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
//                }
//                $update_user = User::where('id', $user->id)->update(['is_cash' => 0]);
//            } elseif (($user->is_cash == 3 and $payment_type == 1) || ($user->is_cash == 1 and $payment_type == 3)) {
//                $update_user = User::where('id', $user->id)->update(['is_cash' => $payment_type]);
//
////                //promo to paid
////            } else if ($user->is_cash == 2){
////                if($payment_type == 0){
////                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
////                }if($payment_type == 1){
////                    $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
////                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
////                }if($payment_type == 3){
////                    $user->newSubscription('Cash Subscription', 'cashsubscription')->create();
////                    User::where('id', $user->id)->update(['is_cash' => $payment_type]);
////                }
////            }
////            //paid to promo
////            else if($user->is_cash == 0 && $payment_type == 2){
////                User::where('id', $user->id)->update(['is_cash' => $payment_type]);
////            }else if($user->is_cash == 1 && $payment_type == 2){
////                if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
////                    $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
////                }
////            }else if($user->is_cash == 3 && $payment_type == 2){
////                if ($user->subscribed('Cash Subscription', 'cashsubscription')) {
////                    $user->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
////                }
////                User::where('id', $user->id)->update(['is_cash' => $payment_type]);
////            }
//            }
//        }
//        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $update_user));
//    }


    function mapData($user = '', $barber = '', $bookinId = '') {
        return $notificationEmailData = ['user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'bookinId' => checkSetValue($bookinId),
            'barberData' => json_encode($barber)];
    }

    public function cronForNotification() {
//        $bookings = Booking::where('status', 'pending')->where('id',474)->get();
//        $cur_date = Carbon::now('PST');
//        $oneDayBefore = $cur_date->addHours(24);
//        foreach ($bookings as $booking) {
//            $booking_time = Carbon::parse($booking->date . ' ' . $booking->start_time);
//            $dayLenght = $booking_time->diffInMinutes($oneDayBefore);
//            if ($dayLenght <= 1440) {
//                if ($booking->send_status == 0) {
//                    //Booking::where('id', $booking->id)->update(['send_status' => 1]);
//                    //dd($booking->User);
//                    if ($booking->User->platform == 'android') {
//                        $this->android($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                    }
//                    if ($booking->User->platform == 'ios') {
//                        $this->ios($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                    }
//                } elseif ($booking->send_status == 1) {
//                    $twoHoursBefore = $cur_date->subHours(22);
//                    $hourLenght = $booking_time->diffInMinutes($twoHoursBefore);
//                    if ($hourLenght <= 120) {
//                        Booking::where('id', $booking->id)->update(['send_status' => 2]);
//                        if ($booking->User->platform == 'android') {
//                            $this->android($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                        }
//                        if ($booking->User->platform == 'ios') {
//                            $this->ios($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                        }
//                    }
//                }
//            }
//        }




//        $bookings = Booking::where('status', 'pending')->get();
//        foreach($bookings as $booking){
//            $barber = $booking->Barber;
//            $barber_time_zone = $barber->time_zone/60;
//            if($barber_time_zone < 0){
//                $cur_time = Carbon::now()->addHours($barber_time_zone);
//                $cur_time_string = $cur_time->toDateString();
//                $tomorrow_time = Carbon::tomorrow()->addHours($barber_time_zone);
//                $tomorrow_time_string = $tomorrow_time->toDateString();
//            } else if($barber_time_zone > 0) {
//                $cur_time = Carbon::now()->subHours($barber_time_zone);
//                $cur_time_string = $cur_time->toDateString();
//                $tomorrow_time = Carbon::tomorrow()->subHours($barber_time_zone);
//                $tomorrow_time_string = $tomorrow_time->toDateString();
//            }
//            if($cur_time->hour >= 6) {
//                $barber_booking_counts = Booking::where('barber_id',$booking->barber_id)->where('date' >= $cur_time_string)->where('date' <= $tomorrow_time_string)->get();
//                if ($barber_booking_counts->count() > 0) {
//                    if ($booking->User->platform == 'android') {
//                        $this->android($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                    }
//                    if ($booking->User->platform == 'ios') {
//                        $this->ios($booking->User->device_id, $booking->User->id, 'You have appointment tomorrow', json_encode($booking), $booking->barber_id, '', 0);
//                    }
//                }
//            }
//
//            //for customer
//            $user = $booking->User;
//            $user_time_zone = $user->time_zone/60;
//            if($user_time_zone < 0){
//                $cur_time = Carbon::now()->addHours($user_time_zone);
//                $cur_time_string = $cur_time->toDateString();
//                $tomorrow_time = Carbon::tomorrow()->addHours($user_time_zone);
//                $tomorrow_time_string = $tomorrow_time->toDateString();
//            } else if($user_time_zone > 0) {
//                $cur_time = Carbon::now()->subHours($user_time_zone);
//                $cur_time_string = $cur_time->toDateString();
//                $tomorrow_time = Carbon::tomorrow()->subHours($user_time_zone);
//                $tomorrow_time_string = $tomorrow_time->toDateString();
//            }
//            if($cur_time->hour >= 6) {
//                $user_appointments = Booking::where('user_id', $booking->user_id)->where('date' >= $cur_time_string)->where('date' <= $tomorrow_time_string)->get();
//                if ($user_appointments) {
//                    foreach ($user_appointments as $user_appointment) {
//                        if ($user_appointment->User->platform == 'android') {
//                            $this->android($user_appointment->User->device_id, $user_appointment->User->id, 'You have appointment tomorrow', json_encode($user_appointment), $user_appointment->barber_id, '', 0);
//                        }
//                        if ($user_appointment->User->platform == 'ios') {
//                            $this->ios($user_appointment->User->device_id, $user_appointment->User->id, 'You have appointment tomorrow', json_encode($user_appointment), $user_appointment->barber_id, '', 0);
//                        }
//                    }
//                }
//            }
//        }
        set_time_limit(0);
        $users_bookings = User::with('getAppointments')->where('user_type','User')->get();
        foreach($users_bookings as $users_booking) {
            if (is_numeric($users_booking->time_zone)) {
                $booking = $users_booking->getAppointments;
                $user = $users_booking;
                $user_time_zone = $user->time_zone / 60;
                if ($user_time_zone < 0) {
                    $user_time_zone = -1 * $user_time_zone;
                    $cur_time = Carbon::now()->addHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->addHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                } else if ($user_time_zone > 0) {
                    $cur_time = Carbon::now()->subHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->subHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                }
                if ($cur_time->hour == 6) {
                    $user_appointments = Booking::where('user_id', $users_booking->id)->where('date', '>=', $cur_time_string)->where('date', '<', $tomorrow_time_string)->get();
                    if ($users_booking->send_status == 0) {
                        if ($user_appointments) {
                            User::where('id', $users_booking->id)->update(['send_status' => 1]);
                            foreach ($user_appointments as $user_appointment) {
                                if ($user_appointment->send_status == 0) {
                                    Booking::where('id', $user_appointment->id)->update(['send_status' => 1]);
                                    $barber = User::where('id', $user_appointment->barber_id)->first();
                                    if ($users_booking->platform == 'android') {
                                        $this->android($users_booking->device_id, $users_booking->id, 'Reminder - You have an appointment today with ' . $barber->first_name . ' ' . $barber->last_name . ' at ' . $user_appointment->start_time, json_encode($user_appointment), $user_appointment->barber_id, '', 1);
                                    }
                                    if ($users_booking->platform == 'ios') {
                                        $this->ios($users_booking->device_id, $users_booking->id, 'Reminder - You have an appointment today with ' . $barber->first_name . ' ' . $barber->last_name . ' at ' . $user_appointment->start_time, json_encode($user_appointment), $user_appointment->barber_id, '', 1);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $bookings = Booking::where('user_id', $users_booking->id)->where('status', 'Approved')->get();
                    foreach ($bookings as $booking) {
                        $user_time_zone = $booking->User->time_zone / 60;
                        if ($user_time_zone < 0) {
                            $user_time_zone = -1 * $user_time_zone;
                            $cur_time = Carbon::now()->addHours($user_time_zone);
                            $cur_time_string = $cur_time->toDateString();
                            $tomorrow_time = Carbon::tomorrow()->addHours($user_time_zone);
                            $tomorrow_time_string = $tomorrow_time->toDateString();
                        } else if ($user_time_zone > 0) {
                            $cur_time = Carbon::now()->subHours($user_time_zone);
                            $cur_time_string = $cur_time->toDateString();
                            $tomorrow_time = Carbon::tomorrow()->subHours($user_time_zone);
                            $tomorrow_time_string = $tomorrow_time->toDateString();
                        }
                        $oneHoursBefore = $cur_time;
                        $booking_time = Carbon::parse($booking->date . ' ' . $booking->start_time);
                        $hourLenght = $booking_time->diffInMinutes($oneHoursBefore);
                        if ($booking->send_status == 1 || $booking->send_status == 0) {
                            if ($hourLenght <= 60) {
                                Booking::where('id', $booking->id)->update(['send_status' => 2]);
                                if ($booking->User->platform == 'android') {
                                    $barber = User::where('id', $booking->barber_id)->first();
                                    $this->android($booking->User->device_id, $booking->User->id, 'Reminder - You have an appointment scheduled with ' . $barber->first_name . ' ' . $barber->last_name . ' in a hour', json_encode($booking), $booking->barber_id, '', 1);
                                }
                                if ($booking->User->platform == 'ios')
                                    $this->android($booking->User->device_id, $booking->User->id, 'Reminder - You have an appointment scheduled with ' . $barber->first_name . ' ' . $barber->last_name . ' in a hour', json_encode($booking), $booking->barber_id, '', 1);
                            }
                        }
                    }
                }

            }
        }

    }
    public function barberCron()
    {
        set_time_limit(0);
        $barber_bookings = User::with('getBookings')->where('user_type', '<>', 'User')->get();
        foreach ($barber_bookings as $barber_booking) {
            if (is_numeric($barber_booking->time_zone)) {
                $booking = $barber_booking->getBookings;
                $barber = $barber_booking;
                $user_time_zone = $barber->time_zone / 60;
                if ($user_time_zone < 0) {
                    $user_time_zone = -1 * $user_time_zone;
                    $cur_time = Carbon::now()->addHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->addHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                } else if ($user_time_zone > 0) {
                    $cur_time = Carbon::now()->subHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->subHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                }
                if ($cur_time->hour == 6) {
                    if ($barber_booking->send_status == 0) {
                        $user_appointments = Booking::where('status', 'Approved')->where('barber_id', $barber_booking->id)->where('date', '>=', $cur_time_string)->where('date', '<', $tomorrow_time_string)->orderBy('start_time', 'asc')->get();
                        if ($user_appointments) {
                            User::where('id', $barber_booking->id)->update(['send_status' => 1]);
                            if ($barber_booking->platform == 'android') {
                                $this->android($barber_booking->device_id, $barber_booking->id, 'Reminder - You have ' . $user_appointments->count() . ' appointments scheduled today starting at ' . $user_appointments->first()->start_time, json_encode($barber_booking->getBookings->first()), $barber_booking->barber_id, '', 1);
                            }
                            if ($barber_booking->platform == 'ios') {
                                $this->android($barber_booking->device_id, $barber_booking->id, 'Reminder - You have ' . $user_appointments->count() . ' appointments scheduled today starting at ' . $user_appointments->first()->start_time, json_encode($barber_booking->getBookings->first()), $barber_booking->barber_id, '', 1);
                            }
                        }
                    }
                }
            }
        }
    }
    public function resetSendStatusBarber(){
//        $user = User::where('id',874)->first();
//        $timestamp = strtotime(date('Y-m-d'));
//        $lat = $user->lat;
//        $lng = $user->lng;
//        $curl_url = "https://maps.googleapis.com/maps/api/timezone/json?location=$lat,$lng&timestamp=$timestamp&key=AIzaSyCRJkfzsC8FMiVDXkGeLHcof05GUiWxWDw";
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $curl_url);
//        curl_setopt($ch, CURLOPT_HEADER, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $response = json_decode(curl_exec($ch));
//        curl_close($ch);
//        $sign = "-";
//        $GMT_hours = "+00";
//        //dd($response);
//        $gmtTime = (abs($response->rawOffset) - $response->dstOffset);
//        if ($response->status == 'OK') {
//            if (strpos($response->rawOffset, '-') !== FALSE)
//                $sign = "+";
//            $GMT_hours = $sign . gmdate("H", abs($gmtTime));
//        }
//        dd($GMT_hours);



//        $users = User::all();
//        foreach ($users as $user){
//            if(!is_numeric($user->time_zone) || $user->time_zone == null){
//                $timestamp = strtotime(date('Y-m-d'));
//                $lat = $user->lat;
//                $lng = $user->lng;
//                if($user->lat != '' || $user->lng != '') {
//
//
//                    $curl_url = "https://maps.googleapis.com/maps/api/timezone/json?location=$lat,$lng&timestamp=$timestamp&key=AIzaSyCRJkfzsC8FMiVDXkGeLHcof05GUiWxWDw";
//                    $ch = curl_init();
//                    curl_setopt($ch, CURLOPT_URL, $curl_url);
//                    curl_setopt($ch, CURLOPT_HEADER, false);
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                    $response = json_decode(curl_exec($ch));
//                    //dd($response,$user);
//                    curl_close($ch);
//                    $sign = "-";
//                    $GMT_hours = "+00";
//                    $gmtTime = (abs($response->rawOffset) - $response->dstOffset);
//                    if ($response->status == 'OK') {
//                        if (strpos($response->rawOffset, '-') !== FALSE)
//                            $sign = "+";
//                        $GMT_hours = $sign . gmdate("H", abs($gmtTime));
//                        $user->time_zone = $GMT_hours * 60;
//                        $user->save();
//                    }
//                }
//            }
//        }
//dd('success');






        set_time_limit(0);
        $barbers = User::where('send_status',1)->get();
        foreach ($barbers as $barber) {
            if (is_numeric($barber->time_zone)) {
                $user_time_zone = $barber->time_zone / 60;
                if ($user_time_zone < 0) {
                    $user_time_zone = -1 * $user_time_zone;
                    $cur_time = Carbon::now()->addHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->addHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                } else if ($user_time_zone > 0) {
                    $cur_time = Carbon::now()->subHours($user_time_zone);
                    $cur_time_string = $cur_time->toDateString();
                    $tomorrow_time = Carbon::tomorrow()->subHours($user_time_zone);
                    $tomorrow_time_string = $tomorrow_time->toDateString();
                }
                if ($cur_time->hour != 6) {
                    $barber->send_status = 0;
                    $barber->save();
                }
            }
        }
    }

    function updateAddressProfessionals($barberId = '') {
        //Professionals Count
        $professionals = User::where('user_type', '!=', 'User')->where('shop_city', '');
        if ($barberId != '') {
            $professionals->where('id', $barberId);
        }
        $professionals = $professionals->get();
        foreach ($professionals as $key => $value) {
            $address = false;
            if (($value->shop_lat == '' && $value->shop_long == '')) {
                $address = getAddressByLatLon($value->lat, $value->lng);
            } else {
                $address = getAddressByLatLon($value->shop_lat, $value->shop_long);
            }
            if ($address) {
                $value->shop_city = $address['city'];
                $value->shop_state = $address['state'];
                echo 'Updated Email: ' . $value->email . '<br>';
            } else {
                $value->shop_city = '';
                $value->shop_state = '';
                echo 'No Address Found for Email: ' . $value->email . '<br>';
            }
            $value->save();
        }
    }

    function updateBarberTime() {

        $validator = Validator::make(
            array(
                'start_time' => $_POST['start_time'],
                'date' => $_POST['date'],
                'barber_id' => $_POST['barber_id'],
                'booking_id' => $_POST['booking_id'],
            ), array(
                'start_time' => 'required',
                'date' => 'required',
                'barber_id' => 'required',
                'booking_id' => 'required',
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        }


        $start_time = $_POST['start_time'];
        $date = $_POST['date'];
        $barber_id = $_POST['barber_id'];
        $booking_id = $_POST['booking_id'];
        $booking = Booking::where('id', $booking_id)->first();
        $barber = User::where('id', $barber_id)->first();

        $user = User::where('id', $booking->user_id)->first();
        $data['barber'] = $barber;
        $data['booking'] = $booking;
        $data['start_time'] = $start_time;
        $data['date'] = $date;

        $full_name = $barber->first_name.' '.$barber->last_name;
        $notificatinText = $full_name.' wants to change the appointment';

        if ($user->platform == 'android') {
            $this->android($user->device_id, $user->id, $notificatinText, json_encode($data), $barber->id, $booking_id);
        }
        if ($user->platform == 'ios') {
            $this->ios($user->device_id, $user->id, $notificatinText, json_encode($data), $barber->id, $booking_id);
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $booking));
    }

    function requestStatus() {
        $booking_id = $_POST['booking_id'];
        $user_id = $_POST['user_id'];
        $status = $_POST['status'];

        $start_time = $_POST['start_time'];
        $date = $_POST['date'];

        $booking = Booking::where('id', $booking_id)->first();
        $barber = User::where('id', $booking->barber_id)->first();

        $user = User::where('id', $user_id)->first();
        if ($status == 0) {
            $notificatinText = $user->first_name .' '.$user->last_name. ' has rejected your appointment change';
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $barber->id, $notificatinText, json_encode($booking), $user_id, $booking_id);
            }
            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $barber->id, $notificatinText, json_encode($booking), $user_id, $booking_id);
            }
        } else {
            $notificatinText = $user->first_name .' '.$user->last_name. ' has accepted your appointment change';
            Booking::where('id', $booking_id)->update(['start_time' => $start_time, 'date' => $date]);
            if ($barber->platform == 'ios') {
                $this->ios($barber->device_id, $barber->id, $notificatinText, json_encode($booking), $user_id, $booking_id);
            }
            if ($barber->platform == 'android') {
                $this->android($barber->device_id, $barber->id, $notificatinText, json_encode($booking), $user_id, $booking_id);
            }
        }
        return Response::json(array('status' => 'success', 'successMessage' => 'Account Updated', 'successData' => $booking));
    }

    function sendNotification($barber) {
        $users=Booking::select('user_id')->where('barber_id',$barber->id)->get()->toArray();
        $all_users=User::whereIn('id',$users)->get();
        $notificatinText= $barber->first_name .' '.$barber->last_name .'has added new announcement';
        foreach ($all_users as $user){
            if($user->send_notifiction){
                if ($user->platform == 'ios') {
                    $this->ios($user->device_id, $user->id, $notificatinText, json_encode($barber), $barber->id, '');
                }
                if ($user->platform == 'android') {
                    $this->android($user->device_id, $user->id, $notificatinText,json_encode($barber) , $barber->id, '');
                }
            }}
        return TRUE;
    }

}
