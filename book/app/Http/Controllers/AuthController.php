<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\User;
use App\UserStripe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use App\Service;
use App\Category;
use App\Gallery;
use App\BussnessHour;
use App\Booking;
use App\Notification;
use App\ProfessionalType;

class AuthController extends Controller {

    function register() {

        $validator = Validator::make(
                        array(
                    'first_name' => $_POST['first_name'],
                    'email' => $_POST['email'],
                    //'password' => $_POST['password'],
                    'device_id' => $_POST['device_id'],
                    'platform' => $_POST['platform'],
                    //'mobile' => $_POST['mobile'],
                    'user_type' => $_POST['user_type'],
                    'login_type' => $_POST['login_type'],
                    'lat' => $_POST['lat'],
                    'lng' => $_POST['long'],
                    'location' => $_POST['location']
                        ), array(
                    'first_name' => 'required',
                    'email' => 'required | email | unique:users',
                    //'password' => 'required | min:6',
                    //'mobile' => 'required',
                    'device_id' => 'required',
                    'platform' => 'required',
                    'user_type' => 'required',
                    'login_type' => 'required',
                    'lat' => 'required',
                    'lng' => 'required',
                    'location' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages()->all();
            $messages = join(',', $messages);
            return Response::json(array('status' => 'error', 'errorMessage' => $messages));
        } else {
            $file = Input::file('profile_pic');
            $add_user = new User;
            if ($_POST['login_type'] != 'email') {
                $add_user->social_pic = $_POST['profile_pic'];
                $add_user->profile_pic = '';
            } elseif ($file) {
                $image = Input::file('profile_pic');
                $image_extension = $image->getClientOriginalExtension(); // getting image extension
                $image_fileName = 'profile_' . Str::random(15) . '.' . $image_extension; // renameing image
                $image_destination_path = 'public/images/'.$image_fileName; // upload path
                Image::make($image->getRealPath())->resize(500, 500)->save($image_destination_path);
//                $image_destination_path = 'public/images'; // upload path
//                $image_extension = Input::file('profile_pic')->getClientOriginalExtension(); // getting image extension
//                $image_fileName = 'profile_' . Str::random(15) . '.' . $image_extension; // renameing image
//                Input::file('profile_pic')->move($image_destination_path, $image_fileName);
                $add_user->profile_pic = $image_fileName;
                $add_user->social_pic = '';
            } else {
                $add_user->profile_pic = '';
                $add_user->social_pic = '';
            }

            //Save user current time zone
            //$location = json_decode(file_get_contents('http://freegeoip.net/json/' . \Request::ip()));
            $add_user->time_zone = $_POST['time_zone'];
            
            $add_user->first_name = $_POST['first_name'];
            $add_user->last_name = $_POST['last_name'];
            $add_user->username = (isset($_POST['username'])?$_POST['username']:'');
            $add_user->email = $_POST['email'];
            $add_user->password = bcrypt($_POST['password']);
            $add_user->mobile = $_POST['mobile'];
            $add_user->device_id = $_POST['device_id'];
            $add_user->platform = $_POST['platform'];
            $add_user->user_type = $_POST['user_type'];
            $add_user->login_type = $_POST['login_type'];
            $add_user->lat = $_POST['lat'];
            $add_user->lng = $_POST['long'];
            $add_user->session_token = bcrypt(time());
            $add_user->location = $_POST['location'];
            $add_user->is_login = 1;
            $add_user->save();
            
            if ($_POST['user_type'] != 'User') {
                $email = $_POST['email'];
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $acc = \Stripe\Account::create(array(
                            "managed" => true,
                            "country" => "US",
                            "email" => "$email"
                ));
                $add_user->account_status = 'Unverified';
                $add_stripe = new UserStripe;
                $add_stripe->barber_id = $add_user->id;
                $add_stripe->stripe_payout_account_id = $acc->id;
                $add_stripe->stripe_payout_account_public_key = $acc->keys->publishable;
                $add_stripe->stripe_payout_account_secret_key = $acc->keys->secret;
                $add_stripe->stripe_payout_account_info = json_encode($acc);
                $add_stripe->save();

                $add_user->services = Service::where('barber_id', $add_user->id)->get();
                $add_user->category = Category::where('barber_id', $add_user->id)->get();
                $add_user->bussiness_hours = BussnessHour::where('barber_id', $add_user->id)->first();
                $add_user->gallery_images = Gallery::where('barber_id', $add_user->id)->orderBy('created_at', 'desc')->get();
                $add_user->bookings = Booking::where('date', '>=', date('Y-m-d'))->where('barber_id', $add_user->id)->with('User')->orderBy('date', 'desc')->get();
                $add_user->notificationcount = Notification::where(array('to_user' => $add_user->id, 'is_read' => 0))->count();
                $add_user->account_last_four = '';
                $add_user->account_card_brand = '';
                $add_user->shop_name = '';
                $add_user->shop_location = '';
                
                //Update City and State
                $address = getAddressByLatLon($_POST['lat'], $_POST['long']);
                if($address){
                    $add_user->shop_city = $address['city'];
                    $add_user->shop_state = $address['state'];
                }
                
                $add_user->account = json_encode($acc);
//            type = individual
            }
            $add_user->professional_types = ProfessionalType::all();
            return Response::json(array('status' => 'success', 'successData' => $add_user));
        }
    }

    function login() {
        if (Auth::attempt(['password' => $_POST['password'], 'email' => $_POST['email']])) {
            $check_user = User::find(Auth::user()->id);
            if($check_user->is_admin == 1){
                return Response::json(array('status' => 'error', 'errorMessage' => 'Admin Cant Login'));
            }
            $check_user->device_id = $_POST['device_id'];
            $check_user->platform = $_POST['platform'];
            $check_user->lat = $_POST['lat'];
            $check_user->lng = $_POST['long'];
            $check_user->location = $_POST['location'];
            $check_user->session_token = bcrypt(time());
            $check_user->is_login = 1;

            //Save user current time zone
            //$location = json_decode(file_get_contents('http://freegeoip.net/json/' . \Request::ip()));
            $check_user->time_zone = $_POST['time_zone'];

            $check_user->save();
            if ($check_user->user_type != 'User') {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $user = UserStripe::where('barber_id', $check_user->id)->first();
                $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
                $user->stripe_payout_account_info = json_encode($account);
                $user->save();
                $check_user->account = json_encode($account);
                $check_user->account_status = $account->legal_entity->verification->status;
                if (isset($account->external_accounts->data[0])) {
                    $check_user->account_last_four = $account->external_accounts->data[0]->last4;
                    $check_user->account_card_brand = $account->external_accounts->data[0]->brand;
                } else {
                    $check_user->account_last_four = '';
                    $check_user->account_card_brand = '';
                }
            }
            $check_user->services = Service::where('barber_id', $check_user->id)->get();
            $check_user->category = Category::where('barber_id', $check_user->id)->get();
            $check_user->bussiness_hours = BussnessHour::where('barber_id', $check_user->id)->first();
            $check_user->gallery_images = Gallery::where('barber_id', $check_user->id)->orderBy('created_at', 'desc')->get();
            $check_user->bookings = Booking::where('date', '>=', date('Y-m-d'))->where('barber_id', $check_user->id)->with('User')->orderBy('date', 'desc')->get();
            $check_user->notificationcount = Notification::where(array('to_user' => $check_user->id, 'is_read' => 0))->count();

            $check_user->professional_types = ProfessionalType::all();
            return Response::json(array('status' => 'success', 'successData' => $check_user));
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'Invalid Email Or Password'));
        }
    }

    function CheckSocialLogin() {
        $check_type = User::where(array('login_type' => $_POST['login_type'], 'email' => $_POST['email']))->first();
        $check_email = User::where(array('email' => $_POST['email']))->first();
        if ($check_type) {
            $check_type->device_id = $_POST['device_id'];
            $check_type->platform = $_POST['platform'];
            $check_type->lat = $_POST['lat'];
            $check_type->lng = $_POST['long'];
            $check_type->location = $_POST['location'];
            $check_type->session_token = bcrypt(time());
            $check_type->is_login = 1;
            $check_type->save();
            if ($check_type->user_type != 'User') {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $user = UserStripe::where('barber_id', $check_type->id)->first();
                $account = \Stripe\Account::retrieve($user->stripe_payout_account_id);
                $user->stripe_payout_account_info = json_encode($account);
                $user->save();
                $check_type->account_status = $account->legal_entity->verification->status;
                $check_type->account = json_encode($account);
                if (isset($account->external_accounts->data[0])) {
                    $check_type->account_last_four = $account->external_accounts->data[0]->last4;
                    $check_type->account_card_brand = $account->external_accounts->data[0]->brand;
                } else {
                    $check_type->account_last_four = '';
                    $check_type->account_card_brand = '';
                }
            }
            $check_type->gallery_images = Gallery::where('barber_id', $check_type->id)->orderBy('created_at', 'desc')->get();
            $check_type->services = Service::where('barber_id', $check_type->id)->get();
            $check_type->bussiness_hours = BussnessHour::where('barber_id', $check_type->id)->first();

            $check_type->bookings = Booking::where('date', '>=', date('Y-m-d'))->where('barber_id', $check_type->id)->with('User')->orderBy('date', 'desc')->get();
            $check_type->notificationcount = Notification::where(array('to_user' => $check_type->id, 'is_read' => 0))->count();

            return Response::json(array('status' => 'success', 'successData' => $check_type));
        } elseif ($check_email) {
            return Response::json(array('status' => 'error', 'errorMessage' => 'Email Already Taken'));
        } else {
            return Response::json(array('status' => 'success', 'can_register' => TRUE));
        }
    }

    function forgetPasswordMial() {
        if (isset($_POST['email'])) {
            $check_email = User::where('email', $_POST['email'])->first();
            if ($check_email) {
                $code = str_random(8);
                $check_email->password = bcrypt($code);
                $check_email->save();
                $viewData['password'] = $code;
//                Mail::send('forgetmail', $viewData, function ($m) use ($check_email) {
//                    $m->from('Info@taperup.com', 'Taper');
//                    $m->to($check_email->email, $check_email->username)->subject('Your New Password');
//                });
                return Response::json(array('status' => 'success', 'successData' => 'Code Has Been Send to your email'));
            } else {
                return Response::json(array('status' => 'error', 'errorMessage' => 'Email Not Found'));
            }
        } else {
            return Response::json(array('status' => 'error', 'errorMessage' => 'Email Is Required'));
        }
    }

    function adminLogin() {
//echo       bcrypt('admintaper');exit;
        if (Auth::attempt(['password' => $_POST['password'], 'email' => $_POST['email'], 'is_admin' => 1])) {
            return Redirect::to(URL::previous());
        } else {
            Session::flash('error', 'Invalid Email Or Password');
            return Redirect::to(URL::previous());
        }
    }

    function userLogout($id) {
        $user = User::find($id);
        $user->is_login = 0;
        $user->save();
        return Response::json(array('status' => 'success', 'successMessage' => 'User Logout SuccessFully'));
    }
function notificationTests($device_id){
    $data = array('barber_data' => '', 'text' => 'test', 'user_id' => '', 'booking_id' => '', 'timeago' => 'Just Now');
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', 
                $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        $body['aps'] = array(
            'alert' => 'Hello Testing',
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
       if (!$result)
			return 'Message not delivered' . PHP_EOL;
		else
			return 'Message successfully delivered' . PHP_EOL;
}
public function notificationTest($devicetoken) {
    $data = array('barber_data' => '', 'text' => 'test', 'user_id' => '', 'booking_id' => '', 'timeago' => 'Just Now');
		$deviceToken = $devicetoken;
		$ctx = stream_context_create();
		// ck.pem is your certificate file
		stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', '');
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		// Create the payload body
		$body['aps'] = array(
            'alert' => 'Hello Testing',
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		// Close the connection to the server
		fclose($fp);
		if (!$result)
			return 'Message not delivered' . PHP_EOL;
		else
			return 'Message successfully delivered' . PHP_EOL;
	}
}
