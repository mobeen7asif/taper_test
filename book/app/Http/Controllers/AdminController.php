<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promotion;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Faq;
use App\User;
use App\Service;
use App\Category;
use Illuminate\Support\Facades\Validator;
use App\UserStripe;
use App\BussnessHour;
use App\History;
use Carbon\Carbon;
use App\Gallery;
use App\Booking;
use App\Notification;
use App\ProfessionalType;
class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    function dashboard() {
        $data['title'] = 'Dashboard';
        return view('dashboard', $data);
    }

    function faqs() {
        $data['title'] = 'Faqs';
        $data['faqs'] = Faq::orderBy('created_at', 'desc')->get();
        return view('faq', $data);
    }

    function addfaq() {
        $data['title'] = 'Add Faq';
        return view('add_faq', $data);
    }

    function editFaq($id) {
        $data['faq'] = Faq::find($id);
        $data['title'] = 'Edit Faq';
        return view('edit_faq', $data);
    }

    function storeFaq() {
        $store_faq = new Faq;
        $message = 'Faq added successfully';
        if (isset($_POST['faq_id'])) {
            $store_faq = Faq::find($_POST['faq_id']);
            $message = 'Faq updated successfully';
        }
        $store_faq->question = $_POST['question'];
        $store_faq->answer = $_POST['answer'];
        $store_faq->save();
        Session::flash('success', $message);
        return Redirect::to(URL::previous());
    }

    function deleteFaq($id) {
        $promo = Faq::find($id);
        $promo->delete();
        Session::flash('success', 'Faq Deleted Successfully');
        return Redirect::to(URL::previous());
    }

    function promations() {
        $data['title'] = 'Promations';
        $data['promos'] = Promotion::orderBy('created_at', 'desc')->get();
        return view('promotions', $data);
    }

    function addPromations() {
        $data['title'] = 'Add Promation';
        return view('add_promorion', $data);
    }

    function editPromations($id) {
        $data['promo'] = Promotion::find($id);
        $data['title'] = 'Edit Promation';
        return view('edit_promorion', $data);
    }

    function storePromations() {
        $store_pro = new Promotion;
        $message = 'promotion added successfully';
        if (isset($_POST['post_id'])) {
            $store_pro = Promotion::find($_POST['post_id']);
            $message = 'promotion updated successfully';
        }
        $store_pro->title = $_POST['title'];
        $store_pro->details = $_POST['description'];
        $file = Input::file('image');
        if ($file) {
            $image_destination_path = 'public/images/promo'; // upload path
            $image_extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
            $image_fileName = 'promo_' . Str::random(15) . '.' . $image_extension; // renameing image
            Input::file('image')->move($image_destination_path, $image_fileName);
            $store_pro->image = $image_fileName;
        }
        $store_pro->save();
        Session::flash('success', $message);
        return Redirect::to(URL::previous());
    }

    function deletePromo($id) {
        $promo = Promotion::find($id);
        $promo->delete();
        Session::flash('success', 'Promotion Deleted Successfully');
        return Redirect::to(URL::previous());
    }

    function deleteBarder($id) {
        $promo = User::find($id);
        $promo->delete();
        Session::flash('success', 'User Deleted Successfully');
        return Redirect::to(URL::previous());
    }

    function users() {
        $data['title'] = 'Customers';
        $data['users'] = User::where('user_type', 'User')->orderBy('first_name', 'asc')->get();
        return view('users', $data);
    }

    function barbaers() {

        $data['title'] = 'Professionals';
        $data['barbers'] = User::where('user_type', '<>', 'User')->orderBy('first_name', 'asc')->get();
        return view('barbers', $data);
    }

    function barbersDetails($id) {
        $data['title'] = 'Professional Details';
        $barber = $data['barber'] = User::find($id);
        $data['services'] = Service::with('getCategory')->where('barber_id', $id)->get();
        $data['categories'] = Category::where('barber_id', $id)->get();
        $data['bussiness'] = BussnessHour::where('barber_id', $id)->first();
        $data['professional_types'] = ProfessionalType::all();
        $types = explode(',',$barber->user_type);
        $data['types'] = $types;

        //print_r($data['services']);die;
        return view('barbers_detail', $data);
    }

    function usersDetails($id) {
        $data['title'] = 'Customer Details';
        $data['user'] = User::find($id);

        return view('users_detail', $data);
    }

    function addBarbder() {
        $data['title'] = 'Add Professional';
        $data['professional_types'] = ProfessionalType::all();
        return view('add_barbder', $data);
    }

    function storeBarber() {
        $validator = Validator::make(
                        array(
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'user_type' => $_POST['user_type'],
                    'password' => $_POST['password'],
                    'mobile' => $_POST['phone']
                        ), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'username' => 'required',
                    'email' => 'required | email | unique:users',
                    'user_type' => 'required',
                    'password' => 'required | min:6',
                    'mobile' => 'required'
                        )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        } else {
            $add_user = new User;
            $add_user->first_name = $_POST['first_name'];
            $add_user->last_name = $_POST['last_name'];
            $add_user->username = $_POST['username'];
            $add_user->email = $_POST['email'];
            $add_user->password = bcrypt($_POST['password']);
            $add_user->mobile = $_POST['phone'];
            //$add_user->user_type = $_POST['user_type'];
            $user_type = implode(',',$_POST['user_type']);
            $add_user->user_type = $user_type;
            $add_user->login_type = 'email';
            $add_user->session_token = '';
            $add_user->shop_location = '';
            $add_user->shop_long = '';
            $add_user->shop_lat = '';
            $add_user->save();
            $email = $_POST['email'];
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $acc = \Stripe\Account::create(array(
                        "managed" => true,
                        "country" => "US",
                        "email" => "$email"
            ));
            $add_user->account_status = 'Unverified';
            $add_stripe = new UserStripe;
            $add_stripe->barber_id = $add_user->id;
            $add_stripe->stripe_payout_account_id = $acc->id;
            $add_stripe->stripe_payout_account_public_key = $acc->keys->publishable;
            $add_stripe->stripe_payout_account_secret_key = $acc->keys->secret;
            $add_stripe->stripe_payout_account_info = json_encode($acc);
            $add_stripe->save();
            Session::flash('success', 'Barber Added SuccessFully');
            return Redirect::to(URL::previous());
        }
    }

    function changeBarbderPassword() {
        $validator = Validator::make(
                        array(
                    'password' => $_POST['password'],
                    'password_confirmation' => $_POST['cpassword']
                        ), array(
                    'password' => 'required | min:6 |confirmed',
                    'password_confirmation' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $barber = User::find($_POST['id']);
        $barber->password = bcrypt($_POST['password']);
        $barber->save();
        Session::flash('success', 'Password Changed SuccessFully');
        return Redirect::to(URL::previous());
    }

    function changeBarberDetails() {
        $validator = Validator::make(
                        array(
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'email' => $_POST['email'],
                    'username' => $_POST['username'],
                    'shopname' => $_POST['shopname'],
                    'shop_location' => $_POST['shop_location'],
                    'mobile' => $_POST['mobile'],
                            'user_type' => $_POST['user_type']
                        ), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required | email',
                    'username' => 'required',
                    'shopname' => 'required',
                    'shop_location' => 'required',
                    'mobile' => 'required',
                            'user_type' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        } else {
            $checkeamil = User::where('email', $_POST['email'])
                            ->where('id', '!=', $_POST['id'])->first();
            if ($checkeamil) {
                Session::flash('error', 'Email Already Taken');
                return Redirect::to(URL::previous());
            }
            $barber = User::find($_POST['id']);
            $barber->first_name = $_POST['first_name'];
            $barber->last_name = $_POST['last_name'];
            $barber->email = $_POST['email'];
            $barber->username = $_POST['username'];
            $barber->shop_name = $_POST['shopname'];
            $barber->shop_location = $_POST['shop_location'];
            $barber->cancel_fee = $_POST['cancel_fee'];
            $barber->mobile = $_POST['mobile'];
            $user_type = implode(',',$_POST['user_type']);
            $barber->user_type = $user_type;
            if ($barber->announcment != $_POST['announcment']) {
                $barber->announcment = $_POST['announcment'];
                $barber->announcment_date = Carbon::now();
                $this->sendNotification($barber);
            }
//            $barber->user_type = $_POST['user_type'];
             \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
              $update_user = '';
            $payment_type = $_POST['payment_type'];
            $is_subscription = $_POST['subscription_type'];

//            $is_subscription = 1;
//            if($_POST['payment_type'] == 0){
//                $is_subscription = 0;
//            }
            if($barber->is_subscribed == $is_subscription){
                $update_user = User::where('id', $barber->id)->update(['is_cash' => $payment_type]);
            }
            else {
                if($barber->is_subscribed == 0 and $is_subscription == 1){
                    $barber->newSubscription('Cash Subscription', 'cashsubscription')->create();
                    $update_user = User::where('id', $barber->id)->update(['is_cash' => $payment_type , 'is_subscribed' => $is_subscription]);
                }
                else if($barber->is_subscribed == 1 and $is_subscription == 0) {
                    if ($barber->subscribed('Cash Subscription', 'cashsubscription')) {
                        $barber->subscription('Cash Subscription', 'cashsubscription')->cancelNow();
                    }
                    $update_user = User::where('id', $barber->id)->update(['cancel_fee' => 0 ,'is_cash' => 0, 'is_subscribed' => $is_subscription]);
                }
            }
            $barber->save();
            Session::flash('success', 'Barber updated');
            return Redirect::to(URL::previous());
        }
    }

    function changeUserDetails() {
        $validator = Validator::make(
                        array(
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile']
                        ), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required | email',
                    'mobile' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        } else {
            $checkeamil = User::where('email', $_POST['email'])
                            ->where('id', '!=', $_POST['id'])->first();
            if ($checkeamil) {
                Session::flash('error', 'Email Already Taken');
                return Redirect::to(URL::previous());
            }
            $barber = User::find($_POST['id']);
            $barber->first_name = $_POST['first_name'];
            $barber->last_name = $_POST['last_name'];
            $barber->email = $_POST['email'];
            $barber->mobile = $_POST['mobile'];
            $barber->save();
            Session::flash('success', 'User updated');
            return Redirect::to(URL::previous());
        }
    }

    function addService() {
        $validator = Validator::make(
                        array(
                    'service' => $_POST['service'],
                    'price' => $_POST['price'],
                    'time' => $_POST['time']
                        ), array(
                    'service' => 'required',
                    'price' => 'required',
                    'time' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $add_service = new Service;
        $add_service->barber_id = $_POST['id'];
        $add_service->service_type = $_POST['service'];
        $add_service->time = $_POST['time'];
        $add_service->price = $_POST['price'];
        $add_service->category_id = $_POST['category_id'];
        $add_service->description = $_POST['description'];
        $add_service->save();
        Session::flash('success', 'Service Added SuccessFully');
        return Redirect::to(URL::previous());
    }

    function addCategory() {
        $validator = Validator::make(
                        array(
                    'category' => $_POST['category']
                        ), array(
                    'category' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $add_category = new Category();
        $add_category->barber_id = $_POST['id'];
        $add_category->category = $_POST['category'];
        $add_category->description = $_POST['description'];
        $add_category->save();
        Session::flash('success', 'Category Added SuccessFully');
        return Redirect::to(URL::previous());
    }

    function updateService() {
        $validator = Validator::make(
                        array(
                    'service' => $_POST['service'],
                    'price' => $_POST['price'],
                    'time' => $_POST['time']
                        ), array(
                    'service' => 'required',
                    'price' => 'required',
                    'time' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $add_service = Service::find($_POST['id']);
        $add_service->service_type = $_POST['service'];
        $add_service->time = $_POST['time'];
        $add_service->price = $_POST['price'];
        $add_service->category_id = $_POST['category_id'];
        $add_service->description = $_POST['description'];
        $add_service->save();
        Session::flash('success', 'Service Updated SuccessFully');
        return Redirect::to(URL::previous());
    }
    function updateCategory() {
        $validator = Validator::make(
                           array(
                    'category' => $_POST['category'],
                    'description' => $_POST['description']
                        ), array(
                    'category' => 'required',
                    'description' => 'required'
                        )
        );

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $add_category = Category::find($_POST['id']);
        $add_category->category_id = $_POST['category_id'];
        $add_category->description = $_POST['description'];
        $add_category->save();
        Session::flash('success', 'Category Updated SuccessFully');
        return Redirect::to(URL::previous());
    }

    function deleteCategory($id) {
        $services = Service::where('category_id', $id)->get();

        if(count($services) < 1) {
            $category = Category::find($id);
            $category->delete();
            Session::flash('success', 'Categroy Deleted SuccessFully');
        } else {
            Session::flash('error', 'Categroy in use');
        }
            return Redirect::to(URL::previous());
    }
    function deleteService($id) {
        $add_service = Service::find($id);
        $add_service->delete();
        Session::flash('success', 'Service Deleted SuccessFully');
        return Redirect::to(URL::previous());
    }

    function updateHours() {
        if (isset($_POST['mon'][4])) {
            $mon = 'Off Day-Off Day';
        } else {
            $mon = $_POST['mon'][0] . $_POST['mon'][1] . '-' . $_POST['mon'][2] . $_POST['mon'][3];
        }
        if (isset($_POST['tue'][4])) {
            $tue = 'Off Day-Off Day';
        } else {
            $tue = $_POST['tue'][0] . $_POST['tue'][1] . '-' . $_POST['tue'][2] . $_POST['tue'][3];
        }
        if (isset($_POST['wed'][4])) {
            $wed = 'Off Day-Off Day';
        } else {
            $wed = $_POST['wed'][0] . $_POST['wed'][1] . '-' . $_POST['wed'][2] . $_POST['wed'][3];
        }
        if (isset($_POST['thus'][4])) {
            $thus = 'Off Day-Off Day';
        } else {
            $thus = $_POST['thus'][0] . $_POST['thus'][1] . '-' . $_POST['thus'][2] . $_POST['thus'][3];
        }
        if (isset($_POST['fri'][4])) {
            $fri = 'Off Day-Off Day';
        } else {
            $fri = $_POST['fri'][0] . $_POST['fri'][1] . '-' . $_POST['fri'][2] . $_POST['fri'][3];
        }
        if (isset($_POST['sat'][4])) {
            $sat = 'Off Day-Off Day';
        } else {
            $sat = $_POST['sat'][0] . $_POST['sat'][1] . '-' . $_POST['sat'][2] . $_POST['sat'][3];
        }
        if (isset($_POST['sun'][4])) {
            $sun = 'Off Day-Off Day';
        } else {
            $sun = $_POST['sun'][0] . $_POST['sun'][1] . '-' . $_POST['sun'][2] . $_POST['sun'][3];
        }

        $upadte_hours = BussnessHour::where('barber_id', $_POST['id'])->first();
        if (!$upadte_hours) {
            $upadte_hours = new BussnessHour;
            $upadte_hours->barber_id = $_POST['id'];
        }
        $upadte_hours->mon = $mon;
        $upadte_hours->tue = $tue;
        $upadte_hours->wed = $wed;
        $upadte_hours->thus = $thus;
        $upadte_hours->fri = $fri;
        $upadte_hours->sat = $sat;
        $upadte_hours->sun = $sun;
        $upadte_hours->save();
        Session::flash('success', 'Bussiness Hours SuccessFully');
        return Redirect::to(URL::previous());
    }

    function payments() {
        $data['title'] = 'Payments';
        $data['payments'] = History::with('getBarber')->orderBy('created_at', 'desc')->get();
        $data['total'] = History::sum('total_payments');
        $data['userincome'] = History::sum('earning');
        $data['remaining'] = History::sum('app_blance');
        $data['app_earning'] = $data['total'] - $data['userincome'] - $data['remaining'];
        return view('payments', $data);
    }

    function getProfessionalTypes(){
        $data['professional_types'] = ProfessionalType::all();
        $data['title'] = 'Professional Types';
        return view('professional_types', $data);
    }
    function addProfessionalTypeView(){
        $data['title'] = 'Professional Types';
        return view('add_professional_type',$data);
    }
    function addProfessionalType(){
        $validator = Validator::make(
            array(
                'name' => $_POST['name'],
            ), array(
                'name' => 'required',
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $professional_type = new ProfessionalType();
        $professional_type->name = $_POST['name'];
        $professional_type->save();
        Session::flash('success', 'Professional Type Added SuccessFully');
        return Redirect::to(URL::previous());
    }
    function editProfessionalTypeView($id){
        $professional_type = ProfessionalType::where('id',$id)->first();
        return view('edit_professional_type', ['professional_type' => $professional_type,'title' => 'Professional Types']);
    }
    function updateProfessionalType($id){
        $validator = Validator::make(
            array(
                'name' => $_POST['name'],
            ), array(
                'name' => 'required',
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(URL::previous())->with('message', $messages)->withInput();
        }
        $professional_type = ProfessionalType::where('id',$id)->first();
        $professional_type->name = $_POST['name'];
        $professional_type->save();
        Session::flash('success', 'Professional Type Updated SuccessFully');
        return Redirect::to(URL::previous());
    }
    function deleteProfessionalType($id){



//        $str = 'Wax,MakeUpArtist';
//        dd($str[0]);
//        $result  = str_replace('Wax',"",$str);
//        dd($result);

        $professional_type = ProfessionalType::where('id',$id)->first();
        ProfessionalType::where('id',$id)->delete();
        $barbers = User::all();
        foreach ($barbers as $barber){
            if($barber->user_type != 'User' && $barber->user_type != null && $barber->user_type != ''){
//                $barber->user_type = str_replace($professional_type->name,"",$barber->user_type);
                $arr = explode(',',$barber->user_type);
                $check = false;
                for ($i = 0; $i < count($arr); $i++){
                    if($arr[$i] == $professional_type->name){
                        $check = true;
                        unset($arr[$i]);
                    }
                }
                if($check){
                    $final_string = implode(',',$arr);
                    $barber->user_type = $final_string;
                    $barber->save();
                }
            }
        }
        Session::flash('success', 'Professional Type Deleted SuccessFully');
        return redirect()->back();
    }


    function testAction() {
        echo request()->ip();
        exit;
        $barber = User::find(88);
        $user = User::find(87);
        //Send notifications
        $notificatinText = $user->first_name . ' ' . $user->last_name . ' requested a new appiontment for ';
        $notificatinSubject = 'New Appointment';
        $emailData['data']['mailData'] = ['email' => 'shahbaz.hassan@codingpixel.com', 'subject' => $notificatinSubject, 'message' => $notificatinText];
        $emailData['data']['mailOtherData'] = $this->mapData($user, $barber, '', 241);
        emailNotification($emailData);
    }

    function mapData($user = '', $barber = '', $pic = '', $bookinId = '', $payment = '') {
        return $notificationEmailData = ['user_id' => checkSetValue($_POST, 'user_id'),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'location' => $user->location,
            'service_at' => checkSetValue($_POST, 'service_at'),
            'start_time' => checkSetValue($_POST, 'start_time'),
            'service_time' => checkSetValue($_POST, 'service_time'),
            'price' => checkSetValue($_POST, 'price'),
            'services' => checkSetValue($_POST, 'services'),
            'date' => checkSetValue($_POST, 'date'),
            'pic' => checkSetValue($pic),
            'bookinId' => checkSetValue($bookinId),
            'payment' => checkSetValue($payment),
            'barberData' => json_encode($barber)];
    }

    function export($type = 'barbers') {
        $fileName = Carbon::now()->format('d-m-Y-His');
        $dataToExport = [];
        $textHelper = new \PHPExcel_Helper_HTML;
        $lineBreak = '<br>';
        if ($type == 'barbers') {
            $data['barbers'] = User::with('getService', 'getTimes')->where('user_type', '<>', 'User')->orderBy('first_name', 'asc')->get();
           // $dataToExport[] = ['First Name', 'Last Name', 'Email', 'Shop Name', 'Shop Location', 'Services', 'Bussiness Hours', 'Cancellation Fee', 'Cash Subscription'];
            foreach ($data['barbers'] as $key => $value) {
                $services = $bussinessHours = '';
                if ($value->getService) {
                    foreach ($value->getService as $service) {
                        $services .= 'Service: ' . $service->service_type
                                . ', Price: $' . $service->price . ', Time: ' . $service->time . $lineBreak;
                    }
                }

                if ($value->getTimes) {
                    $bussiness = $value->getTimes;
                    if (strpos($bussiness->mon, 'Off') === false) {
                        $twodats = explode('-', $bussiness->mon);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Monday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Monday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->tue, 'Off') === false) {
                        $twodats = explode('-', $bussiness->tue);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Tuesday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Tuesday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->wed, 'Off') === false) {
                        $twodats = explode('-', $bussiness->wed);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Wednesday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Wednesday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->thus, 'Off') === false) {
                        $twodats = explode('-', $bussiness->thus);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Thursday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Thursday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->fri, 'Off') === false) {
                        $twodats = explode('-', $bussiness->fri);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Friday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Friday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->sat, 'Off') === false) {
                        $twodats = explode('-', $bussiness->sat);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Saturday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Saturday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                    if (strpos($bussiness->sun, 'Off') === false) {
                        $twodats = explode('-', $bussiness->sun);
                        $startingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[0], 2);
                        $endingarray = preg_split('/(?<=[a-z])|(?=[a-z])/i', $twodats[1], 2);
                        $bussinessHours .= 'Sunday: ' . $startingarray[0] . ' ' . $startingarray[1] . ' - ' . $endingarray[0] . ' ' . $endingarray[1];
                    } else {
                        $bussinessHours .= 'Sunday: Closed';
                    }
                    $bussinessHours .= $lineBreak;
                }

                $services = $textHelper->toRichTextObject($services);
                $bussinessHours = $textHelper->toRichTextObject($bussinessHours);

                $dataToExport[] = ['First Name'=>$value->first_name, 'Last Name'=>$value->last_name, 'Email'=>$value->email, 'Shop Name'=>$value->shop_name, 'Shop Location'=>$value->shop_location, 'Services'=>$services, 'Bussiness Hours'=>$bussinessHours, 'Cancellation Fee'=>$value->cancel_fee,'Cash Subscription'=> $value->is_cash];
            }
        } elseif ($type == 'users') {
            $data['users'] = User::where('user_type', 'User')->orderBy('first_name', 'asc')->get();
          //  $dataToExport[] = ['First Name', 'Last Name', 'Email'];
             foreach ($data['users'] as $key => $value) {
            $dataToExport[] = ['First Name' =>$value->first_name, 'Last Name' =>$value->last_name, 'Email' =>$value->email];
             }
        }
        $file_str = ($type == 'users') ? 'customers' : 'professionals';
        $fileName = $file_str . '-' . $fileName;
//        \Excel::create($fileName, function($excel) use($dataToExport, $type) {
//            $excel->sheet(ucfirst($type), function($sheet) use($dataToExport) {
//                $sheet->cells("A:I", function ($cells) {
//                    $cells->setAlignment('center');
//                    $cells->setValignment('center');
//                });
//                $sheet->getStyle("A:I")->getAlignment()->setWrapText(true);
//                $sheet->setWidth(array(
//                    'E' => 50,
//                    'F' => 50,
//                    'G' => 50
//                ));
//                $sheet->setAutoSize(array(
//                    'A', 'B', 'C', 'D', 'H', 'I'
//                ));
//                $sheet->setAutoSize(false);
//                $sheet->fromArray($dataToExport, null, 'A1', true, false);
//            });
//        })->export('xlsx');
        $this->download_send_headers($fileName . ".csv");
        echo $this->array2csv($dataToExport);
    }

    function barberImages($barber_id) {
        $data['title'] = 'Images';
        $data['images'] = Gallery::where('barber_id', $barber_id)->orderBy('created_at', 'desc')->get();
        $data['barber_id'] = $barber_id;
        return view('images', $data);
    }

    function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
         ob_start();
        $df = fopen("php://output", 'w');
         fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
           // print_r($row);exit;
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    function deleteBarberImage($id) {
        $delete_image = Gallery::find($id);
        $delete_image->delete();
        Session::flash('success', 'Image Deleted SuccessFully');
        return Redirect::to(URL::previous());
    }

    function addBarberImage(Request $request) {
        $file = $request['photo'];
        $image_destination_path = 'public/images'; // upload path
        $image_extension = $file->getClientOriginalExtension(); // getting image extension
        $image_fileName = 'gallery_' . Str::random(15) . '.' . $image_extension; // renameing image
        $file->move($image_destination_path, $image_fileName);
        $add_image = new Gallery;
        $add_image->barber_id = $request['barber_id'];
        $add_image->image = $image_fileName;
        $add_image->save();
        Session::flash('success', 'Image Added SuccessFully');
        return Redirect::to(URL::previous());
    }

    function ios($deviceToken, $user_id, $message, $data = '', $barber_id, $booking_id = '', $is_save = 1) {
        set_time_limit(0);
        if ($is_save == 1) {
            $savenotification = new Notification;
            $savenotification->to_user = $user_id;
            $savenotification->from_user = $barber_id;
            $savenotification->text = $message;
            $savenotification->date = date('Y-m-d');
            $savenotification->is_read = 0;
            $savenotification->save();
        }

        $data = array('barber_data' => $data, 'text' => $message, 'user_id' => $user_id, 'booking_id' => $booking_id, 'timeago' => 'Just Now');
        // Put your private key's passphrase here:
        $passphrase = '';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/book/production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        $fp = stream_socket_client(
//                'ssl://gatew  ay.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'payload' => $data,
            'badge' => 1
        );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return TRUE;
    }

    function android($device_id, $user_id, $message, $data = '', $barber_id, $booking_id = '', $is_save = 1) {
        set_time_limit(0);
        if ($is_save == 1) {
            $savenotification = new Notification;
            $savenotification->to_user = $user_id;
            $savenotification->from_user = $barber_id;
            $savenotification->text = $message;
            $savenotification->date = date('Y-m-d');
            $savenotification->is_read = 0;
            $savenotification->save();
        }
        $gcmKey = "AIzaSyCenDLL9Pb1nayFHzcdUEg5bXB3mkU85vE";
        $url = 'https://android.googleapis.com/gcm/send';
        $registatoin_ids = array($device_id);
        $data = array('message' => $message, 'payload' => array('barber_data' => $data, 'text' => $message, 'user_id' => $user_id, 'timeago' => 'Just Now', 'booking_id' => $booking_id, 'badge' => 1));

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $data
        );
        $headers = array(
            'Authorization: key=' . $gcmKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return TRUE;
    }

    function sendNotification($barber) {
        $users = Booking::select('user_id')->where('barber_id', $barber->id)->get()->toArray();
        $all_users = User::whereIn('id', $users)->get();
        $notificatinText = $barber->first_name . ' ' . $barber->last_name . 'has added new announcement';
        foreach ($all_users as $user) {
            if ($user->send_notifiction) {
                if ($user->platform == 'ios') {
                    $this->ios($user->device_id, $user->id, $notificatinText, json_encode($barber), $barber->id, '');
                }
                if ($user->platform == 'android') {
                    $this->android($user->device_id, $user->id, $notificatinText, json_encode($barber), $barber->id, '');
                }
            }
        }
        return TRUE;
    }

}
