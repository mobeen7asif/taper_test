<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table='reviews';
            function getUser(){
        return $this->hasOne('\App\User', 'id', 'reviewed_by');
    }
}
