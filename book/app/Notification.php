<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    function getUser(){
       return $this->hasOne('App\User','id','from_user');
    }
}
