<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table='history';
    
    function getBarber(){
        return $this->hasOne('App\User','id','barber_id');
    }
}
