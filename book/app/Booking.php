<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    function User(){
        return $this->hasOne('App\User','id','user_id');
    }
    function Barber(){
        return $this->hasOne('App\User','id','barber_id');
    }
    function Rating(){
        return $this->hasOne('App\Rating','barber_id','barber_id');
    }
}
