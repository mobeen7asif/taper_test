<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalType extends Model
{
    protected $table = 'professional_types';
}
