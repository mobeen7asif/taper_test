<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
class User extends Authenticatable
{
    use Notifiable, Billable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    function getReviews(){
        return $this->hasMany('App\Review','barber_id');
    }
    function getRating(){
        return $this->hasOne('App\Rating','barber_id');
    }
    function getService(){
        return $this->hasMany('App\Service','barber_id');
    }
    function getImages(){
        return $this->hasMany('App\Gallery','barber_id');
    }
    function getTimes(){
        return $this->hasOne('App\BussnessHour','barber_id');
    }
    function getBookings(){
        return $this->hasMany('App\Booking','barber_id');
    }
    function getAppointments(){
        return $this->hasMany('App\Booking','user_id');
    }
    function getUserFavourites(){
        return $this->hasMany('App\UserFavorite','barber_id');
    }
}
