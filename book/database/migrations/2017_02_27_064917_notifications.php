<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from_user')->unsigned()->index();
            $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('to_user')->unsigned()->index();
            $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');
            $table->string('text')->nullable();
            $table->boolean('is_read')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('notifications');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
