<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('email')->unique();
            $table->string('password')->default('');
            $table->string('mobile')->default('');
            $table->string('user_type')->default('');
            $table->string('platform')->default('');
            $table->string('device_id')->default('');
            $table->string('login_type')->default('');
            $table->string('lat')->default('');
            $table->string('long')->default('');
            $table->string('session_token')->default('');
            $table->string('location')->default('');
            $table->string('profile_pic')->default('');
            $table->string('social_pic')->default('');
            $table->string('cover')->default('');
            $table->string('shop_name')->default('');
            $table->boolean('is_admin')->default(0);
            $table->string('shop_location')->default('');
            $table->string('shop_long')->default('');
            $table->string('shop_lat')->default('');
            $table->integer('cash_count')->default(0);
            $table->boolean('is_login')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('users');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
