<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('barber_id')->unsigned()->index();
            $table->foreign('barber_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('status')->nullable();
            $table->string('service_at')->nullable();
            $table->date('date')->nullable();
            $table->string('payment')->nullable();
            $table->integer('price')->default(0);
            
            $table->string('services')->nullable();
            $table->integer('service_time')->default(0);
            $table->string('stripe_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('bookings');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
