<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stripesection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_stripe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('barber_id')->unsigned()->index();
            $table->foreign('barber_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('stripe_payout_account_id')->nullable();
            $table->string('stripe_payout_account_public_key')->nullable();
            $table->string('stripe_payout_account_secret_key')->nullable();
            $table->longText('stripe_payout_account_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('user_stripe');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
