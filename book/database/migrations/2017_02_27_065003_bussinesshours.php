<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bussinesshours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bussness_hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('barber_id')->unsigned()->index();
            $table->foreign('barber_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('mon')->nullable();
            $table->string('tue')->nullable();
            $table->string('wed')->nullable();
            $table->string('thus')->nullable();
            $table->string('fri')->nullable();
            $table->string('sat')->nullable();
            $table->string('sun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('bussness_hours');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
