<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class History extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('barbder_id')->unsigned()->index();
            $table->foreign('barbder_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('total_payments')->nullable();
            $table->string('payment_type')->nullable();
            $table->integer('earning')->nullable();
            $table->integer('app_blance')->nullable();
            $table->integer('appiontment_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('history');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }

}
