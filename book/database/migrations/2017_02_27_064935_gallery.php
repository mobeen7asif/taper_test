<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Gallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('barber_id')->unsigned()->index();
            $table->foreign('barber_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        Schema::drop('gallries');
        \DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
